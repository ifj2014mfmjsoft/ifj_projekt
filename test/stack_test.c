/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   stack_test.c
 * Datum:   14.11.2014
 * Autori:  Martin Cernek, xcerne01@stud.fit.vutbr.cz
 */

#include <stdio.h>
#include <stdlib.h>
#include "../src/stack.h"

#define pi 3.141592653589

int main(void)
{
	t_stack S;

	if (init_stack(&S) == STACK_INIT_FAIL) {
		printf("ERROR: Inicializacia zasobniku zlyhala.\n");
		return 1;
	}

	for (int i = 1; i <= 10; i++)
	{
		//long double tmp = pi * i;
		int tmp = i;
		if (push(&S, &tmp, sizeof(tmp)) == STACK_PUSH_FAIL) {
		 	printf("ERROR: Chyba pri push.\n");
			return 1;
		}
	}

	int tmp;

	printf("Je zasobnik prazdny? (0 - false, 1 - true):\t%d\n", empty(&S));

	for (int i = 0; i < 10; i++)
	{
		if (get_from_index(&S, &tmp, sizeof(tmp), i) == STACK_GFI_FAIL) {
		 	printf("ERROR: Chyba pri vracani.\n");
			return 1;
		}
		printf("Zobrana hodnota:\t%d\t\tHodnota vrcholu zasobiku:\t%d\n", tmp, S.stack_top);
	}

	printf("\n\n#############################################################################################\n\n");

    tmp = 0;

	for (int i = 0; i < 10; i=i+5)
	{
		if (rewrite_in_index(&S, &tmp, sizeof(tmp), i) == STACK_RII_FAIL) {
		 	printf("ERROR: Chyba pri prepisovani.\n");
			return 1;
		}
	}

	for (int i = 0; i < 10; i++)
	{
		if (get_from_index(&S, &tmp, sizeof(tmp), i) == STACK_GFI_FAIL) {
		 	printf("ERROR: Chyba pri vracani.\n");
			return 1;
		}
		printf("Zobrana hodnota:\t%d\t\tHodnota vrcholu zasobiku:\t%d\n", tmp, S.stack_top);
	}
	/*
	while (!empty(&S)) {
		if (top(&S, &tmp, sizeof(tmp)) == STACK_TOP_FAIL) {
		 	printf("ERROR: Chyba pri top.\n");
			return 1;
		}
		if (pop(&S, sizeof(tmp)) == STACK_POP_FAIL) {
		 	printf("ERROR: Chyba pri pop.\n");
			return 1;
		}
		printf("Topnuta hodnota:\t%Lf\tHodnota vrcholu zasobiku:\t%d\n", tmp, S.stack_top);
	} */

	printf("Je zasobnik prazdny? (0 - false, 1 - true):\t%d\n", empty(&S));

	free_stack(&S);

	return 0;
}