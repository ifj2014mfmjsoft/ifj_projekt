#!/bin/bash     

#/**
# * Projekt: Projekt do predmetu IFJ
# * Popis:   Interpret jazyka IFJ14
# * Subor:   Skript na testovanie spravnej funkcionality
# * Datum:   28.11. 2014
# * Autori:  Frederik Muller, xmulle20@stud.fit.vutbr.cz
# */



#----------------------------------------------------
#----------------------------------------------------

#                   TESTOVANIE

#----------------------------------------------------
#----------------------------------------------------



cd tests/scanner/
iterator=`ls test{100..999} 2>/dev/null`
cd ..
cd ..

echo ""
for t in $iterator
do
  echo "################################################################################"
	echo $t.
	cat ./tests/scanner/$t
    echo -n _EOF
	echo
     
        valgrind --quiet --leak-check=yes ./scanner_test ./tests/scanner/$t 
        echo $?
done


echo
 echo "----------------------------------"


############################## END ############################################

