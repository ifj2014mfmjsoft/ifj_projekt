/**
 * Test for string.c  15.10.2014
 * Author: Jergus Jacko, xjacko03@stud.fit.vutbr.cz
 */

#include <stdio.h>
#include <stdlib.h>
#include "../src/string.h"

int main()
{
    t_string newStr1;
    t_string newStr2;
    char c;

    if (stringInit(&newStr1) != STR_OPERATION_OK)
    {
        printf("STRINIT_ERR");  //TODO nech vracia chybovy kod podla predpisu z common.h
        return 0;
    }

    printf("Zadaj Retazec1: ");
    while ((c = getchar()) != EOF)
    {
        stringAddChar(&newStr1, c);
    }

    printf("\nRetazec1: %s\n", newStr1.string);


    if (stringInit(&newStr2) != STR_OPERATION_OK)
    {
        printf("STRINIT_ERR");  //TODO nech vracia chybovy kod podla predpisu z common.h
        return 0;
    }
    
    printf("Zadaj Retazec2: ");
    while ((c = getchar()) != EOF)
    {
        stringAddChar(&newStr2, c);
    }

    printf("\nRetazec2: %s\n", newStr2.string);

    if((stringCmpString(&newStr1, &newStr2)) == 0 )
    {
        printf("STR1 == STR2\n");
    }
    else if((stringCmpString(&newStr1, &newStr2)) >  0)
    {
        printf("STR1 > STR2\n");
    }
    else
    {
        printf("STR1 < STR2\n");
    }

//Test for stringCmpConstString
    if((stringCmpConstString(&newStr1, "while")) == 0)
    {
        printf("STR1 a constString 'while' su rovnake.\n");
    }
    else
    {
        printf("STR1 a constString 'while' nie su rovnake.\n");
    }

//Test for stringConcatenation
    t_string concatenatedString;
    if ((stringConcatenation(&concatenatedString, &newStr1, &newStr2)) == STR_ALLOC_FAIL)
    {
        printf("ALLOCATION FAILURE");
        return EXIT_FAILURE;
    }
    printf("Concatenated string: %s\n", concatenatedString.string);

//Test for stringCopy
    stringCopy(&concatenatedString, &newStr2);
    printf("Kopia Retazca2: %s\n", concatenatedString.string);

//Test for stringDuplicate
    t_string duplicate;
    if((stringDuplicate(&duplicate, &newStr1)) == STR_ALLOC_FAIL)
    {
        printf("E_ALLOC");
        return EXIT_FAILURE;
    }
    printf("Duplikat Retazca1: %s\n", duplicate.string);

    stringFree(&newStr1);
    stringFree(&newStr2);
    stringFree(&concatenatedString);
    stringFree(&duplicate);
    return 0;
}
