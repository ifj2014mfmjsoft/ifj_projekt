#!/bin/bash     

#/**
# * Projekt: Projekt do predmetu IFJ
# * Popis:   Interpret jazyka IFJ14
# * Subor:   Skript na testovanie spravnej funkcionality
# * Datum:   28.11. 2014
# * Autori:  Frederik Muller, xmulle20@stud.fit.vutbr.cz
# */


a="";
b="";
c="";
d="";
e="";
f="";
g="";
h="";
i="";
j="";
k="";
l="";
while getopts "1234vabcdefg" o
do case "$o" in
"1") a="TRUE";; 
"2") b="TRUE";; 
"3") c="TRUE";; 
"4") d="TRUE";;
"v") e="TRUE";;
"a") f="TRUE";;
"b") g="TRUE";;
"c") h="TRUE";;
"d") i="TRUE";;
"e") j="TRUE";;
"f") k="TRUE";;
"g") l="TRUE";;
*) echo "Use options 1, 2, 3 , 4 or a(base), b(minus), c(repeat), d(elseif), e(funexp), f(boolop), g(msg) or -v (without valgrind) with a parameter." >&2
  exit 1;;
esac
done

echo
if [[ "$a" == "TRUE" ]]; then cyklus="1"
fi
if [[ "$b" == "TRUE" ]]; then cyklus="$cyklus 2"
fi
if [[ "$c" == "TRUE" ]]; then cyklus="$cyklus 3"
fi
if [[ "$d" == "TRUE" ]]; then cyklus="$cyklus 4"
fi
if [[ "$f" == "TRUE" ]]; then cyklus="$cyklus a"
fi
if [[ "$g" == "TRUE" ]]; then cyklus="$cyklus b"
fi
if [[ "$h" == "TRUE" ]]; then cyklus="$cyklus c"
fi
if [[ "$i" == "TRUE" ]]; then cyklus="$cyklus d"
fi
if [[ "$j" == "TRUE" ]]; then cyklus="$cyklus e"
fi
if [[ "$k" == "TRUE" ]]; then cyklus="$cyklus f"
fi
if [[ "$l" == "TRUE" ]]; then cyklus="$cyklus g"
fi

cd ./../src
make purge        ##vymaze make files
make -B           ##pripraveny make
rm -f *.o         ##zmazeme objektove
rm dep.list
chmod +x main     ##pridame prava
cd ./../test



#  VYMAZEME stare subory
cd tests
for b in $cyklus
do
  cd $b
  rm *-diff  2>/dev/null >/dev/null
  rm *-result  2>/dev/null >/dev/null
  cd ..
done
cd ..


return_variable=0 ##pocitadlo zlyhania returnu
diff_variable=0   ##pocitadlo zlyhania diffu
valgrind_errors=0 ##pocitadlo memory leakov pomocou valgrindu

return_faults_list=""
diff_faults_list=""
valgrind_faults_list=""

#----------------------------------------------------
#----------------------------------------------------

#                   TESTOVANIE

#----------------------------------------------------
#----------------------------------------------------


for b in $cyklus
do

cd tests/$b/
iterator=`ls test{1..400} 2>/dev/null`
cd ..
cd ..
echo ""
for t in $iterator
do
  echo "################################################################################"
  echo "Running tests/"$b" for "$t":"        
     if [ -z "$e" ] && [ -a ./tests/$b/$t-input ] #ak existuje -input
                              then     
                                 valgrind --log-file=valgrind.txt ./../src/main ./tests/$b/$t < ./tests/$b/$t-input > ./tests/$b/$t-result 2>/dev/null
                                 value=$?
     fi 
     if [ -z "$e" ] && [ ! -f ./tests/$b/$t-input ] #ak existuje -input
                              then     valgrind --log-file=valgrind.txt ./../src/main ./tests/$b/$t > ./tests/$b/$t-result 2>/dev/null
                              value=$?
     fi

     if [ ! -z "$e" ] && [ -a ./tests/$b/$t-input ]   #ak existuje -input
                               then     
                                 ./../src/main ./tests/$b/$t < ./tests/$b/$t-input > ./tests/$b/$t-result 2>/dev/null
                                 value=$?
     fi
     if [ ! -z "$e" ] && [ ! -f ./tests/$b/$t-input ] #ak existuje -input
                              then  ./../src/main ./tests/$b/$t > ./tests/$b/$t-result 2>/dev/null
                              value=$?
     fi

     if [ -a ./tests/$b/$t-return ] #ak existuje -return
      then
        return_value=`cat ./tests/$b/$t-return`
      else
        return_value=""
      fi      
    if [ -z "$return_value" ]
     then 
        if [ "$value" -eq 0 ]
          then echo "   OK exit_code"
          else echo "   failed exit_code !!!"
               echo "   expected 0 but returned" $value
               ((return_variable+=1))
               return_faults_list="$return_faults_list:      $b/$t"
        fi
     else 
        if [ "$value" -eq $return_value ]
          then echo "   OK exit_code"
          else echo "   failed exit_code !!!"
               echo "   expected" $return_value "but returned " $value
               ((return_variable+=1))
               return_faults_list="$return_faults_list:      $b/$t"
        fi
    fi
    diff ./tests/$b/$t-expected ./tests/$b/$t-result > ./tests/$b/$t-diff
    if [ -s ./tests/$b/$t-diff ]
      then  echo "   failed diff !!!"
            ((diff_variable+=1))
            diff_faults_list="$diff_faults_list:      $b/$t"
            echo "!!! FAILURE !!!"
                cat ./tests/$b/$t-diff
      else echo "   OK diff"
    fi
  echo ""
  if [ -z "$e" ] 
    then
        valgrind_lines=`cat valgrind.txt | sed 's/^.*[=]/     |/g' | grep bytes | wc -l`
        if [ "$valgrind_lines" != "2" ]
        then
            ((valgrind_errors+=1))
            valgrind_faults_list="$valgrind_faults_list:      $b/$t"
        fi

        echo "     ---------------------------------------------------------------VALGRIND OUT"
        cat valgrind.txt | sed 's/^.*[=]/     |/g' | grep bytes 
        echo "     ---------------------------------------------------------------VALGRIND OUT"
  fi
  echo ""
done

done

    diff_faults_list=`echo $diff_faults_list | sed "y/:/\n/" | sed "s/ /      /"`
    return_faults_list=`echo $return_faults_list | sed "y/:/\n/" | sed "s/ /      /"`
    valgrind_faults_list=`echo $valgrind_faults_list | sed "y/:/\n/" | sed "s/ /      /"`

 echo "----------------------------------"
 echo
 if [ "$diff_variable" -eq 0 ] 
    then echo "   diff success"
    else echo "   diff failure" $diff_variable "times"
         echo "$diff_faults_list"
 fi
 if [ "$return_variable" -eq 0 ] 
    then echo "   return success"
    else echo "   return failure" $return_variable "times"
         echo "$return_faults_list"
 fi
 if [ -z "$e" ]
    then
    if [ "$valgrind_errors" -eq 0 ] 
        then echo "   valgrind success"
        else echo "   valgrind failure" $valgrind_errors "times"
             echo "$valgrind_faults_list"
    fi 
 fi

echo
 echo "----------------------------------"

echo 
echo "Do you want to remove temporary files (Y/N)?"
read x
if [ "$x" = "Y" -o "$x" = "y" ]
  then
    rm valgrind.txt 2>/dev/null >/dev/null
    cd tests
    for b in $cyklus
    do
      cd ./$b
      rm *-diff 2>/dev/null >/dev/null
      rm *-result 2>/dev/null >/dev/null
    cd ..
    done
    cd ..
fi
cd .. 
cd src
rm main
############################## END ############################################

