#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include "../src/string.h"

#define TYPE_INT 0
#define TYPE_REAL 1
#define E_INTERN 2
#define E_RUN_INPUT 3
#define E_SEM_TYPE 4
#define E_RUN_N_INIT 5
#define READLN_OK 6

int read_number(int type, t_string *read_string, void *number);

int main()
{
	t_string str;
	int c;
	double number;

    stringInit(&str);

    while ((c = getchar()) != '\n' && c != EOF) {
        stringAddChar(&str, c);
    }

    if (read_number(TYPE_REAL, &str, &number) != READLN_OK) {
        printf("Nespravne cislo!\n");
        stringFree(&str);
        return 1;
    }

    printf("Prekonvertovane cislo: %g\n", number);

    stringFree(&str);
	return 0;
}

int read_number(int type, t_string *read_string, void *number)
{
    t_string tmp_string;
    bool is_read = false;
    char *errstr = 0;
    int ret;
    
    ret = stringInit(&tmp_string);
    if (ret != STR_OPERATION_OK)
        return E_INTERN;

    for (int i = 0; i < read_string->length; i++)
    {
        if (isspace(read_string->string[i]) != 0) 
        {
            if (is_read != true)
                continue;
            else
                break;
        }
        stringAddChar(&tmp_string, read_string->string[i]);
        is_read = true;
    }

    if (is_read != true) {
        stringFree(&tmp_string);
        return E_RUN_INPUT;
    }

    switch (type) {
        case TYPE_INT:
            *((int *) number) = strtol(tmp_string.string, &errstr, 10);
            if (*errstr != 0) {
                stringFree(&tmp_string);
                return E_RUN_N_INIT;
            }
            break;
        case TYPE_REAL:
            *((double *) number) = strtod(tmp_string.string, &errstr);
             if (*errstr != 0) {
                stringFree(&tmp_string);
                return E_RUN_N_INIT;
            }
            break;
        default:
            stringFree(&tmp_string);
            return E_SEM_TYPE;
            break;
    }

    stringFree(&tmp_string);
    return READLN_OK;
}