#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "../src/ial.h"
#include "../src/string.h"

#define NUM 30
#define KEY_SIZE 7

void print_table(t_table * table);
void print_key(char * key, uint64_t key_size);
void print_table_info(t_table * table);

int main(void)
{
    t_table *table = table_init(128);
    if (table == NULL) {
        fprintf(stderr, "Table could not be initialized!\n");
        return EXIT_FAILURE;
    }
    
    print_table_info(table);
    print_table(table);

    int data_pole[NUM] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    t_string key_pole[NUM];

    int i;
    for (i = 0; i < NUM; i++) {
        stringInit(&key_pole[i]);
    }

    for (i = 0; i < NUM; i++) {
        stringAddChar(&key_pole[i], 'a' + i);
    }

    int ret = 0;
    t_tab_data * data_ptr;

    for (i = 0; i < NUM; i++) {
        ret = table_insert(table, &key_pole[i], &data_ptr);
        switch (ret) {
        case TAB_INSERT_OK:
            data_ptr->a = data_pole[i];
            break;
        case TAB_INSERT_FOUND:
            fprintf(stderr, "Item already inserted\n");
            break;
        case TAB_INSERT_ALL_ERR:
            fprintf(stderr, "Allocation error\n");
            break;
        }
    }

    print_table_info(table);
    print_table(table);
    
    /*
    //1. raz
    printf("INSERT\n\n");
    for (i = 0; i < NUM; i++) {
        ret = table_insert(table, key_pole[i], 7, (t_tab_data **) &data);
        switch (ret) {
        case TAB_INSERT_FAIL:
            fprintf(stderr, "Item already inserted.\n");
            break;
        case TAB_INSERT_FULL:
            while (ret == TAB_INSERT_FULL) {
                if (table_resize(table) == TAB_RESIZE_FAIL) {
                    fprintf(stderr, "RESIZE FAIL\n");
                    table_destroy(table);
                    return EXIT_FAILURE;
                }
                ret = table_insert(table, key_pole[i], 7, (t_tab_data **)&data);
            }
            if (ret == TAB_INSERT_FAIL) {
                fprintf(stderr, "Item already inserted.\n");
                break;
            }
            if (ret == TAB_INSERT_ALL_ERR) {
                fprintf(stderr, "ALLOC ERR\n");
                table_destroy(table);
                return EXIT_FAILURE;
            }
            *(int *)data = data_pole[i];
            break;
        case TAB_INSERT_OK:
            *(int *)data = data_pole[i];
            break;
        case TAB_INSERT_ALL_ERR:
            fprintf(stderr, "ALLOC ERROR\n");
            table_destroy(table);
            return EXIT_FAILURE;
        }
    }

    print_table(table);
    */
    
    printf("-----------------------------------------------------------------------------------------\n\n");

    printf("GET DATA\n\n");
    for (i = 0; i < NUM; i++) {
        ret = table_search(table, &key_pole[i], (t_tab_data **) &data_ptr);
        if (ret == TAB_SEARCH_OK) {
            printf("Key:    %s\n", key_pole[i].string);
            printf("Data:  %d\n\n", data_ptr == NULL ? -1 : ((t_tab_data *)data_ptr)->a);
        }
    }

    /*
    print_table(table);
    */

    for (i = 0; i < NUM; i++) {
        stringFree(&key_pole[i]);
    }
    table_destroy(table);
    return EXIT_SUCCESS;
}

void print_table(t_table * table)
{
    unsigned int i;
    unsigned int j;
    t_tab_item * tmp;

    for (i = 0; i < table->size; i++) {
        printf("Row:    %d\n", i);

        for (tmp = table->items[i], j = 0; tmp != NULL; tmp = tmp->next, j++) {
            printf("--Item:         %d\n", j);
            printf("--Key-size:     %d\n", tmp->key.length);
            printf("--Key-all-size: %d\n", tmp->key.allocSize);
            printf("---Key:         ");
            print_key(tmp->key.string, tmp->key.length);
            printf("---Data:        %d\n\n", tmp->data.a);
        }
        printf("-------------------------------------\n\n");
    }
    printf("-----------------------------------------------------------------------------------------\n\n");


    /*
    for (i = 0; i < table->size; i++) {
        printf("Row:    %d\n", i);

        for (j = 0; j < TAB_ROW_ITEMS; j++) {
            printf("---Col:         %d\n", j);
            printf("---Key-size:    %llu\n", (long long unsigned) table->items[i*TAB_ROW_ITEMS + j].key_size);
            printf("---Free-flag:   %d\n", table->items[i*TAB_ROW_ITEMS + j].free_flag);
            printf("---Key:         ");
            print_key(table->keys + table->items[i*TAB_ROW_ITEMS + j].key_offset, table->items[i*TAB_ROW_ITEMS + j].key_size);
            printf("---Data:        %d\n\n", table->items[i*TAB_ROW_ITEMS + j].data.a);
        }

        printf("-------------------------------------\n\n");
    }
    printf("-----------------------------------------------------------------------------------------\n\n");
    */
}

void print_key(char * key, uint64_t key_size)
{
    uint64_t i;
    if (key != NULL) {
        for (i = 0; i < key_size; i++)
            putchar(key[i]);
    }
    putchar('\n');
}

void print_table_info(t_table * table)
{
    printf("TABLE_SIZE: %llu\n", (long long unsigned) table->size);
    printf("TABLE_PTR:  %llx\n", (long long unsigned) table);
    printf("ITEMS_PTR:  %llx\n\n", (long long unsigned) table->items);
}
