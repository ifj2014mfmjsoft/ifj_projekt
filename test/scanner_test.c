/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   scaner.h
 * Datum:   11.10. 2014
 * Autori:  Frederik Müller, xmulle20@stud.fit.vutbr.cz
 */

#include <stdio.h>
#include <stdlib.h>
#include "../src/scanner.h"
#include "../src/common.h"
#include "../src/string.h"

FILE * source_file;
t_token token;

int main (int argc, char *argv[])
{
    if (stringInit(&token.attr.lit_string_or_id) == STR_ALLOC_FAIL)
        return E_INTERN;
    if (open_File(argc, argv) == 1){
       stringFree(&token.attr.lit_string_or_id);
       return E_INTERN; //could not open file or invalid input (argc != 2)
    }
    int a;
    while ((a = getNextToken()) == E_OK || a == TOKEN_END_OF_FILE){
        if (token.type==TOKEN_LIT_INTEGER)
            printf("%d \n", token.attr.lit_int);
        else if (token.type==TOKEN_LIT_REAL)
            printf("%f \n", token.attr.lit_double);
        else if (token.type==TOKEN_LIT_STRING)
            printf("%s \n", token.attr.lit_string_or_id.string);
        else if (token.type==TOKEN_IDENTIFIER)
            printf("%s \n", token.attr.lit_string_or_id.string);
        else if (token.type==TOKEN_END_OF_FILE){
            printf("Ok, Koniec. \n");
            stringFree(&token.attr.lit_string_or_id);
            fclose(source_file);
            return E_OK;
        } else
            printf("%d \n", token.type);
    }
    printf("Chyba. \n");
    fclose(source_file);
    stringFree(&token.attr.lit_string_or_id);
    return E_LEX;
}
