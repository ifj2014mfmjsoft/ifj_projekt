#include <stdio.h>
#include <stdlib.h>
#include "../src/ial.c"
#include "../src/string.c"

int main(void)
{
    t_string vysledok;
    t_string povodny;
    t_string podretazec;

    if (stringInitValue(&povodny, "avakakedabra") != STR_OPERATION_OK){
        return -1;
        printf("Chyba!\n");
    }

    if (stringInitValue(&podretazec, "kake") != STR_OPERATION_OK){
        return -1;
        printf("Chyba!\n");
    }


    printf("HEAPSORT:\n");
    printf("%s\n", povodny.string);
    heap_sort(&povodny, &vysledok);
    printf("%s\n", vysledok.string);
    
    printf("\nFIND:\n");
    printf("Index: %d\n",find(&povodny, &podretazec));

    stringFree(&povodny);
    stringFree(&vysledok);
    stringFree(&podretazec);

    return 0;
}
