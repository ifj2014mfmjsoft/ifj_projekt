#include "../src/list.h"

#define NUM 10

t_tab_item *tab_item_ptr[NUM];
t_tab_item tab_item[NUM];

int main(void)
{
    int i;
    int ret;
    t_list * list = list_init();

    if (list == NULL)
        return 1;

    for (i = 0; i < NUM; i++) {
        tab_item_ptr[i] = &tab_item[i];
    }

    for (i = 0; i < NUM; i++) {
        ret = list_insert_last(list, tab_item_ptr[i]);
        if (ret != LIST_OP_OK)
            fprintf(stderr, "CHYBA\n");
    }

    printf("LIST\n");
    
    for (i = 0; i < NUM; i++) {
        printf("%d: %x\n", i, list_get_first(list));
        list_free_first(list);
    }

    for (i = 0; i < NUM; i++) {
        printf("%d: %x\n", i, list_get_first(list));
        list_free_first(list);
    }

    list_free(list);

    printf("%x\n", list);

    return 0;
}
