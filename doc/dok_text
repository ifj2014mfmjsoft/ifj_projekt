2. Popis riešenia

Projekt sme implementovali z niekoľkých funkčných celkov podľa schémy na obrázku <uviest odkaz na obrazok>. 
Main predstavuje hlavný program, ktorý spracúva argumenty príkazového riadku, potom volá parser a interpreter,
ktorých úlohou je samotný preklad a prevedenie vstupného programu. V nasledujúcich podkapitolách sú popísané
jednotlivé moduly.


2.2 Syntaktický analyzátor

Syntaktický analyzátor je reprezentovaný dvomi modulmi - parser a expression. Úlohou syntaktického analyzátoru
je prevádzať syntaktickú a sémantickú analýzu vstupného programu a priebežne generovať inštrukcie vnútorného kódu. 
Keďže sémantické akcie a generovanie inštrukcií vnútorného kódu sú tesne previazané s pravidlami
gramatiky pre syntaktickú analýzu, neimplementovali sme samostatné moduly pre sémantickú analýzu a generovanie
inštrukcií, ale zahrnuli sme ich do syntaktického analyzátoru. 

Sémantické kontroly sa uskutočňujú pomocou komunikácie s tabuľkou symbolov. Tabuľka symbolov je tvorená hashovaciou
tabuľkou (štruktúra tabuľky podrobnejšie v kapitole <cislo kapitoly>). Pre globálne premenné, funkcie a konštanty slúži 
globálna tabuľka symbolov. Každá funkcia má ešte vlastnú lokálnu tabuľku symbolov, do ktorej sa ukladajú parametre,
lokálne premenné a špeciálna premenná pre návratovú hodnotu funkcie.

Vnútorný kód sa ukladá do lineárneho zoznamu, kde každý záznam predstavuje jednu inštrukciu.

Pre syntaktickú analýzu sme zvolili metódu rekurzívneho zostupu a výrazy sme spracovali pomocou precedenčnej syntaktickej analýzy. 
LL-gramatika a precedenčná tabuľka, ktoré sme použili pri implementácii syntaktického analyzátoru, sú v prílohe <odkaz na prílohu>. V gramatike sa nachádza
špeciálny neterminál <expression>, ktorý znamená prechod z rekurzívneho zostupu na precedenčnú analýzu výrazu.


2.2.1 Rekurzívny zostup

Syntaktickú analýzu metódou rekurzívneho zostupu, ktorá je založená na LL-gramatike v prílohe <uviesť odkaz na prílohu>,
zabezpečuje modul parser. Modul je implementovaný ako sada funkcií, z ktorých každá predstavuje jeden neterminál gramatiky. 

Parser je volaný z hlavného programu (main) po vyhodnotení vstupných parametrov. Parser následne podľa potreby
volá scanner, ktorý uloží do globálnej premennej aktuálne načítaný token. V prípade, že nie je možné poskladať príslušné pravidlo
gramatiky, dôjde k ukončeniu programu s návratovým kódom pre syntaktickú chybu. Parser taktiež zabezpečuje všetky potrebné
sémantické kontroly. Pri definíciách premenných a funkcií vkladá parser do tabuľky nové záznamy, kontroluje, či nedochádza k predefinovaniu
premenných a funkcií, kontroluje počet definícií a deklarácií funkcií, kontroluje zhodnosť dopredných deklarácii s definíciami a to, či sa identifikátory
premenných a parametrov nezhodujú s identifikátormi už definovaných funkcií. Pri príkazoch je kontrolovaná správnosť typov, počty
parametrov funkcií.

Ďalšou úlohou parsru je generovanie vnútorného kódu. Vnútorný kód bude podrobnejšie popísaný v podkapitole zaoberajúcej sa 
interpretáciou vnútorného kódu <uviesť číslo kapitoly>.

Pri príkaze priradenia a volania funkcie sme museli riešiť problém ako rozoznať, o ktorý príkaz sa jedná. V prípade, že sa na pravej 
strane príkazu nachádza identifikátor, ktorý odpovedá identifikátoru funkcie, overí sa, či sa tento príkaz nachádza v tele funkcie.
Ak je to v tele funkcie a identifikátor je rovnaký ako názov funkcie, môže ísť o volanie funkcie alebo o použitie premennej pre
návratovú hodnotu. Vtedy sa načíta ďalší token a podľa toho, či je to ľavá okrúhla zátvorka alebo nie, bude sa pokračovať
vyhodnocovaním volania funkcie alebo precedenčnou analýzou výrazu. V ostatných prípadoch sa tiež zavolá precedenčná analýza výrazu.
Implementácia rozšírenia FUNEXP preniesla tento problém do precedenčnej analýzy výrazu (kapitola <cislo>).


2.4 Rozšírenia

V nasledujúcich podkapitolách sú popísané zmeny, ktoré sme museli začleniť do implementácie, prípadne problémy, ktorým sme
pri jednotlivých rozšíreniach museli čeliť a riešenia, ktoré sme na tieto problémy aplikovali.


2.4.3 REPEAT

Implementácia rozšírenia REPEAT vyžadovala pridanie nového pravidla do gramatiky a pridanie kľúčových slov "repeat" a "until" do zoznamu
kľúčových slov. Taktiež bolo potrebné navrhnúť postupnosť generovania inštrukcií vnútorného kódu, ktorá je podobná postupnosti generovanej pri
príkaze cyklu while. Nebolo nutné pridávať žiadne ďalšie inštrukcie.


2.4.5 ELSEIF

Pre podporu rozšírenia ELSEIF sme museli upraviť pravidlo pre podmienený príkaz a zaviesť nový neterminál <else_nonterm>. Postupnosť generovaných inštrukcií
nebolo nutné upravovať.


2.4.4 FUNEXP

Rozšírenie FUNEXP vyžadovalo úpravy v gramatike a implementácii modulov parser a expression. Toto rozšírenie prinieslo zjednodušenie LL-gramatiky,
pretože nebolo nutné mať samostatné pravidlo pre príkaz priradenia a volania funkcie. Problém s rozlíšením identifikátoru premennej a volania funkcie sa tak
preniesol do precedenčnej analýzy, kde je riešený podobne ako je to popísané v kapitole <cislo kapitoly s rekurzivnym zostupom>. Keď sa vyhodnotí, že sa jedná
o volanie funkcie, zavolá sa funkcia reprezentujúca špeciálny neterminál <function_call> a volanie funkcie sa vyhodnotí rekurzívnym zostupom. K ďalšej zmene
došlo v pravidlách pre vstupné parametre funkcie, kde namiesto neterminálu <term> vystupuje neterminál <expression>, nakoľko sa vo vstupných parametroch
môžu nachádzať aj výrazy.


3.3 Hashovacia tabuľka

Tabuľku symbolov sme implementovali ako tabuľku s rozptýlenými položkami s explicitne zreťazenými synonymami. V projekte sme použili jednu globálnu tabuľku
symbolov, kde sú uložené záznamy globálnych premenných, literálov a funkcií. Každá funkcia obsahuje vlastnú lokálnu tabuľku symbolov, v ktorej sú uložené
záznamy lokálnych premenných, parametrov a návratovej hodnoty funkcie. Štruktúra globálnej a lokálnej tabuľky sú rovnaké. Jediný rozdiel je v prednastavenej
veľkosti tabuľky pri inicializácii. Záznam tabuľky obsahuje kľúč, dáta a ukazovateľ na ďalšiu položku v zozname synoným. 

Kľúč je typu t_string, ktorý je popísaný v kapitole <cislo kapitoly Algoritmy>. Pre parametre, premenné a funkcie obsahuje ich identifikátor. 
Pre literály je vygenerovaný unikátny kľúč začínajúci číslicou. Tým je zabezpečené, že nemôže byť zhodný s identifikátorom.

Dáta obsahujú typ dát a samotný obsah dát. Typ dát je typu integer a môže obsahovať hodnoty priradené výčtovým typom, ktoré reprezentujú globálnu premennú,
funkciu, jednu zo štyroch vstavaných funkcií, literál alebo položku lokálnej tabuľky symbolov. Položka v lokálnej tabuľke symbolov je rovnaká pre lokálnu
premennú, parameter, aj premennú pre návratovú hodnotu. Samotný obsah dát je reprezentovaný unionom, ktorý môže obsahovať štruktúru pre globálnu premennú, 
funkciu, literál, položku v lokálnej tabuľke symbolov. Štruktúra pre globálnu premennú obsahuje typ premennej, jej hodnotu a boolovskú premennú uchovávajúcu
informáciu o inicializácii premennej. Štruktúra pre literál obsahuje iba typ a hodnotu literálu. Štruktúra pre funkciu obsahuje návratový typ, počet parametrov,
počet lokálnych premenných, boolovské informácie o doprednej deklarácii a definícii, ukazovatele na prvú inštrukciu funkcie, na lokálnu tabuľku symbolov,
na prvý parameter, na prvú lokálnu premennú, na premennú pre návratovú hodnotu. Štruktúra v lokálnej tabuľke obsahuje typ, polohu na zásobníku, s ktorou pracuje
interpret vnútorného kódu, a ukazovateľ na nasledujúcu položku rovnakého typu (parameter alebo lokálna premenná).

Pre mapovanie položiek do tabuľky sme použili hashovaciu funkciu MurmurHash3 < uviest spravne citaciu, odkaz https://code.google.com/p/smhasher/wiki/MurmurHash3, 
autor Austin Appleby >.
Pre túto funkciu sme sa rozhodli kvôli jej rýchlosti.


4.1 Rozdelenie úloh

Matej Vido:
    vedenie tímu, modul parser pre rekurzívny zostup, návrh gramatiky, hashovacia tabuľka, dokumentácia

Frederik Muller:
    modul scanner pre lexikálnu analýzu, návrh konečného automatu, pomocné štruktúry, dokumentácia

Martin Černek:
    modul interpreter, návrh konečného automatu, algoritmus pre Heap sort, Knuth-Moris-Prattov algoritmus, dokumentácia

Jerguš Jacko:
    modul expression pre precedenčnú analýzu výrazov, pomocné štruktúry, návrh gramatiky, dokumentácia
