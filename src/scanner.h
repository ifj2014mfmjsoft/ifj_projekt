/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   scanner.h
 * Datum:   11.10. 2014
 * Autori:  Martin Cernek, xcerne01@stud.fit.vutbr.cz
 */

#ifndef __SCANER_H__
#define __SCANER_H__

#include <stdio.h>
#include "common.h"

extern char *keyword[]; //array of keywords

int getNextToken(void);

/**
 * \brief Checks command line arguments and opens source file.
 *
 * @return  0 on success
 *          1 on failure
 */
int open_File(int argc, char *argv[]);

void close_File(void);

//------------Tokens allowed in expressions----------------

/**
 * Token for identifier.
 */
#define TOKEN_IDENTIFIER 0

/**
 * Tokens for literals.
 */
#define TOKEN_LIT_INTEGER 1
#define TOKEN_LIT_REAL 2
#define TOKEN_LIT_STRING 3
#define TOKEN_LIT_BOOLEAN 4

/**
 * Tokens for other characters *1
 */
#define TOKEN_LEFT_PARENTHESIS 5    // '('
#define TOKEN_RIGHT_PARENTHESIS 6    // ')'

/**
 * Tokens for arithmetic operators.
 */
#define TOKEN_PLUS 7    // '+'
#define TOKEN_MINUS 8    // '-'
#define TOKEN_MULTIPLICATION 9    // '*'
#define TOKEN_DIVISION 10    // '/'

/**
 * Tokens for relational operators.
 */
#define TOKEN_LESS_THAN 11    // '<'
#define TOKEN_GREATER_THAN 12    // '>'
#define TOKEN_LESS_THAN_OR_EQUAL 13    // '<='
#define TOKEN_GREATER_THAN_OR_EQUAL 14    // '>='
#define TOKEN_EQUAL 15    // '='
#define TOKEN_NOT_EQUAL 16    // '<>'

//----------End of allowed tokens in expressions---------------

/**
 * Tokens for other characters *2
 */
#define TOKEN_COMMA 20    // ','
#define TOKEN_SEMICOLON 21    // ';'
#define TOKEN_COLON 22    // ':'
#define TOKEN_ASSIGNMENT 23    // ':='
#define TOKEN_END_OF_FILE  24
#define TOKEN_DOT  25 // '.' 

/**
 * Tokens for keywords.
 */
#define TOKEN_KW_BEGIN 26
#define TOKEN_KW_BOOLEAN 27
#define TOKEN_KW_DO 28
#define TOKEN_KW_ELSE 29
#define TOKEN_KW_END 30
#define TOKEN_KW_FALSE 31
#define TOKEN_KW_FORWARD 32
#define TOKEN_KW_FUNCTION 33
#define TOKEN_KW_IF 34
#define TOKEN_KW_INTEGER 35
#define TOKEN_KW_READLN 36
#define TOKEN_KW_REAL 37
#define TOKEN_KW_STRING 38
#define TOKEN_KW_THEN 39
#define TOKEN_KW_TRUE 40
#define TOKEN_KW_VAR 41
#define TOKEN_KW_WHILE 42
#define TOKEN_KW_WRITE 43
#define TOKEN_KW_REPEAT 44
#define TOKEN_KW_UNTIL 45

/**
 * States for automat.
 */
#define STATE_START 50
#define STATE_LESS_THAN 51    // '<'
#define STATE_GREATER_THAN 52    // '>'
#define STATE_COLON 53    // ':'
#define STATE_COMMENT 54    // ':'
#define STATE_IDENTIFIER 55
#define STATE_LIT_STRING1 56
#define STATE_LIT_STRING2 57
#define STATE_LIT_STRING3 58
#define STATE_LIT_STRING4 59
#define STATE_LIT_STRING5 60
#define STATE_LIT_INTEGER 61
#define STATE_LIT_REAL1 62
#define STATE_LIT_REAL2 63
#define STATE_LIT_REAL3 64
#define STATE_LIT_REAL4 65
#define STATE_LIT_REAL5 66
#define STATE_LIT_REAL6 67

/**
 * BASE states
 */

#define STATE_LIT_INTEGER_HEXADECIMAL1 68
#define STATE_LIT_INTEGER_HEXADECIMAL2 69
#define STATE_LIT_INTEGER_OCTAL1 70
#define STATE_LIT_INTEGER_OCTAL2 71
#define STATE_LIT_INTEGER_BINARY1 72
#define STATE_LIT_INTEGER_BINARY2 73
#define STATE_LIT_STRING_BINARY1 74
#define STATE_LIT_STRING_BINARY2 75
#define STATE_LIT_STRING_OCTAL1 76
#define STATE_LIT_STRING_OCTAL2 77
#define STATE_LIT_STRING_HEXADECIMAL1 78
#define STATE_LIT_STRING_HEXADECIMAL2 79


#endif
