/**
 * Projekt:     Projekt do predmetu IFJ
 * Popis:       Interpret jazyka IFJ14
 * Subor:       list.c
 * Datum:       16.11.2014
 * Autori:      Matej Vido, xvidom00@stud.fit.vutbr.cz
 */

#include "list.h"

/**
 * \brief Initializes list.
 */
t_list * list_init(void)
{
    t_list * new = (t_list *) calloc(1, sizeof(t_list));

    if (new == NULL)
        return NULL;

    new->first_ptr = NULL;
    new->last_ptr = NULL;

    return new;
}

/**
 * \brief Frees list.
 */
void list_free(t_list * list)
{
    t_list_item * tmp;

    if (list != NULL) {
        for (tmp = list->first_ptr; tmp != NULL; tmp = list->first_ptr) {
            list->first_ptr = tmp->next_ptr;
            free(tmp);
        }

        free(list);
    }
}

/**
 * \brief Inserts item in the end of list.
 */
int list_insert_last(t_list * list, t_tab_item * st_item_ptr)
{
    t_list_item * new = (t_list_item *) calloc(1, sizeof(t_list_item));

    if (new == NULL)
        return LIST_OP_FAIL;

    new->st_item_ptr = st_item_ptr;
    new->next_ptr = NULL;

    if (list->first_ptr == NULL) {
        list->first_ptr = new;
        list->last_ptr = new;
    } else {
        list->last_ptr->next_ptr = new;
        list->last_ptr = new;
    }

    return LIST_OP_OK;
}

/**
 * \brief Gets first item from list.
 */
t_tab_item * list_get_first(t_list * list)
{
    if (list->first_ptr == NULL)
        return NULL;
    else
        return list->first_ptr->st_item_ptr;
}

/**
 * \brief Frees first item in list.
 */
void list_free_first(t_list * list)
{
    if (list->first_ptr != NULL) {
        t_list_item * tmp = list->first_ptr;

        list->first_ptr = list->first_ptr->next_ptr;
        free(tmp);
    }
}
