/**
 * Projekt:     Projekt do predmetu IFJ
 * Popis:       Interpret jazyka IFJ14
 * Subor:       list.h
 * Datum:       16.11.2014
 * Autori:      Matej Vido, xvidom00@stud.fit.vutbr.cz
 */

#ifndef __LIST_H__
#define __LIST_H__

#include "ial.h"

enum list_op {
    LIST_OP_OK,
    LIST_OP_FAIL,
};

typedef struct t_list_item {
    t_tab_item * st_item_ptr;
    struct t_list_item * next_ptr;
} t_list_item;

typedef struct {
    t_list_item * first_ptr;
    t_list_item * last_ptr;
} t_list;

/**
 * \brief Initializes list.
 */
t_list * list_init(void);

/**
 * \brief Frees list.
 */
void list_free(t_list * list);

/**
 * \brief Inserts item in the end of list.
 */
int list_insert_last(t_list * list, t_tab_item * st_item_ptr);

/**
 * \brief Gets first item from list.
 */
t_tab_item * list_get_first(t_list * list);

/**
 * \brief Frees first item in list.
 */
void list_free_first(t_list * list);

#endif
