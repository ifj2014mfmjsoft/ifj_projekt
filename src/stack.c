/**
 * Projekt:     Projekt do predmetu IFJ
 * Popis:       Interpret jazyka IFJ14
 * Subor:       stack.c
 * Datum:       23.10.2014
 * Autori:      Martin Cernek, xcerne01@stud.fit.vutbr.cz
 */

#include "stack.h"

/**
 * \brief Function initializes new stack.
 * @param   S         pointer to t_stack structute
 * @return  STACK_INIT_FAIL  if allocation failed
 *          STACK_INIT_OK    if success
 */
int init_stack(t_stack *S) 
{
    S->array = (uint8_t *) malloc(sizeof(uint8_t) * STACK_ALLOC_SIZE);
    
    if (S->array == NULL)
        return STACK_INIT_FAIL;
    
    S->stack_top = 0;
    S->alloc_size = STACK_ALLOC_SIZE;

    return STACK_INIT_OK;
}

/**
 * \brief Function frees memory of stack.
 * @param   S         pointer to t_stack structute
 */
void free_stack(t_stack *S) 
{
    free(S->array);
}

/**
 * \brief Function push onto stack item.
 * @param   S         pointer to t_stack structute
 * @param   source    pointer to pushed item
 * @param   size      size in bytes of pushed item
 * @return  STACK_PUSH_FAIL  if allocation failed
 *          STACK_PUSH_OK    if success
 */
int push(t_stack *S, void *source, int size)
{
    if ((S->stack_top + size) > S->alloc_size) {
        
        uint8_t *tmptr_array = (uint8_t *) realloc(S->array, S->alloc_size + STACK_ALLOC_SIZE);
        
        if (tmptr_array == NULL) {
            free_stack(S);
            return STACK_PUSH_FAIL;
        }
        
        S->array = tmptr_array;
        S->alloc_size = S->alloc_size + STACK_ALLOC_SIZE;
    }

    if (source != NULL) 
        memcpy(S->array + S->stack_top, source, size);

    S->stack_top = S->stack_top + size;
    return STACK_PUSH_OK;
}

/**
 * \brief Function pop from stack item.
 * @param   S            pointer to t_stack structute
 * @param   destination  pointer to poped item
 * @param   size         size in bytes of poped item
 * @return  STACK_POP_FAIL  if stack is empty
 *          STACK_POP_OK    if success
 */
int pop(t_stack *S, int size) 
{
    if (empty(S))
        return STACK_POP_FAIL;
    S->stack_top = S->stack_top - size;
    return STACK_POP_OK;
}

/**
 * \brief Function return from stack value of top item.
 * @param   S            pointer to t_stack structute
 * @param   destination  pointer to destination where the value has been returned
 * @param   size         size in bytes of returned value
 * @return  STACK_TOP_FAIL  if stack is empty
 *          STACK_TOP_OK    if success
 */
int top(t_stack *S, void *destination, int size)
{
    if (empty(S))
        return STACK_TOP_FAIL;
    memcpy(destination, S->array + S->stack_top - size, size);
    return STACK_TOP_OK;
}

/**
 * \brief Function return from stack value of item by index from top of stack.
 * @param   S            pointer to t_stack structute
 * @param   destination  pointer to destination where the value has been returned
 * @param   size         size in bytes of returned value
 * @param   index        index from top of stack
 * @return  STACK_GFI_FAIL  if stack is empty
 *          STACK_GFI_OK    if success
 */
int get_from_index(t_stack *S, void *destination, int size, int index)
{
    if (empty(S) || (((int) S->stack_top - (index + 1) * size) < 0))
        return STACK_GFI_FAIL;
    memcpy(destination, S->array + S->stack_top - (index + 1) * size, size);
    return STACK_GFI_OK;
}

/**
 * \brief Function rewrite value of item by index from top of stack.
 * @param   S            pointer to t_stack structute
 * @param   source       pointer to new value
 * @param   size         size in bytes of rewrited value
 * @param   index        index from top of stack
 * @return  STACK_RII_FAIL  if stack is empty
 *          STACK_RII_OK    if success
 */
int rewrite_in_index(t_stack *S, void *source, int size, int index)
{
    if (empty(S) || (((int) S->stack_top - (index + 1) * size) < 0))
        return STACK_RII_FAIL;
    memcpy(S->array + S->stack_top - (index + 1) * size, source, size);
    return STACK_RII_OK;
}

/**
 * \brief Function determines if stack is empty.
 * @param   S            pointer to t_stack structute
 * @return  true     if stack is empty
 *          false    if success
 */
bool empty(t_stack *S)
{
    return S->stack_top == 0;
}
