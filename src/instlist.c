/**
 * Projekt:	 	Projekt do predmetu IFJ
 * Popis:   	Interpret jazyka IFJ14
 * Subor:   	instlist.c
 * Datum:   	23.10.2014
 * Autori:  	Frederik Muller, xmulle20@stud.fit.vutbr.cz
 *              Martin Cernek, xcerne01@stud.fit.vutbr.cz
 */

#include "instlist.h"
#include "list.h"

/**
 * \brief Function initializes new list
 * @param   L    pointer to the instruction list
 */
void instr_list_init(t_instr_list *L)
{
    L->first = NULL; 
    L->active = NULL;
    L->last = NULL;
    L->program_start = NULL;
}

/**
 * \brief Function disposes list
 * @param   L    pointer to the instruction list
 */
void instr_list_dispose(t_instr_list *L)
{
    t_instr_list_item *tmp_item;  
    while (L->first != NULL) {
        tmp_item = L->first;
        L->first = L->first->next_item;

        if (tmp_item->instruction.instr_type == I_FUN_CALL) {
            list_free(tmp_item->instruction.address2);
        } else if (tmp_item->instruction.instr_type == I_WRITE) {
            list_free(tmp_item->instruction.address1);
        }
        free(tmp_item);
    }
    L->first = NULL;
    L->active = NULL;
    L->last = NULL;
    L->program_start = NULL;
}

/**
 * \brief Function inserts new item in the end of list
 * @param   L    pointer to the instruction list
 * @param   I    inserted instruction
 * @return  LIST_OPERATION_OK    if success
 *          LIST_OPERATION_FAIL  if allocation failed
 */
int instr_list_insert_last(t_instr_list *L, t_instruction I)
{
    t_instr_list_item *tmp_item;
    if ((tmp_item = malloc(sizeof(struct instr_list_item))) == NULL) {
        return LIST_OPERATION_FAIL;
    }
    tmp_item->instruction = I;
    tmp_item->next_item = NULL;
    if (L->first == NULL)
        L->first = tmp_item;
    else
        L->last->next_item = tmp_item;
    L->last = tmp_item;
    return LIST_OPERATION_OK;
}

/**
 * \brief Function gives activity to the first item in list
 * @param   L    pointer to the instruction list
 */
void instr_list_first(t_instr_list *L)
{
    L->active = L->first;
}

/**
 * \brief Function gives activity to the first instruction of program.
 * @param   L    pointer to the instruction list
 */
void instr_list_program_start(t_instr_list *L)
{
    L->active = L->program_start;
}

/**
 * \brief Function sets program start
 * @param   L                pointer to the instruction list
 * @param   program_start    pointer to item represents the first instruction of main program
 */
void instr_list_set_program_start(t_instr_list *L, t_instr_list_item *program_start)
{
    L->program_start = program_start;
}

/**
 * \brief Function gives activity to the next item in list
 * @param   L    pointer to the instruction list
 */
void instr_list_next(t_instr_list *L)
{
    if (L->active != NULL)
        L->active = L->active->next_item;
}

/**
 * \brief Function gives activity to the item according to second parameter
 * @param   L             pointer to the instruction list
 * @param   goto_instr    pointer to the item where go to
 */
void instr_list_goto(t_instr_list *L, void *goto_instr)
{
	L->active = (t_instr_list_item*) goto_instr;
}

/**
 * \brief Function returns pointer to the last instruction
 * @param   L    pointer to the instruction list
 * @return       pointer to the last instruction item
 */
void *instr_list_get_last(t_instr_list *L)
{
    return (void*) L->last;
}

/**
 * \brief Function returns instruction of active item
 * @param   L    pointer to the instruction list
 * @return       pointer to the active instruction item
 */
t_instr_list_item *instr_list_get_instruction_item(t_instr_list *L)
{
	if (L->active == NULL)
		return NULL;
	else
		return L->active;
}
