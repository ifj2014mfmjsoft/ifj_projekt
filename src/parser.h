/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   parser.h
 * Datum:   24.10. 2014
 * Autori:  Matej Vido, xvidom00@stud.fit.vutbr.cz
 */

#ifndef __PARSER_H__
#define __PARSER_H__

#include "list.h"
#include "ial.h"
#include "common.h"

//due to debug messages
#if __STDC_VERSION__ < 199901L
# if __GNUC__ >= 2
#  define __func__ __FUNCTION__
# else
#  define __func__ "<unknown>"
# endif
#endif

/**
 * Global variable used for generating keys for constants.
 */
extern uint32_t constant_counter;

/**
 * \brief Function generates key for constant.
 *
 * @param string Pointer to initialized t_string structure.
 */
void generate_key(t_string * string);

/**
 * \brief Function generates instruction and inserts it in instruction list.
 *
 * @param   type    Type of instruction.
 * @param   ptr1    First pointer, see instruction details.
 * @param   ptr2    Second pointer.
 * @param   ptr3    Third pointer.
 *
 * @return  LIST_OPERATION_OK     if success
 *          LIST_OPERATION_FAIL   if failure
 */
int generate_instruction(int type, void * ptr1, void * ptr2, void * ptr3);

/**
 * @return Error code according to ecodes from common.h
 */
int parse(void);

/*** Following functions represent nonterminals from LL-grammar. ***/

/**
 * @return Error code according to ecodes from common.h
 */
int program(void);

/**
 * @return Error code according to ecodes from common.h
 */
int glob_var_def(void);

/**
 * @return Error code according to ecodes from common.h
 */
int glob_var_def_list(void);

/**
 * @return Error code according to ecodes from common.h
 */
int glob_var_def_list_part(void);

/**
 * @return Error code according to ecodes from common.h
 */
int fun_def_list(void);

/**
 * @return Error code according to ecodes from common.h
 */
int main_body(void);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 *
 * @return Error code according to ecodes from common.h
 */
int param_list(t_tab_item * fun_ptr);

/**
 * @param  type     Pointer where number representing type will be set.
 *
 * @return Error code according to ecodes from common.h
 */
int type_term(int * type);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 *
 * @return Error code according to ecodes from common.h
 */
int fun_decl(t_tab_item * fun_ptr);

/**
 * @param  fun_ptr              Pointer points to function record of function which body is being 
 *                              processed or NULL if main body is being processed.
 * @param  next_param_num       Number of next parameter (starting from zero).
 * @param  previous_next_param  Pointer to pointer to next parameter from previous parameter.
 *
 * @return Error code according to ecodes from common.h
 */
int param(t_tab_item * fun_ptr, int next_param_num, t_tab_item ** previous_next_param);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 *
 * @return Error code according to ecodes from common.h
 */
int loc_var_def(t_tab_item * fun_ptr);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 *
 * @return Error code according to ecodes from common.h
 */
int loc_var_def_list(t_tab_item * fun_ptr);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 * @param  previous_next_var  Pointer to pointer to next variable from previous variable.
 *
 * @return Error code according to ecodes from common.h
 */
int loc_var_def_list_part(t_tab_item * fun_ptr, t_tab_item ** previous_next_var);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 *
 * @return Error code according to ecodes from common.h
 */
int statement_list(t_tab_item * fun_ptr);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 *
 * @return Error code according to ecodes from common.h
 */
int statement(t_tab_item * fun_ptr);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 *
 * @return Error code according to ecodes from common.h
 */
int else_nonterm(t_tab_item * fun_ptr);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 *
 * @return Error code according to ecodes from common.h
 */
int statement_list_part(t_tab_item * fun_ptr);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 *
 * @return Error code according to ecodes from common.h
 */
int complex_statement(t_tab_item * fun_ptr);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 * @param  list     Pointer to list of input parameters.
 *
 * @return Error code according to ecodes from common.h
 */
int term_list(t_tab_item * fun_ptr, t_list * list);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 * @param  list     Pointer to list of input parameters.
 *
 * @return Error code according to ecodes from common.h
 */
int term(t_tab_item * fun_ptr, t_list * list);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 * @param  list     Pointer to list of input parameters.
 *
 * @return Error code according to ecodes from common.h
 */
int term_list_part(t_tab_item * fun_ptr, t_list * list);

/**
 * @param  fun_ptr      Pointer points to function record of function which body is being 
 *                      processed or NULL if main body is being processed.
 * @param  fun_to_call  Pointer to function record of function which will be called.
 *
 * @return Error code according to ecodes from common.h
 */
int function_call(t_tab_item * fun_ptr, t_tab_item * fun_to_call);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 * @param  list     Pointer to list of input parameters.
 *
 * @return Error code according to ecodes from common.h
 */
int input_param_list(t_tab_item * fun_ptr, t_list * list);

/**
 * @param  fun_ptr  Pointer points to function record of function which body is being 
 *                  processed or NULL if main body is being processed.
 * @param  list     Pointer to list of input parameters.
 *
 * @return Error code according to ecodes from common.h
 */
int input_param_list_part(t_tab_item * fun_ptr, t_list * list);


#endif
