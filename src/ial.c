/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   ial.c
 * Datum:   6.10. 2014
 * Autori:  Matej Vido, xvidom00@stud.fit.vutbr.cz
 *          Martin Cernek, xcerne01@stud.fit.vutbr.cz
 */

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <stdbool.h>

#include "common.h"
#include "ial.h"
#include "string.h"

/**
 * \brief Function initializes table.
 * 
 * @param   table_size      Size of the table, must be power of two.
 * 
 * @return  Pointer to structure with table.
 */
t_table * table_init(uint64_t table_size)
{
    //power of two
    if (!(table_size && !(table_size & (table_size - 1))))
        return NULL;

    t_table * new_table = (t_table *) malloc(sizeof(t_table) + sizeof(t_tab_item *) * table_size);
    if (new_table == NULL)
        return NULL;

    memset(new_table, 0, sizeof(t_table) + sizeof(t_tab_item *) * table_size);

    new_table->size = table_size;

    return new_table;
}

/**
 * \brief Function frees memory and destroys table.
 *
 * @param   table   Pointer to structure of table, which will be freed.
 */
void table_destroy(t_table * table)
{
    uint64_t i;
    t_tab_item * tmp;
    t_tab_item * tmp2;

    if (table != NULL) {
        for (i = 0; i < table->size; i++) {
            for (tmp = table->items[i]; tmp != NULL; tmp = tmp2) {
                stringFree(&tmp->key);
                switch (tmp->data.type) {
                case DATA_TYPE_VAR:
                    if (tmp->data.value.variable.type == TYPE_STR) {
                        stringFree(&tmp->data.value.variable.value.string);
                    }
                    break;
                case DATA_TYPE_FUN:
                case DATA_TYPE_FUN_LEN:
                case DATA_TYPE_FUN_CPY:
                case DATA_TYPE_FUN_FND:
                case DATA_TYPE_FUN_SRT:
                    table_destroy(tmp->data.value.function.loc_table);
                    break;
                case DATA_TYPE_CONST:
                    if (tmp->data.value.constant.type == TYPE_STR) {
                        stringFree(&tmp->data.value.constant.value.string);
                    }
                    break;
                }
                tmp2 = tmp->next;
                free(tmp);
            }
        }
        free(table);
    }
}

/**
 * \brief Function lookups item and if not found, inserts it.
 *
 * Pointer to data is set whether new item is inserted or item already is in table.
 *
 * @param   table   Pointer to table structure.
 * @param   key     Key of item which will be inserted.
 * @param   item    Pointer where the pointer to inserted item will be stored.
 *
 * @return  TAB_INSERT_OK       If item was succesfully inserted.
 *          TAB_INSERT_ALL_ERR  If memory for new item could not be allocated.
 *          TAB_INSERT_FOUND    If item with such key already is in table.
 */
int table_insert(t_table * table, t_string * key, t_tab_item **item)
{
    uint64_t table_row = (table->size - 1) & murmur_hash_3(key->string, (uint32_t) key->length);
    t_tab_item * new;

    if (table_search_in_row(table, key, item, table_row) == TAB_SEARCH_OK)
        return TAB_INSERT_FOUND;

    new = calloc(1,sizeof(t_tab_item));
    if (new == NULL)
        return TAB_INSERT_ALL_ERR;

    new->next = table->items[table_row];
    if (stringDuplicate(&new->key, key) != STR_OPERATION_OK) {
        free(new);
        return TAB_INSERT_ALL_ERR;
    }
    
    if (item != NULL)
        *item = new;

    table->items[table_row] = new;

    return TAB_INSERT_OK;
}

/**
 * \brief Function looks for item.
 *
 * @param   table   Pointer to table structure.
 * @param   key     Key of searched item.
 * @param   item    Pointer where pointer to found item will be stored. 
 *                  If item is not found, this pointer has undefined value.
 *
 * @return  TAB_SEARCH_OK   If item is found.
 *          TAB_SEARCH_FAIL If item is not found.
 */
int table_search(t_table * table, t_string * key, t_tab_item **item)
{
    uint64_t table_row = (table->size - 1) & murmur_hash_3(key->string, (uint32_t) key->length);
    return table_search_in_row(table, key, item, table_row);
}

/**
 * \brief Functions provides sorting of strings.
 *
 * @param   source        Pointer to string.
 *          destination   Pointer to string, where sorted string will be stored.
 *
 * @return  HEAPSORT_OP_OK     If success.
 *          HEAPSORT_OP_FAIL   If duplication of new new string failed.
 */
int heap_sort(t_string *source, t_string *destination)
{

    if (stringDuplicate(destination, source) != STR_OPERATION_OK){
        return HEAPSORT_OP_FAIL;
    }

    char swap;
    int n = destination->length - 1;
    int left = n / 2;
    int right = n;

    for (int i = left; i >= 0; i--) {
        sift_down(destination, i, right);
    }

    for (right = n; right >= 1; right--) {
        swap = destination->string[0];
        destination->string[0] = destination->string[right];
        destination->string[right] = swap;
        sift_down(destination, 0, right - 1);
    }

    return HEAPSORT_OP_OK;
}

/**
 * \brief Required function for heapsort.
 * @param   string   Pointer to string.
 * @param   left     Index of the leftmost node.
 * @param   right    Index of the rightmost node.
 */
void sift_down(t_string *string, int left, int right)
{   
    int i = left;
    int j = 2 * i;
    char tmp_char = string->string[i];
    bool cont = j <= right;
    
    while (cont) {
        if (j < right)
            if (string->string[j] < string->string[j + 1]) {
                j = j + 1;
            } // if
        if (tmp_char >= string->string[j])
            cont = false;
        else {
            string->string[i] = string->string[j];
            i = j;
            j = 2 * i;
            cont = j <= right;
        } // else
    } // while
    string->string[i] = tmp_char;
}

/**
 * \brief Function finds substring in string and returns its position.
 *        If substring is not found, then function returns -1.
 *
 * @param   string      Pointer to string.
 * @param   substring   Pointer to substring.
 *
 * @return  Index of first occurrence substring in string.
 */
int find(t_string *string, t_string *substring)
{
    int fail_vector[substring->length];
    int k, r;

    fail_vector[0] = -1;
    for (k = 1; k  < substring->length; k++) {
        r = fail_vector[k-1];
        while ((r > -1) && (substring->string[r] != substring->string[k-1]))
            r = fail_vector[r];
        fail_vector[k] = r + 1;
    }

    int string_index = 0;
    int substring_index = 0;

    while ((string_index < string->length) && (substring_index < substring->length)) {
        if ((substring_index == -1) || (string->string[string_index] == substring->string[substring_index])) {
            string_index++;
            substring_index++;
        } // if
        else 
            substring_index = fail_vector[substring_index];
    } // while
    if (substring_index == substring->length)
        return string_index - substring->length;
    else
        return -1;
}
