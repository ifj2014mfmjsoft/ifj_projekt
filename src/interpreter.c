/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   interpreter.c
 * Datum:   25.10.2014
 * Autori:  Martin Cernek, xcerne01@stud.fit.vutbr.cz
 */

#include "interpreter.h"
#include "common.h"
#include "list.h"
#include <string.h>


/**
 * \brief  Function directly executes instructions from instruction list. 
 * @return  E_OK            if success
 *          E_SEM_TYPE      if operation contains incorrect types
 *          E_RUN_INPUT     if 'readln' read incorrect value
 *          E_RUN_N_INIT    if used value is uninitialized
 *          E_RUN_Z_DIV     if the second operand of division is zero 
 *          E_RUN_OTHER     if if occur others errors
 *          E_RUN_INTERN    if allocation failure and etc.
 */
int interpret(void)
{
    /* Accumulator register */
    t_acc_register accumulator;
    /* Instruction stack */
    t_stack instruction_stack;
    /* Stack for local variables and parameters */
    t_stack pv_stack;
    /* Temporary instruction item pointer */
    t_instr_list_item *tmp_instr_item;
    /* Temporary table item pointer */
    t_list_item *tmp_list_ptr;
    /* Temporary item in stack for local variables and parameters */
    t_pv_stack_item tmp_pv_stack_item;
    /* Variable for return values */
    int ret;
    
    /* Initialization of stacks */
    ret = init_stack(&instruction_stack);
    if (ret != STACK_INIT_OK) {
        return E_INTERN;
    }
    ret = init_stack(&pv_stack);
    if (ret != STACK_INIT_OK) {
        free_stack(&instruction_stack);
        return E_INTERN;
    }

    /* Assignment of type to register */
    accumulator.type = TYPE_UNINIT;
    /* Assignment of type to temporary stack item */
    tmp_pv_stack_item.type = TYPE_UNINIT;

    /* Setting up the start of program */ 
    instr_list_program_start(&instruction_list);

    while (1) {
        
        /* Getting item from instruction list */
        tmp_instr_item = instr_list_get_instruction_item(&instruction_list);
        
        /* Handling the validity of instruction list */
        if (tmp_instr_item == NULL) {
            free_sources(&instruction_stack, &pv_stack, &accumulator);
            return E_INTERN;
        }

        switch (tmp_instr_item->instruction.instr_type) {
            
            case I_START: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_START\n");
#endif

                // NOTHING TO DO                    

                break;
            } // I_START

            case I_STOP: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_STOP\n");
#endif

                free_sources(&instruction_stack, &pv_stack, &accumulator);
                return E_OK;

            } // I_STOP

            case I_FUN_LENGTH: {
                        
#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_FUN_LENGTH\n");
#endif

                /* Temporary variable for store string length */
                int tmp_length;

                switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.type) {
                    
                    case DATA_TYPE_VAR: {
                        if (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_length = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.string.length;
                        break; 
                    } // DATA_TYPE_VAR
                            
                    case DATA_TYPE_CONST: {
                        tmp_length = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value.string.length;
                        break; 
                    } // DATA_TYPE_CONST

                    case DATA_TYPE_FUN_ITEM: {
                        ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.top_stack_index);
                        if (ret != STACK_GFI_OK) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        if (tmp_pv_stack_item.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_length = tmp_pv_stack_item.value.string.length;
                        break; 
                    } // DATA_TYPE_FUN_ITEM

                    default: {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN; // Incorrect type
                    } // DEFAULT

                } // END SWITCH

                clear_register(&accumulator);
                accumulator.value.integer = tmp_length;
                accumulator.type = TYPE_INT;
                        
                break;
            } // I_FUN_LENGTH

            case I_FUN_COPY: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_FUN_COPY\n");
#endif
                
                /* Temporary variable for storing string */
                t_string tmp_string;
                /* Temporary variable for storing substring of string */
                char *tmp_substring;
                /* Temporary variable for storing index */
                int tmp_index;
                /* Temporary variable for storing length of copy substring */
                int tmp_length;

                switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.type) {  // FIRST ADDRESS
                           
                    case DATA_TYPE_VAR: {
                        if (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_string = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.string;
                        break; 
                    } // DATA_TYPE_VAR
                    
                    case DATA_TYPE_CONST: {
                        tmp_string = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value.string;
                        break; 
                    } // DATA_TYPE_CONST

                    case DATA_TYPE_FUN_ITEM: {
                        ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.top_stack_index);
                        if (ret != STACK_GFI_OK) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        if (tmp_pv_stack_item.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_string = tmp_pv_stack_item.value.string;
                        break; 
                    } // DATA_TYPE_FUN_ITEM

                    default: {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN; // Incorrect type
                    } // DEFAULT

                } // FIRST ADDRESS

                switch (((t_tab_item *) tmp_instr_item->instruction.address2)->data.type) {   // SECOND ADDRESS
                   
                    case DATA_TYPE_VAR: {
                        if (((t_tab_item *) tmp_instr_item->instruction.address2)->data.value.variable.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_index = ((t_tab_item *) tmp_instr_item->instruction.address2)->data.value.variable.value.integer - 1; // Indexing in pascal from 1
                        break; 
                    } // DATA_TYPE_VAR
                    
                    case DATA_TYPE_CONST: {
                        tmp_index = ((t_tab_item *) tmp_instr_item->instruction.address2)->data.value.constant.value.integer - 1; // Indexing in pascal from 1
                        break; 
                    } // DATA_TYPE_CONST

                    case DATA_TYPE_FUN_ITEM: {
                        ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address2)->data.value.fun_item.top_stack_index);
                        if (ret != STACK_GFI_OK) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        if (tmp_pv_stack_item.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_index = tmp_pv_stack_item.value.integer - 1; // Indexing in pascal from 1
                        break; 
                    } // DATA_TYPE_FUN_ITEM

                    default: {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN; // Incorrect type
                    } // DEFAULT

                } // SECOND ADDRESS

                switch (((t_tab_item *) tmp_instr_item->instruction.address3)->data.type) {   // THIRD ADDRESS
                   
                    case DATA_TYPE_VAR: {
                        if (((t_tab_item *) tmp_instr_item->instruction.address3)->data.value.variable.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_length = ((t_tab_item *) tmp_instr_item->instruction.address3)->data.value.variable.value.integer;
                        break; 
                    } // DATA_TYPE_VAR
                    
                    case DATA_TYPE_CONST: {
                        tmp_length = ((t_tab_item *) tmp_instr_item->instruction.address3)->data.value.constant.value.integer;
                        break; 
                    } // DATA_TYPE_CONST

                    case DATA_TYPE_FUN_ITEM: {
                        ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address3)->data.value.fun_item.top_stack_index);
                        if (ret != STACK_GFI_OK) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        if (tmp_pv_stack_item.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_length = tmp_pv_stack_item.value.integer;
                        break; 
                    } // DATA_TYPE_FUN_ITEM

                    default: {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN; // Incorrect type
                    }

                } // THIRD ADDRESS

                /* Handling special cases of index */
                if (tmp_index < 0)
                    tmp_index = 0;
                if ((tmp_length < 0) || (tmp_index >= tmp_string.length))
                    tmp_length = 0;
                
                if ((tmp_substring = (char*) malloc(tmp_length + 1)) == NULL) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return E_INTERN; // Allocation failed
                }

                strncpy(tmp_substring, tmp_string.string + tmp_index, tmp_length);
                tmp_substring[tmp_length] = '\0';

                clear_register(&accumulator);
                ret = stringInitValue(&(accumulator.value.string), tmp_substring);
                if (ret != STR_OPERATION_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return E_INTERN;
                }
                accumulator.type = TYPE_STR;

                free(tmp_substring);

                break;
            } // I_FUN_COPY
            
            case I_FUN_FIND: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_FUN_FIND\n");
#endif
                
                /* Temporary variable for storing string */
                t_string tmp_string;
                /* Temporary variable for storing searched substring */
                t_string tmp_substring;

                switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.type) {  // FIRST ADDRESS
                   
                    case DATA_TYPE_VAR: {
                        if (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_string = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.string;
                        break; 
                    } // DATA_TYPE_VAR
                    
                    case DATA_TYPE_CONST: {
                        tmp_string = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value.string;
                        break; 
                    } // DATA_TYPE_CONST

                    case DATA_TYPE_FUN_ITEM: {
                        ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.top_stack_index);
                        if (ret != STACK_GFI_OK) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        if (tmp_pv_stack_item.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_string = tmp_pv_stack_item.value.string;
                        break; 
                    } // DATA_TYPE_FUN_ITEM

                    default: {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN; // Incorrect type
                    } // DEFAULT

                } // FIRST ADDRESS

                switch (((t_tab_item *) tmp_instr_item->instruction.address2)->data.type) {   // SECOND ADDRESS
                   
                    case DATA_TYPE_VAR: {
                        if (((t_tab_item *) tmp_instr_item->instruction.address2)->data.value.variable.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_substring = ((t_tab_item *) tmp_instr_item->instruction.address2)->data.value.variable.value.string;
                        break; 
                    } // DATA_TYPE_VAR
                    
                    case DATA_TYPE_CONST: {
                        tmp_substring = ((t_tab_item *) tmp_instr_item->instruction.address2)->data.value.constant.value.string;
                        break; 
                    } // DATA_TYPE_CONST

                    case DATA_TYPE_FUN_ITEM: {
                        ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address2)->data.value.fun_item.top_stack_index);
                        if (ret != STACK_GFI_OK) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        if (tmp_pv_stack_item.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_substring = tmp_pv_stack_item.value.string;
                        break; 
                    } // DATA_TYPE_FUN_ITEM

                    default: {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN; // Incorrect type
                    } // DEFAULT

                } // SECOND ADDRESS

                clear_register(&accumulator);
                accumulator.value.integer = find(&tmp_string, &tmp_substring) + 1; // Indexing in pascal from 1
                accumulator.type = TYPE_INT;

                break;
            } // I_FUN_FIND
            
            case I_FUN_SORT: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_FUN_SORT\n");
#endif
                
                /* Temporary variable for storing sorted string */
                t_string tmp_string;

                switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.type) {
                   
                    case DATA_TYPE_VAR: {
                        if (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_string = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.string;
                        break; 
                    } // DATA_TYPE_VAR
                    
                    case DATA_TYPE_CONST: {
                        tmp_string = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value.string;
                        break; 
                    } // DATA_TYPE_CONST

                    case DATA_TYPE_FUN_ITEM: {
                        ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.top_stack_index);
                        if (ret != STACK_GFI_OK) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        if (tmp_pv_stack_item.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        tmp_string = tmp_pv_stack_item.value.string;
                        break; 
                    } // DATA_TYPE_FUN_ITEM

                    default: {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN; // Incorrect type
                    } // DEFAULT

                }
                
                clear_register(&accumulator);
                accumulator.type = TYPE_STR;
                ret = heap_sort(&tmp_string, &(accumulator.value.string));
                if (ret != HEAPSORT_OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return E_INTERN;
                }

                break;
            } // I_FUN_SORT
            
            case I_ASSIGN: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_ASSIGN\n");
#endif

                switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.type) {
                    case DATA_TYPE_VAR: {
                        if (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.type != accumulator.type) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.type) {
                            case TYPE_STR:
                                if (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.is_init == true)
                                    stringFree(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.string));
                                ret = stringDuplicate(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.string), &(accumulator.value.string));
                                if (ret != STR_OPERATION_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return E_INTERN;
                                }
                                break;
                            default:
                                ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value = accumulator.value;
                                break;
                        }
                        ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.is_init = true;
                        break; 
                    } // DATA_TYPE_VAR

                    case DATA_TYPE_CONST: {
                        if (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.type != accumulator.type) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.type) {
                            case TYPE_STR:
                                stringFree(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value.string));
                                ret = stringDuplicate(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value.string), &(accumulator.value.string));
                                if (ret != STR_OPERATION_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return E_INTERN;
                                }
                                break;
                            default:
                                ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value = accumulator.value;
                                break;
                        }
                        break; 
                    } // DATA_TYPE_CONST

                    case DATA_TYPE_FUN_ITEM: {
                        switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.type) {
                            case TYPE_STR:
                                ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.top_stack_index);
                                if (ret != STACK_GFI_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return E_INTERN;
                                }
                                if (tmp_pv_stack_item.is_init == true)
                                    stringFree(&tmp_pv_stack_item.value.string);
                                ret = stringDuplicate(&(tmp_pv_stack_item.value.string), &(accumulator.value.string));
                                if (ret != STR_OPERATION_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return E_INTERN;
                                }
                                break;
                            default:
                                tmp_pv_stack_item.value = accumulator.value;
                                break;
                        }
                        tmp_pv_stack_item.is_init = true;
                        tmp_pv_stack_item.type = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.type;
                        ret = rewrite_in_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.top_stack_index);
                        if (ret != STACK_RII_OK) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        break;
                    } // DATA_TYPE_FUN_ITEM

                    default: {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN; // Incorrect type
                    } // DEFAULT

                }

                break; 
            } // I_ASSIGN_EXPR
            
            case I_FUN_CALL: {

#ifdef DEBUG
                    fprintf(stderr, "instruction:\tI_FUN_CALL\n");
#endif

                /* Variable for access to local variables and parameters caller function */
                int num_of_pushed_items = 0;

                ret = push(&instruction_stack, &(tmp_instr_item->next_item), sizeof(tmp_instr_item)); // Push onto stack next instruction
                if (ret == STACK_PUSH_FAIL) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return E_INTERN;
                }
                tmp_list_ptr = ((t_list *) tmp_instr_item->instruction.address2)->first_ptr; // Get first item of list
                while (tmp_list_ptr != NULL) {
                    
                    switch (tmp_list_ptr->st_item_ptr->data.type) {
                   
                        case DATA_TYPE_VAR: {
                            if (tmp_list_ptr->st_item_ptr->data.value.variable.is_init != true) {
                                free_sources(&instruction_stack, &pv_stack, &accumulator);
                                return E_RUN_N_INIT; // Neinicializovana
                            }
                            tmp_pv_stack_item.is_init = true;
                            tmp_pv_stack_item.type = tmp_list_ptr->st_item_ptr->data.value.variable.type;
                            if (tmp_list_ptr->st_item_ptr->data.value.variable.type == TYPE_STR) {
                                ret = stringDuplicate(&(tmp_pv_stack_item.value.string), &(tmp_list_ptr->st_item_ptr->data.value.variable.value.string));
                                if (ret != STR_OPERATION_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return E_INTERN;
                                }        
                            }
                            else {
                                tmp_pv_stack_item.value = tmp_list_ptr->st_item_ptr->data.value.variable.value;
                            }
                            break; 
                        } // DATA_TYPE_VAR
                    
                        case DATA_TYPE_CONST: {
                            tmp_pv_stack_item.is_init = true;
                            tmp_pv_stack_item.type = tmp_list_ptr->st_item_ptr->data.value.constant.type;
                            if (tmp_list_ptr->st_item_ptr->data.value.constant.type == TYPE_STR) {
                                ret = stringDuplicate(&(tmp_pv_stack_item.value.string), &(tmp_list_ptr->st_item_ptr->data.value.constant.value.string));
                                if (ret != STR_OPERATION_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return E_INTERN;
                                }        
                            }
                            else {
                                tmp_pv_stack_item.value = tmp_list_ptr->st_item_ptr->data.value.variable.value;
                            }
                            break; 
                        } // DATA_TYPE_CONST

                        case DATA_TYPE_FUN_ITEM: {
                            t_pv_stack_item tmp_pv_stack_item2;
                            ret = get_from_index(&pv_stack, &tmp_pv_stack_item2, sizeof(tmp_pv_stack_item2), tmp_list_ptr->st_item_ptr->data.value.fun_item.top_stack_index + num_of_pushed_items);
                            if (ret != STACK_GFI_OK) {
                                free_sources(&instruction_stack, &pv_stack, &accumulator);
                                return E_INTERN;
                            }
                            if (tmp_pv_stack_item2.is_init != true) {
                                free_sources(&instruction_stack, &pv_stack, &accumulator);
                                return E_RUN_N_INIT;
                            }
                            tmp_pv_stack_item.is_init = true;
                            tmp_pv_stack_item.type = tmp_list_ptr->st_item_ptr->data.value.fun_item.type;
                            if (tmp_list_ptr->st_item_ptr->data.value.fun_item.type == TYPE_STR) {
                                ret = stringDuplicate(&(tmp_pv_stack_item.value.string), &(tmp_pv_stack_item2.value.string));
                                if (ret != STR_OPERATION_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return E_INTERN;
                                }        
                            }
                            else {
                                tmp_pv_stack_item.value = tmp_pv_stack_item2.value;
                            }
                            break; 
                        } // DATA_TYPE_FUN_ITEM

                        default: {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN; // Incorrect type
                        }

                    } // SWITCH

                    ret = push(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item));
                    if (ret != STACK_PUSH_OK) {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN;
                    }
                    num_of_pushed_items++;

                    tmp_list_ptr = tmp_list_ptr->next_ptr; // Get next list item

                } // WHILE

                t_tab_item *tmp_item = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.function.first_var;

                while (tmp_item != NULL) {
                    tmp_pv_stack_item.type = tmp_item->data.value.fun_item.type;
                    tmp_pv_stack_item.is_init = false;
                    ret = push(&pv_stack, &tmp_pv_stack_item, sizeof(t_pv_stack_item));
                    if (ret == STACK_PUSH_FAIL) {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN;
                    }
                    tmp_item = tmp_item->data.value.fun_item.next;
                }

                tmp_pv_stack_item.type = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.function.ret_type;
                tmp_pv_stack_item.is_init = false;
                ret = push(&pv_stack, &tmp_pv_stack_item, sizeof(t_pv_stack_item));
                if (ret == STACK_PUSH_FAIL) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return E_INTERN;
                }

                instr_list_goto(&instruction_list, ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.function.first_inst); // Set up first isntruction of function
                
                break;
            } // I_FUN_CALL

            case I_LABEL: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_LABEL\n");
#endif

                // NOTHING TO DO

                break;
            } // I_LABEL
            
            case I_RETURN: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_RETURN\n");
#endif

                ret = top(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item)); // Get return value of function
                if (ret != STACK_TOP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return E_INTERN;
                }
                ret = pop(&pv_stack, sizeof(tmp_pv_stack_item));
                if (ret != STACK_POP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return E_INTERN;
                }
                if (tmp_pv_stack_item.is_init != true) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return E_RUN_N_INIT;
                }
                clear_register(&accumulator);
                if (tmp_pv_stack_item.type == TYPE_STR) {
                    ret = stringDuplicate(&(accumulator.value.string), &(tmp_pv_stack_item.value.string));
                    if (ret != STR_OPERATION_OK) {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN;
                    }
                    stringFree(&(tmp_pv_stack_item.value.string));
                }
                else {
                    accumulator.value = tmp_pv_stack_item.value;
                }
                accumulator.type = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.function.ret_type;
                
                int counter = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.function.param_count + ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.function.var_count;
                while (counter != 0) {
                    ret = top(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item)); // Get return value of function
                    if (ret != STACK_TOP_OK) {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN;
                    }
                    if (tmp_pv_stack_item.type == TYPE_STR) 
                        stringFree(&(tmp_pv_stack_item.value.string));
                    ret = pop(&pv_stack, sizeof(tmp_pv_stack_item));
                    if (ret != STACK_POP_OK) {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN;
                    }
                    counter--;
                }
                ret = top(&instruction_stack, &tmp_instr_item, sizeof(tmp_instr_item)); // Get next instruction of main program
                if (ret != STACK_TOP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return E_INTERN;
                }
                ret = pop(&instruction_stack, sizeof(tmp_instr_item));
                if (ret != STACK_POP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return E_INTERN;
                }
                instr_list_goto(&instruction_list, tmp_instr_item); // Go to next instruction of main program

                break;
            } // I_RETURN
            
            case I_COND_JUMP: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_COND_JUMP\n");
#endif

                if (!(accumulator.value.boolean))
                    instr_list_goto(&instruction_list, tmp_instr_item->instruction.address1);

                break;
            } // I_COND_JUMP
            
            case I_JUMP: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_JUMP\n");
#endif

                instr_list_goto(&instruction_list, tmp_instr_item->instruction.address1);

                break;
            } // I_JUMP
            
            case I_READLN: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_READLN\n");
#endif

                switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.type) {
                   
                    case DATA_TYPE_VAR: {
                        
                        switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.type) {
                            
                            case TYPE_INT: {
                                ret = readln(TYPE_INT, &(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.integer));
                                if (ret != OP_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return ret;
                                }
                                break;
                            } // TYPE_INT

                            case TYPE_REAL: {
                                ret = readln(TYPE_REAL, &(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.real));
                                if (ret != OP_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return ret;
                                }
                                break;
                            } // TYPE_REAL

                            case TYPE_STR: {

                                if (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.is_init == true) {
                                    stringFree(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.string));
                                }
                                
                                ret = readln(TYPE_STR, &(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.string));
                                if (ret != OP_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return ret;
                                }
                                break;
                            } // TYPE_STR

                            default: {
                                free_sources(&instruction_stack, &pv_stack, &accumulator);
                                return E_SEM_TYPE; // Incorrect type
                            } // DEFAULT

                        } // SWITCH1

                        ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.is_init = true;
                        break; 
                    } // DATA_TYPE_VAR
                    
                    case DATA_TYPE_FUN_ITEM: {

                        switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.type) {
                            
                            case TYPE_INT: {
                                ret = readln(TYPE_INT, &tmp_pv_stack_item.value.integer);
                                if (ret != OP_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return ret;
                                }
                                tmp_pv_stack_item.type = TYPE_INT;
                                break;
                            } // TYPE_INT

                            case TYPE_REAL: {
                                ret = readln(TYPE_REAL, &tmp_pv_stack_item.value.real);
                                if (ret != OP_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return ret;
                                }
                                tmp_pv_stack_item.type = TYPE_REAL;
                                break;
                            } // TYPE_REAL

                            case TYPE_STR: {

                                ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.top_stack_index);
                                if (ret != STACK_GFI_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return E_INTERN;
                                }
                                if (tmp_pv_stack_item.is_init == true)
                                    stringFree(&tmp_pv_stack_item.value.string);

                                ret = readln(TYPE_STR, &tmp_pv_stack_item.value.string);
                                if (ret != OP_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return ret;
                                }
                                tmp_pv_stack_item.type = TYPE_STR;
                                break;
                            } // TYPE_STR

                            default: {
                                free_sources(&instruction_stack, &pv_stack, &accumulator);
                                return E_SEM_TYPE; // Incorrect type
                            } // DEFAULT

                        } // SWITCH2

                        tmp_pv_stack_item.is_init = true;
                        ret = rewrite_in_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.top_stack_index);
                        if (ret != STACK_RII_OK) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }

                        break;
                    } // DATA_TYPE_FUN_ITEM

                    default: {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN; // Incorrect type
                    } // DEFAULT

                } // SWITCH

                break;
            } // I_READLN

            case I_WRITE: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_WRITE\n");
#endif

                tmp_list_ptr = ((t_list *) tmp_instr_item->instruction.address1)->first_ptr; // Get first item of list

                while (tmp_list_ptr != NULL) {
                    
                    switch (tmp_list_ptr->st_item_ptr->data.type) {
                   
                        case DATA_TYPE_VAR: {
                            if (tmp_list_ptr->st_item_ptr->data.value.variable.is_init != true) {
                                free_sources(&instruction_stack, &pv_stack, &accumulator);
                                return E_RUN_N_INIT;
                            }
                            switch (tmp_list_ptr->st_item_ptr->data.value.variable.type) {
                                case TYPE_INT:
                                    printf("%d", tmp_list_ptr->st_item_ptr->data.value.variable.value.integer);
                                    break;
                                case TYPE_REAL:
                                    printf("%g", tmp_list_ptr->st_item_ptr->data.value.variable.value.real);
                                    break;
                                case TYPE_STR:
                                    printf("%s", tmp_list_ptr->st_item_ptr->data.value.variable.value.string.string);
                                    break;
                                case TYPE_BOOL:
                                    if (tmp_list_ptr->st_item_ptr->data.value.variable.value.boolean == true)
                                        printf("TRUE");
                                    else
                                        printf("FALSE");
                                    break;
                            } 
                            break; 
                        } // DATA_TYPE_VAR
                    
                        case DATA_TYPE_CONST: {
                            switch (tmp_list_ptr->st_item_ptr->data.value.constant.type) {
                                case TYPE_INT:
                                    printf("%d", tmp_list_ptr->st_item_ptr->data.value.constant.value.integer);
                                    break;
                                case TYPE_REAL:
                                    printf("%g", tmp_list_ptr->st_item_ptr->data.value.constant.value.real);
                                    break;
                                case TYPE_STR:
                                    printf("%s", tmp_list_ptr->st_item_ptr->data.value.constant.value.string.string);
                                    break;
                                case TYPE_BOOL:
                                    if (tmp_list_ptr->st_item_ptr->data.value.constant.value.boolean == true)
                                        printf("TRUE");
                                    else
                                        printf("FALSE");
                                    break;
                            } 
                            break; 
                        } // DATA_TYPE_CONST

                        case DATA_TYPE_FUN_ITEM: {
                            ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), tmp_list_ptr->st_item_ptr->data.value.fun_item.top_stack_index);
                            if (ret != STACK_GFI_OK) {
                                free_sources(&instruction_stack, &pv_stack, &accumulator);
                                return E_INTERN;
                            }
                            if (tmp_pv_stack_item.is_init != true) {
                                free_sources(&instruction_stack, &pv_stack, &accumulator);
                                return E_RUN_N_INIT;
                            }
                            switch (tmp_list_ptr->st_item_ptr->data.value.fun_item.type) {
                                case TYPE_INT:
                                    printf("%d", tmp_pv_stack_item.value.integer);
                                    break;
                                case TYPE_REAL:
                                    printf("%g", tmp_pv_stack_item.value.real);
                                    break;
                                case TYPE_STR:
                                    printf("%s", tmp_pv_stack_item.value.string.string);
                                    break;
                                case TYPE_BOOL:
                                    if (tmp_pv_stack_item.value.boolean == true)
                                        printf("TRUE");
                                    else
                                        printf("FALSE");
                                    break;
                            }
                            break; 
                        } // DATA_TYPE_FUN_ITEM

                        default: {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN; // Incorrect type
                        } // DEFAULT

                    } // SWITCH

                    tmp_list_ptr = tmp_list_ptr->next_ptr; // Get next item of list

                } // WHILE

                break;
            } // I_WRITE
            
            case I_LOAD: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_LOAD\n");
#endif

                switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.type) {
                    
                    case DATA_TYPE_VAR: {
                        if (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT; // Neinicializovana hodnota
                        }
                        clear_register(&accumulator);
                        accumulator.type = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.type;
                        switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.type) {
                            case TYPE_STR:
                                ret = stringDuplicate(&accumulator.value.string, &(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value.string));
                                if (ret != STR_OPERATION_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return E_INTERN;
                                }
                                break;
                            default:
                                accumulator.value = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.variable.value;
                                break;
                        }
                        break; 
                    } // DATA_TYPE_VAR
                    
                    case DATA_TYPE_CONST: {
                        clear_register(&accumulator);
                        accumulator.type = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.type;
                        switch (((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.type) {
                            case TYPE_STR:
                                ret = stringDuplicate(&accumulator.value.string, &(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value.string));
                                if (ret != STR_OPERATION_OK) {
                                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                                    return E_INTERN;
                                }
                                break;
                            default:
                                accumulator.value = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value;
                                break;
                        }
                        break;
                    } // DATA_TYPE_CONST

                    case DATA_TYPE_FUN_ITEM: {
                        ret = get_from_index(&pv_stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.top_stack_index);
                        if (ret != STACK_GFI_OK) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_INTERN;
                        }
                        if (tmp_pv_stack_item.is_init != true) {
                            free_sources(&instruction_stack, &pv_stack, &accumulator);
                            return E_RUN_N_INIT;
                        }
                        clear_register(&accumulator);
                        accumulator.type = ((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.fun_item.type;
                        if (accumulator.type == TYPE_STR){
                            ret = stringDuplicate(&(accumulator.value.string), &(tmp_pv_stack_item.value.string));
                            if (ret != STR_OPERATION_OK) {
                                free_sources(&instruction_stack, &pv_stack, &accumulator);
                                return E_INTERN;
                            }
                        }
                        else
                            accumulator.value = tmp_pv_stack_item.value;
                        break; 
                    } // DATA_TYPE_FUN_ITEM

                    default: {
                        free_sources(&instruction_stack, &pv_stack, &accumulator);
                        return E_INTERN;  // Incorrect type
                    } // DEFAULT

                }

                break;
            } // I_LOAD

            case I_NEG: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_NEG\n");
#endif

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_NEG, (t_tab_item *) tmp_instr_item->instruction.address2, NULL, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return ret;
                }

                break;
            } // I_NEG

            case I_ADD: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_ADD\n");
#endif    

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_ADD, (t_tab_item *) tmp_instr_item->instruction.address2, (t_tab_item *) tmp_instr_item->instruction.address3, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return ret;
                }

                break;
            } // I_ADD

            case I_SUB: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_SUB\n");
#endif

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_SUB, (t_tab_item *) tmp_instr_item->instruction.address2, (t_tab_item *) tmp_instr_item->instruction.address3, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return ret;
                }

                break;
            } // I_SUB

            case I_MUL:  {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_MUL\n");
#endif

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_MUL, (t_tab_item *) tmp_instr_item->instruction.address2, (t_tab_item *) tmp_instr_item->instruction.address3, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return ret;
                }

                break;
            } // I_MUL

            case I_DIV: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_DIV\n");
#endif

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_DIV, (t_tab_item *) tmp_instr_item->instruction.address2, (t_tab_item *) tmp_instr_item->instruction.address3, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return ret;
                }

                break;
            } // I_DIV

            case I_LESS_THAN: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_LESS_THAN\n");
#endif

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_LESS_THAN, (t_tab_item *) tmp_instr_item->instruction.address2, (t_tab_item *) tmp_instr_item->instruction.address3, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return ret;
                }

                break;
            } // I_LESS_THAN

            case I_GREATER_THAN: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_GREATER_THAN\n");
#endif

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_GREATER_THAN, (t_tab_item *) tmp_instr_item->instruction.address2, (t_tab_item *) tmp_instr_item->instruction.address3, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);                    
                    return ret;
                }

                break;
            } // I_GREATER_THAN

            case I_LESS_THAN_OR_EQUAL: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_LESS_THAN_OR_EQUAL\n");
#endif

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_LESS_THAN_OR_EQUAL, (t_tab_item *) tmp_instr_item->instruction.address2, (t_tab_item *) tmp_instr_item->instruction.address3, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return ret;
                }

                break;
            } // I_LESS_THAN_OR_EQUAL

            case I_GREATER_THAN_OR_EQUAL: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_GREATER_THAN_OR_EQUAL\n");
#endif

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_GREATER_THAN_OR_EQUAL, (t_tab_item *) tmp_instr_item->instruction.address2, (t_tab_item *) tmp_instr_item->instruction.address3, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return ret;
                }

                break;
            } // I_GREATER_THAN_OR_EQUAL

            case I_EQUAL: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_EQUAL\n");
#endif

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_EQUAL, (t_tab_item *) tmp_instr_item->instruction.address2, (t_tab_item *) tmp_instr_item->instruction.address3, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return ret;
                }

                break;
            } // I_EQUAL

            case I_NOT_EQUAL: {

#ifdef DEBUG
                fprintf(stderr, "instruction:\tI_NOT_EQUAL\n");
#endif

                ret = generate_op(&(((t_tab_item *) tmp_instr_item->instruction.address1)->data.value.constant.value), I_NOT_EQUAL, (t_tab_item *) tmp_instr_item->instruction.address2, (t_tab_item *) tmp_instr_item->instruction.address3, &pv_stack);
                if (ret != OP_OK) {
                    free_sources(&instruction_stack, &pv_stack, &accumulator);
                    return ret;
                }

                break;
            } // I_NOT_EQUAL
        }

        /* Moving to the next instruction */
        instr_list_next(&instruction_list);
    }
}


/**
 * \brief Function generates operation.
 * @param   result      pointer to where result of operations has to been stored
 * @param   op          type of operations
 * @param   tab_item1   pointer to the first operand in table of symbols
 * @param   tab_item2   pointer to the second operand in table of symbols
 * @param   stack       pointer to the stack
 * @return  OP_OK           if success
 *          E_RUN_N_INIT    if used value is uninitialized
 *          E_RUN_Z_DIV     if the second operand of division is zero 
 *          E_RUN_INTERN    if allocation failure and etc.
 */
int generate_op(t_tab_item_value *result, int op, t_tab_item *tab_item1, t_tab_item *tab_item2, t_stack *stack)
{
    /* Temporary item value of the first operand */
    t_tab_item_value tmp_item_value1;
    /* Temporary item value of the second operand */
    t_tab_item_value tmp_item_value2;
    /* Temporary stack item in stack for local variables and parameters */
    t_pv_stack_item tmp_pv_stack_item;
    /* Type of value of the first operand */
    int type1 = TYPE_UNINIT;
    /* Type of value of the second operand */
    int type2 = TYPE_UNINIT;
    /* Variable for return values */
    int ret;

    switch (tab_item1->data.type) {
        case DATA_TYPE_VAR: {
            if (tab_item1->data.value.variable.is_init != true)
                return E_RUN_N_INIT;
            type1 = tab_item1->data.value.variable.type;
            tmp_item_value1 = tab_item1->data.value.variable.value;
            break;
        } // DATA_TYPE_VAR
        
        case DATA_TYPE_CONST: {
            type1 = tab_item1->data.value.constant.type;
            tmp_item_value1 = tab_item1->data.value.constant.value;
            break;
        } // DATA_TYPE_CONST

        case DATA_TYPE_FUN_ITEM: {
            type1 = tab_item1->data.value.fun_item.type;
            ret = get_from_index(stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), tab_item1->data.value.fun_item.top_stack_index);
            if (ret != STACK_GFI_OK)
                return E_INTERN;
            if (tmp_pv_stack_item.is_init != true)
                return E_RUN_N_INIT;
            tmp_item_value1 = tmp_pv_stack_item.value;
            break;
        } // DATA_TYPE_FUN_ITEM

        default: {
            return E_INTERN;
            break;
        } // DEFAULT
    }

    if (tab_item2 != NULL) {
        switch (tab_item2->data.type) {
            case DATA_TYPE_VAR: {
                if (tab_item2->data.value.variable.is_init != true)
                    return E_RUN_N_INIT;
                type2 = tab_item2->data.value.variable.type;
                tmp_item_value2 = tab_item2->data.value.variable.value;
                break;
            } // DATA_TYPE_VAR
            
            case DATA_TYPE_CONST: {
                type2 = tab_item2->data.value.constant.type;
                tmp_item_value2 = tab_item2->data.value.constant.value;
                break;
            } // DATA_TYPE_CONST
    
            case DATA_TYPE_FUN_ITEM: {
                type2 = tab_item2->data.value.fun_item.type;
                ret = get_from_index(stack, &tmp_pv_stack_item, sizeof(tmp_pv_stack_item), tab_item2->data.value.fun_item.top_stack_index);
                if (ret != STACK_GFI_OK)
                    return E_INTERN;
                if (tmp_pv_stack_item.is_init != true)
                    return E_RUN_N_INIT;
                tmp_item_value2 = tmp_pv_stack_item.value;
                break;
            } // DATA_TYPE_FUN_ITEM
    
            default: {
                return E_INTERN;
                break;
            } // DEFAULT
        }
    }

    switch (op) {
        case I_NEG: {
            switch (type1) {
                case TYPE_INT:
                    result->integer = -tmp_item_value1.integer;
                    break;
                case TYPE_REAL:
                    result->real = -tmp_item_value1.real;
                    break;
                default: 
                    return E_SEM_TYPE;
            }
            break;
        } // I_NEG

        case I_ADD: {
            if (type1 == TYPE_STR && type2 == TYPE_STR) {
                stringFree(&(result->string));
                if (stringConcatenation(&(result->string), &tmp_item_value1.string, &tmp_item_value2.string) != STR_OPERATION_OK)
                    return E_INTERN; // Concatenation error
            }
            else if (type1 == TYPE_INT && type2 == TYPE_INT) {
                result->integer = tmp_item_value1.integer + tmp_item_value2.integer; 
            }
            else if (type1 == TYPE_REAL && type2 == TYPE_REAL) {
                result->real = tmp_item_value1.real + tmp_item_value2.real; 
            }
            else if (type1 == TYPE_REAL && type2 == TYPE_INT) {
                result->real = tmp_item_value1.real + tmp_item_value2.integer; 
            }
            else if (type1 == TYPE_INT && type2 == TYPE_REAL) {
                result->real = tmp_item_value1.integer + tmp_item_value2.real; 
            }
            else
                return E_SEM_TYPE; // Incorrect types
            break;
        } // I_ADD

        case I_SUB: {
            if (type1 == TYPE_INT && type2 == TYPE_INT) {
                result->integer = tmp_item_value1.integer - tmp_item_value2.integer; 
            }
            else if (type1 == TYPE_REAL && type2 == TYPE_REAL) {
                result->real = tmp_item_value1.real - tmp_item_value2.real; 
            }
            else if (type1 == TYPE_REAL && type2 == TYPE_INT) {
                result->real = tmp_item_value1.real - tmp_item_value2.integer; 
            }
            else if (type1 == TYPE_INT && type2 == TYPE_REAL) {
                result->real = tmp_item_value1.integer - tmp_item_value2.real; 
            }
            else
                return E_SEM_TYPE; // Incorrect types
            break;
        } // I_SUB

        case I_MUL: {
            if (type1 == TYPE_INT && type2 == TYPE_INT) {
                result->integer = tmp_item_value1.integer * tmp_item_value2.integer; 
            }
            else if (type1 == TYPE_REAL && type2 == TYPE_REAL) {
                result->real = tmp_item_value1.real * tmp_item_value2.real; 
            }
            else if (type1 == TYPE_REAL && type2 == TYPE_INT) {
                result->real = tmp_item_value1.real * tmp_item_value2.integer; 
            }
            else if (type1 == TYPE_INT && type2 == TYPE_REAL) {
                result->real = tmp_item_value1.integer * tmp_item_value2.real; 
            }
            else
                return E_SEM_TYPE; // Incorrect types
            break;
        } // I_MUL

        case I_DIV: {
            if (type1 == TYPE_INT && type2 == TYPE_INT) {
                if (tmp_item_value2.integer == 0)
                    return E_RUN_Z_DIV; // Division by zero
                result->real = (double) tmp_item_value1.integer / (double) tmp_item_value2.integer; 
            }
            else if (type1 == TYPE_REAL && type2 == TYPE_REAL) {
                if (tmp_item_value2.real == 0)
                    return E_RUN_Z_DIV; // Division by zero
                result->real = tmp_item_value1.real / tmp_item_value2.real; 
            }
            else if (type1 == TYPE_REAL && type2 == TYPE_INT) {
                if (tmp_item_value2.integer == 0)
                    return E_RUN_Z_DIV; // Division by zero
                result->real = tmp_item_value1.real / tmp_item_value2.integer; 
            }
            else if (type1 == TYPE_INT && type2 == TYPE_REAL) {
                if (tmp_item_value2.real == 0)
                    return E_RUN_Z_DIV; // Division by zero
                result->real = tmp_item_value1.integer / tmp_item_value2.real; 
            }
            else
                return E_SEM_TYPE; // Incorrect types
            break;
        } // I_DIV
    
        case I_LESS_THAN: {
            switch (type1) {
                case TYPE_INT:
                    result->boolean = tmp_item_value1.integer < tmp_item_value2.integer;
                    break;
                case TYPE_REAL:
                    result->boolean = tmp_item_value1.real < tmp_item_value2.real;
                    break;
                case TYPE_STR:
                    result->boolean = strcmp(tmp_item_value1.string.string, tmp_item_value2.string.string) < 0;
                    break;
                case TYPE_BOOL:
                    result->boolean = tmp_item_value1.boolean < tmp_item_value2.boolean;
                    break;
            }
            break;
        } // I_LESS_THAN

        case I_GREATER_THAN: {
            switch (type1) {
                case TYPE_INT:
                    result->boolean = tmp_item_value1.integer > tmp_item_value2.integer;
                    break;
                case TYPE_REAL:
                    result->boolean = tmp_item_value1.real > tmp_item_value2.real;
                    break;
                case TYPE_STR:
                    result->boolean = strcmp(tmp_item_value1.string.string, tmp_item_value2.string.string) > 0;
                    break;
                case TYPE_BOOL:
                    result->boolean = tmp_item_value1.boolean > tmp_item_value2.boolean;
                    break;
            }
            break;
        } // I_GREATER_THAN

        case I_LESS_THAN_OR_EQUAL: {
            switch (type1) {
                case TYPE_INT:
                    result->boolean = tmp_item_value1.integer <= tmp_item_value2.integer;
                    break;
                case TYPE_REAL:
                    result->boolean = tmp_item_value1.real <= tmp_item_value2.real;
                    break;
                case TYPE_STR:
                    result->boolean = strcmp(tmp_item_value1.string.string, tmp_item_value2.string.string) <= 0;
                    break;
                case TYPE_BOOL:
                    result->boolean = tmp_item_value1.boolean <= tmp_item_value2.boolean;
                    break;
            }
            break;
        } // I_LESS_THAN_OR_EQUAL

        case I_GREATER_THAN_OR_EQUAL: {
            switch (type1) {
                case TYPE_INT:
                    result->boolean = tmp_item_value1.integer >= tmp_item_value2.integer;
                    break;
                case TYPE_REAL:
                    result->boolean = tmp_item_value1.real >= tmp_item_value2.real;
                    break;
                case TYPE_STR:
                    result->boolean = strcmp(tmp_item_value1.string.string, tmp_item_value2.string.string) >= 0;
                    break;
                case TYPE_BOOL:
                    result->boolean = tmp_item_value1.boolean >= tmp_item_value2.boolean;
                    break;
            }
            break;
        } // I_GREATER_THAN_OR_EQUAL

        case I_EQUAL: {
            switch (type1) {
                case TYPE_INT:
                    result->boolean = tmp_item_value1.integer == tmp_item_value2.integer;
                    break;
                case TYPE_REAL:
                    result->boolean = tmp_item_value1.real == tmp_item_value2.real;
                    break;
                case TYPE_STR:
                    result->boolean = strcmp(tmp_item_value1.string.string, tmp_item_value2.string.string) == 0;
                    break;
                case TYPE_BOOL:
                    result->boolean = tmp_item_value1.boolean == tmp_item_value2.boolean;
                    break;
            }
            break;
        } // I_EQUAL

        case I_NOT_EQUAL: {
            switch (type1) {
                case TYPE_INT:
                    result->boolean = tmp_item_value1.integer != tmp_item_value2.integer;
                    break;
                case TYPE_REAL:
                    result->boolean = tmp_item_value1.real != tmp_item_value2.real;
                    break;
                case TYPE_STR:
                    result->boolean = strcmp(tmp_item_value1.string.string, tmp_item_value2.string.string) != 0;
                    break;
                case TYPE_BOOL:
                    result->boolean = tmp_item_value1.boolean != tmp_item_value2.boolean;
                    break;
            }
            break;
        } // I_NOT_EQUAL
    }

    return OP_OK;
}


/**
 * \brief Function reads values from stdin.
 * @param   type         type
 * @param   red_value   pointer to where result of reading has to been stored
 * @return  OP_OK           if success
 *          E_RUN_INPUT     if read value is incorrect
 *          E_RUN_INTERN    if allocation failure and etc.
 *          E_SEM_TYPE      if types are incompatible
 */
int readln(int type, void *read_value)
{   
    /* Read string from stdin */
    t_string read_string;
    /* Temporary string */
    t_string tmp_string;
    /* Bool variable indicates if is something readed except white spaces */
    bool is_number_read = false;
    /* Pointer to the end of conversion */
    char *errstr = 0;
    /* Variable for return values */
    int ret;
    /* Variable where read char is stored */
    int c;

    /* Initialization of string structures */
    ret = stringInit(&read_string);
    if (ret != STR_OPERATION_OK)
        return E_INTERN;
    ret = stringInit(&tmp_string);
    if (ret != STR_OPERATION_OK) {
        stringFree(&read_string);
        return E_INTERN;
    }

    /* Reading string char by char */
    while ((c = getchar()) != '\n' && c != EOF) {
        ret = stringAddChar(&read_string, c);
        if (ret != STR_OPERATION_OK) {
            stringFree(&tmp_string);
            stringFree(&read_string);
            return E_INTERN;
        }
    }

    if (type != TYPE_STR) {
        for (int i = 0; i < read_string.length; i++) {
            if (isspace(read_string.string[i]) != 0)                    // Removing white spaces.
            {
                if (is_number_read != true)
                    continue;
                else
                    break;
            }
            ret = stringAddChar(&tmp_string, read_string.string[i]);    // If its something else than white space, then its stored to tmp string.
            if (ret != STR_OPERATION_OK) {
                stringFree(&tmp_string);
                stringFree(&read_string);
                return E_INTERN;
            }
            is_number_read = true;
        }
        if (is_number_read != true) {                                   // If something is really read.
            stringFree(&tmp_string);
            stringFree(&read_string);
            return E_RUN_INPUT;
        }
    }

    switch (type) {
        case TYPE_INT:
            *((int *) read_value) = strtol(tmp_string.string, &errstr, 10);      // String to integer.
            if (*errstr != 0) {
                stringFree(&tmp_string);
                stringFree(&read_string);
                return E_RUN_INPUT;
            }
            break;
        case TYPE_REAL:
            *((double *) read_value) = strtod(tmp_string.string, &errstr);      // String to double.
             if (*errstr != 0) {
                stringFree(&tmp_string);
                stringFree(&read_string);
                return E_RUN_INPUT;
            }
            break;
        case TYPE_STR:
            ret = stringDuplicate((t_string *) read_value, &read_string);      // Duplicating string to destination.
            if (ret != STR_OPERATION_OK) {
                stringFree(&tmp_string);
                stringFree(&read_string);
                return E_INTERN;
            }
            break;
        default:
            stringFree(&tmp_string);
            stringFree(&read_string);
            return E_SEM_TYPE;
            break;
    }

    stringFree(&tmp_string);
    stringFree(&read_string);
    return OP_OK;
}


/**
 * \brief Function clear the register
 * @param   accumulator     pointer to the register
 */
void clear_register(t_acc_register *accumulator)
{
    if (accumulator->type == TYPE_STR)
        stringFree(&(accumulator->value.string));
    accumulator->type = TYPE_UNINIT;
}


/**
 * \brief Function frees sources.
 * @param   stack1           pointer to the first stack
 *          stack2           pointer to the second stack
 *          acc_register     pointer to the register
 */
void free_sources(t_stack *stack1, t_stack *stack2, t_acc_register *acc_register)
{
    clear_register(acc_register);
    free_stack(stack1);

    t_pv_stack_item tmp_item;
    while (!empty(stack2)) {                                 // Remove everything from stack.
        top(stack2, &tmp_item, sizeof(tmp_item));
        pop(stack2, sizeof(tmp_item));
        if (tmp_item.type == TYPE_STR && tmp_item.is_init == true)
            stringFree(&tmp_item.value.string);
    }
    free_stack(stack2);
}


/**
 * \brief Function print items of stack with local variables and parameters. Used for DEBUG.
 * @param   stack           pointer to the stack with local variables and parameters
 */
void print_stack(t_stack *stack)
{

    t_pv_stack_item tmp_item;
    int tmp_top = stack->stack_top / sizeof(tmp_item);
    int index = 0;

    if (stack->stack_top == 0)
        return;

    do {
        get_from_index(stack, &tmp_item, sizeof(tmp_item), index);
        printf("Index: %d\t", index);
        printf("Type: %d\t", tmp_item.type);
        if (tmp_item.is_init == true)
            printf("Is_init: TRUE\t");
        else
            printf("Is_init: FALSE\t");
        switch (tmp_item.type) {
        case TYPE_INT:
            printf("Value: %d", tmp_item.value.integer);
            break;
        case TYPE_REAL:
            printf("Value: %g", tmp_item.value.real);
            break;
        case TYPE_STR:
            printf("Value: %s", tmp_item.value.string.string);
            break;
        case TYPE_BOOL:
            if (tmp_item.value.boolean == true)
                printf("Value: TRUE");
            else
                printf("Value: FALSE");
            break;
        }
        printf("\n");

        index++;
    } while (tmp_top != index);
}
