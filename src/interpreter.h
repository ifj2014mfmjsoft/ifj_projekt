/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   interpreter.h
 * Datum:   25.10.2014
 * Autori:  Martin Cernek, xcerne01@stud.fit.vutbr.cz
 */

#ifndef __INTERPRET_H__
#define __INTERPRET_H__

#include "string.h"
#include "stack.h"
#include "ial.h"
#include <stdbool.h>
#include <ctype.h>


#define	OP_OK 0    // Generating of operations or reading completed successfully

/**
 * \brief Structure for register data type.
 */
typedef struct acc_register {
    int type;
    t_tab_item_value value;
} t_acc_register;

/**
 * \brief Structure for items in stack for local variables and parameters.
 */
typedef struct pv_stack_item {
    int type;
    bool is_init;
    t_tab_item_value value;
} t_pv_stack_item;

/**
 * \brief  Function directly executes instructions from instruction list. 
 * @return  E_OK            if success
 *          E_SEM_TYPE      if operation contains incorrect types
 *          E_RUN_INPUT     if 'readln' read incorrect value
 *          E_RUN_N_INIT    if used value is uninitialized
 *          E_RUN_Z_DIV     if the second operand of division is zero 
 *          E_RUN_OTHER     if if occur others errors
 *          E_RUN_INTERN    if allocation failure and etc.
 */
int interpret(void);

/**
 * \brief Function generates operation.
 * @param   result      pointer to where result of operations has to been stored
 * @param   op          type of operations
 * @param   tab_item1   pointer to the first operand in table of symbols
 * @param   tab_item2   pointer to the second operand in table of symbols
 * @param   stack       pointer to the stack
 * @return  OP_OK           if success
 *          E_RUN_N_INIT    if used value is uninitialized
 *          E_RUN_Z_DIV     if the second operand of division is zero 
 *          E_RUN_INTERN    if allocation failure and etc.
 */
int generate_op(t_tab_item_value *result, int op, t_tab_item *tab_item1, t_tab_item *tab_item2, t_stack *stack);

/**
 * \brief Function reads values from stdin.
 * @param   type         type
 * @param   red_value   pointer to where result of reading has to been stored
 * @return  OP_OK           if success
 *          E_RUN_INPUT     if read value is incorrect
 *          E_RUN_INTERN    if allocation failure and etc.
 *          E_SEM_TYPE      if types are incompatible
 */
int readln(int type, void *read_value);

/**
 * \brief Function clear the register
 * @param   accumulator     pointer to the register
 */
void clear_register(t_acc_register *accumulator);

/**
 * \brief Function frees sources.
 * @param   stack1           pointer to the first stack
 *          stack2           pointer to the second stack
 *          acc_register     pointer to the register
 */
void free_sources(t_stack *stack1, t_stack *stack2, t_acc_register *acc_register);

/**
 * \brief Function print items of stack with local variables and parameters. Used for DEBUG.
 * @param   stack           pointer to the stack with local variables and parameters
 */
void print_stack(t_stack *stack);

#endif
