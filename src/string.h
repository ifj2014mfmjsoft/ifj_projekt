/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   string.h
 * Datum:   12.10. 2014
 * Autori:  Jergus Jacko, xjacko03@stud.fit.vutbr.cz
 *          Matej Vido, xvidom00@stud.fit.vutbr.cz
 */

#ifndef __STRING_H__
#define __STRING_H__

#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#define STR_LEN_INIT 8  //Constant which specifies number of firstly memory allocation in bytes.
                        //When the memory will be filled during the loading of chars,
                        //the memory will be gradually reallocated to double sizes of previous allocation.

enum str_alloc {
    STR_ALLOC_OK,
    STR_ALLOC_FAIL,
};

enum str_operation {
    STR_OPERATION_OK,
    STR_OPERATION_FAIL,
};


/**
 * \brief Structure for String data type
 */
typedef struct {
    char* string;
    int length;
    int allocSize;
} t_string;


/**
 * \brief Function initializes new string
 * \param t_string struct, which will be initialized to default values
 * \return STR_ALLOC_FAIL if allocation failed or STR_OPERATION_OK if success
 */
int stringInit(t_string *str);

/**
 * \brief Function initializes new string with constant value.
 * \param   str         pointer to t_string structute, which will be initialized
 * \param   str_value   value to initialize
 * \return  STR_OPERATION_FAIL  if str_value is NULL
 *          STR_OPERATION_OK    if success
 *          STR_ALLOC_FAIL      if allocation failure
 */
int stringInitValue(t_string *str, const char *str_value);

/**
 * \brief Function sets string with constant value.
 * \param   str         pointer to t_string structute, which will be initialized
 * \param   str_value   value to initialize
 * \return  STR_OPERATION_FAIL  if str_value is NULL
 *          STR_OPERATION_OK    if success
 *          STR_ALLOC_FAIL      if allocation failure
 */
int stringSetValue(t_string *str, const char *str_value);

/**
 * \brief Function frees string from memory
 *        and set his values to zero
 */
void stringFree(t_string *str);

/**
 * \brief Function deletes the content of string,
 *        size of allocated memory will be unchanged.
 */
void stringClear(t_string *str);

/**
 * \brief Function adds char value to the end of string value
 */
int stringAddChar(t_string *str1, char c);

/**
 * \brief Functiom compares two strings.
 * \param pointer to t_string
 * \param pointer to t_string
 * \return integer, result of comparation.
 *         str1 > str2 return integer < 0
 *         str1 > str2 return integer > 0
 *         str1 == str 2 return integer ==  0
 */
int stringCmpString(t_string *str1, t_string *str2);

/**
 * \brief Function compares string with constant string.
 * \param pointer to t_string
 * \param constant string value
 * \return integer, result of comparation.
 */
int stringCmpConstString(t_string *str1, char* str2);

/**
 * \brief Function makes concatenation of two string values.
 * \param pointer to destination t_string, where the result of concatenation will be stored
 * \param pointer to first t_string
 * \param pointer to second t_string
 * \return STR_ALLOC_FAIL if allocation failed or STR_OPERATION_OK if success
 */
int stringConcatenation(t_string *result, t_string *str1, t_string *str2);

/**
 * \brief Function for copying string from source to destination t_string
 *        source and destination t_string structs must be initialized!!
 * \param pointer to destination t_string
 * \param pointer to source t_string
 * \return STR_ALLOC_FAIL if allocation failed or STR_OPERATION_OK if success
 */
int stringCopy(t_string *destination, t_string *source);

/**
 * \brief Function for duplicating string from source to uninitialized t_string duplicate
 * \param pointer to duplicate t_string
 * \param pointer to source t_string
 * \return STR_ALLOC_FAIL if allocation failed or STR_OPERATION_OK if success
 */
int stringDuplicate(t_string *duplicate, t_string *source);

#endif
