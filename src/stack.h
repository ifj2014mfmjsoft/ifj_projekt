/**
 * Projekt:     Projekt do predmetu IFJ
 * Popis:       Interpret jazyka IFJ14
 * Subor:       stack.h
 * Datum:       23.10.2014
 * Autori:      Martin Cernek, xcerne01@stud.fit.vutbr.cz
 */

#ifndef __STACK_H__
#define __STACK_H__

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define STACK_ALLOC_SIZE 1024 //Constant which specifies size of firstly memory allocation in bytes.
                              //When stack will be full, the memory will be reallocated and increased by this value.

enum stack_operations {
    STACK_INIT_OK,
    STACK_INIT_FAIL,
    STACK_PUSH_OK,
    STACK_PUSH_FAIL,
    STACK_POP_OK,
    STACK_POP_FAIL,
    STACK_TOP_OK,
    STACK_TOP_FAIL,
    STACK_GFI_OK,
    STACK_GFI_FAIL,
    STACK_RII_OK,
    STACK_RII_FAIL,
};   

/**
 * \brief Structure for Stack data type
 */
typedef struct t_stack_struct { 
    uint8_t *array;
    uint32_t stack_top;
    uint32_t alloc_size;
} t_stack;

/**
 * \brief Function initializes new stack.
 * \param   S         pointer to t_stack structute
 * \return  STACK_INIT_FAIL  if allocation failed
 *          STACK_INIT_OK    if success
 */
int init_stack(t_stack *S);

/**
 * \brief Function unallocated memory of stack.
 * \param   S         pointer to t_stack structute
 */
void free_stack(t_stack *S);

/**
 * \brief Function push onto stack item.
 * \param   S         pointer to t_stack structute
 * \param   source    pointer to pushed item
 * \param   size      size in bytes of pushed item
 * \return  STACK_PUSH_FAIL  if allocation failed
 *          STACK_PUSH_OK    if success
 */
int push(t_stack *S, void *source, int size);

/**
 * \brief Function pop from stack item.
 * \param   S            pointer to t_stack structute
 * \param   destination  pointer to poped item
 * \param   size         size in bytes of poped item
 * \return  STACK_POP_FAIL  if stack is empty
 *          STACK_POP_OK    if success
 */
int pop(t_stack *S, int size);

/**
 * \brief Function return from stack value of item.
 * \param   S            pointer to t_stack structute
 * \param   destination  pointer to destination where the value has been returned
 * \param   size         size in bytes of returned value
 * \return  STACK_TOP_FAIL  if stack is empty
 *          STACK_TOP_OK    if success
 */
int top(t_stack *S, void *destination, int size);

/**
 * \brief Function return from stack value of item by index from top of stack.
 * @param   S            pointer to t_stack structute
 * @param   destination  pointer to destination where the value has been returned
 * @param   size         size in bytes of returned value
 * @param   index        index from top of stack
 * @return  STACK_TOP_FAIL  if stack is empty
 *          STACK_TOP_OK    if success
 */
int get_from_index(t_stack *S, void *destination, int size, int index);

/**
 * \brief Function rewrite value of item by index from top of stack.
 * @param   S            pointer to t_stack structute
 * @param   source       pointer to new value
 * @param   size         size in bytes of rewrited value
 * @param   index        index from top of stack
 * @return  STACK_RII_FAIL  if stack is empty
 *          STACK_RII_OK    if success
 */
int rewrite_in_index(t_stack *S, void *source, int size, int index);

/**
 * \brief Function determines if stack is empty.
 * \param   S            pointer to t_stack structute
 * \return  true     if stack is empty
 *          false    if success
 */
bool empty(t_stack *S);

#endif
