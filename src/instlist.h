/**
 * Projekt:     Projekt do predmetu IFJ
 * Popis:       Interpret jazyka IFJ14
 * Subor:       instlist.h
 * Datum:       23.10.2014
 * Autori:      Frederik Muller, xmulle20@stud.fit.vutbr.cz
 *              Martin Cernek, xcerne01@stud.fit.vutbr.cz
 */

#ifndef __INSTLIST_H__
#define __INSTLIST_H__

#include <stdio.h>
#include <stdlib.h>

enum instructions {
    I_START,            // Start of program             NULL/NULL/NULL
    I_STOP,             // End of program               NULL/NULL/NULL
    I_FUN_LENGTH,       // Built-in function length     par/NULL/NULL
    I_FUN_COPY,         // Built-in function copy       par1/par2/par3
    I_FUN_FIND,         // Built-in function find       par/par/NULL
    I_FUN_SORT,         // Built-in function sort       par/NULL/NULL
    I_ASSIGN,           // Assignment expression        dst/NULL/NULL       instruction receives value from register
    I_FUN_CALL,         // Function call                src1/src2/NULL      src1 = address of function in table of symbols
                        //                                                  src2 = address to list of parameters
                        //                                                  instruction push in stack label of next instruction
    I_LABEL,            // Label                        NULL/NULL/NULL
    I_RETURN,           // Return from function         src/NULL/NULL       instruction pop from stack label of next instruction address
                        //                                                  src = addres of function
    I_COND_JUMP,        // Conditional jump             dst/NULL/NULL       dst = address of instruction where to jump 
                        //                                                  instruction receives conditional value from register
    I_JUMP,             // Jump                         dst/NULL/NULL       dst = address of instruction where to jump 
    I_READLN,           // Readln                       dst/NULL/NULL       dst = address where data has to been stored
    I_WRITE,            // Write                        src/NULL/NULL       src = address to list of terms
    I_LOAD,             // Load to register             src/NULL/NULL       src = adress of result expression
    I_NEG,              // Negation of value            dst/src/NULL        dst= adress, where new data has to been stored
                        //                                                  src = adress of data, which need to be negated
    I_ADD,              // Addition                     dst/src1/src2       \..
    I_SUB,              // Subtraction                  dst/src1/src2           >  dst = address where data has to been stored
    I_MUL,              // Multiplication               dst/src1/src2           >  src1/src2 = addresses of the operands
    I_DIV,              // Division                     dst/src1/src2       
    I_LESS_THAN,        //                              dst/src1/src2
    I_GREATER_THAN,     //                              dst/src1/src2
    I_LESS_THAN_OR_EQUAL,       //                      dst/src1/src2
    I_GREATER_THAN_OR_EQUAL,    //                      dst/src1/src2
    I_EQUAL,            //                              dst/src1/src2
    I_NOT_EQUAL,        //                              dst/src1/src2       /..
};


/**
 * Structure for instruction type
 */
typedef struct {
    int instr_type; // Instruction type
    void *address1; // Operand address
    void *address2; // Operand address
    void *address3; // Operand address
} t_instruction;

/**
 * Structure for item in list.
 */
typedef struct instr_list_item {
    t_instruction instruction; // Instruction
    struct instr_list_item *next_item; // Pointer to next instruction
} t_instr_list_item;

/**
 * Structure for list.
 */
typedef struct {
    t_instr_list_item *first; // Pointer to first instruction in list
    t_instr_list_item *active; // Pointer to active instruction in list
    t_instr_list_item *last; // Pointer to last instruction in list
    t_instr_list_item *program_start; // Pointer to first instruction of program (I_START)
} t_instr_list;

enum instr_list_operation {
    LIST_OPERATION_OK,
    LIST_OPERATION_FAIL,
};

/**
 * \brief Function initializes new list
 * @param   L    pointer to the instruction list
 */
void instr_list_init(t_instr_list *L);

/**
 * \brief Function disposes list
 * @param   L    pointer to the instruction list
 */
void instr_list_dispose(t_instr_list *L);

/**
 * \brief Function inserts new item in the end of list
 * @param   L    pointer to the instruction list
 * @param   I    inserted instruction
 * @return  LIST_OPERATION_OK    if success
 *          LIST_OPERATION_FAIL  if allocation failed
 */
int instr_list_insert_last(t_instr_list *L, t_instruction I);

/**
 * \brief Function gives activity to the first item in list
 * @param   L    pointer to the instruction list
 */
void instr_list_first(t_instr_list *L);

/**
 * \brief Function gives activity to the first instruction of program.
 * @param   L    pointer to the instruction list
 */
void instr_list_program_start(t_instr_list *L);

/**
 * \brief Function sets program start
 * @param   L                pointer to the instruction list
 * @param   program_start    pointer to item represents the first instruction of main program
 */
void instr_list_set_program_start(t_instr_list *L, t_instr_list_item *program_start);

/**
 * \brief Function gives activity to the next item in list
 * @param   L    pointer to the instruction list
 */
void instr_list_next(t_instr_list *L);

/**
 * \brief Function gives activity to the item according to second parameter
 * @param   L             pointer to the instruction list
 * @param   goto_instr    pointer to the item where go to
 */
void instr_list_goto(t_instr_list *L, void *goto_instr);

/**
 * \brief Function returns pointer to the last instruction
 * @param   L    pointer to the instruction list
 * @return       pointer to the last instruction item
 */
void *instr_list_get_last(t_instr_list *L);

/**
 * \brief Function returns instruction of active item
 * @param   L    pointer to the instruction list
 * @return       pointer to the active instruction item
 */
t_instr_list_item *instr_list_get_instruction_item(t_instr_list *L);

#endif
