/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   main.c
 * Datum:   24.10. 2014
 * Autori:  Matej Vido, xvidom00@stud.fit.vutbr.cz
 */

#include <stdio.h>

#include "common.h"
#include "scanner.h"
#include "string.h"
#include "ial.h"
#include "parser.h"
#include "instlist.h"
#include "interpreter.h"

FILE * source_file;
t_token token;
t_table * symbol_table;
t_instr_list instruction_list;

int main(int argc, char *argv[])
{
    int ret;

    ret = open_File(argc, argv);
    if (ret != 0)
        return E_INTERN;

    //init symbol table
    symbol_table = table_init(DEFAULT_TABLE_SIZE);
    if (symbol_table == NULL) {
        close_File();
        return E_INTERN;
    }

    //init token (init string in token)
    ret = stringInit(&token.attr.lit_string_or_id);
    if (ret != STR_OPERATION_OK) {
        close_File();
        table_destroy(symbol_table);
        return E_INTERN;
    }

    //init instruction list
    instr_list_init(&instruction_list);

    //call parser
    ret = parse();
    if (ret != E_OK) {
#ifdef DEBUG
        fprintf(stderr, "PARSE ERROR %d\n", ret);
#endif
        instr_list_dispose(&instruction_list);
        stringFree(&token.attr.lit_string_or_id);
        table_destroy(symbol_table);
        close_File();
        return ret;
    }
#ifdef DEBUG
    fprintf(stderr, "PARSE OK\n");
#endif
    
    //call interpreter
    ret = interpret();
    if (ret != E_OK) {
#ifdef DEBUG
        fprintf(stderr, "INTERPRET ERROR %d\n", ret);
#endif
    } else {
#ifdef DEBUG
        fprintf(stderr, "INTERPRET OK\n");
#endif
    }
    
    instr_list_dispose(&instruction_list);
    stringFree(&token.attr.lit_string_or_id);
    table_destroy(symbol_table);
    close_File();
    return ret;
}
