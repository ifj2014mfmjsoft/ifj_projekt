/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   expression.h
 * Datum:   12.11. 2014
 * Autori:  Jergus Jacko, xjacko03@stud.fit.vutbr.cz
 */

#ifndef _EXPRESSION_H_
#define _EXPRESSION_H_

#include "scanner.h"
#include "common.h"
#include "stack.h"
#include "ial.h"
#include "parser.h"
#include "instlist.h"

#define END_EXPRESSION TOKEN_NOT_EQUAL+1    //terminal $
#define EXPRESSION TOKEN_IDENTIFIER-1       //to recognize EXPRESSION on stack

extern bool fun_call; //for ex_funexp, to correct end of expression, in last parameter in function call

int process_expression(t_tab_item * fun_ptr, int *result_type);

#endif
