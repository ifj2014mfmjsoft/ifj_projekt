
/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   string.c
 * Datum:   12.10. 2014
 * Autori:  Jergus Jacko, xjacko03@stud.fit.vutbr.cz
 *          Matej Vido, xvidom00@stud.fit.vutbr.cz
 */

#include "string.h"


/**
 * \brief Function initializes new string
 * \param t_string struct, which will be initialized to default values
 * \return STR_ALLOC_FAIL if allocation failed or STR_OPERATION_OK if success
 */
int stringInit(t_string *str)
{
    if ((str->string = (char*) malloc(STR_LEN_INIT)) == NULL)
        return STR_ALLOC_FAIL;

    str->string[0] = '\0';
    str->length = 0;
    str->allocSize = STR_LEN_INIT;

    return STR_OPERATION_OK;
}

/**
 * \brief Function initializes new string with constant value.
 * \param   str         pointer to t_string structute, which will be initialized
 * \param   str_value   value to initialize
 * \return  STR_OPERATION_FAIL  if str_value is NULL
 *          STR_OPERATION_OK    if success
 *          STR_ALLOC_FAIL      if allocation failure
 */
int stringInitValue(t_string *str, const char *str_value)
{
    int size;
    if (str_value == NULL)
        return STR_OPERATION_FAIL;

    size = strlen(str_value);
    
    if ((str->string = (char *) malloc((size + 1) * sizeof(char))) == NULL)
        return STR_ALLOC_FAIL;

    strncpy(str->string, str_value, size);
    str->string[size] = '\0';
    str->length = size;
    str->allocSize = size + 1;

    return STR_OPERATION_OK;
}

/**
 * \brief Function sets string with constant value.
 * \param   str         pointer to t_string structute, which will be initialized
 * \param   str_value   value to initialize
 * \return  STR_OPERATION_FAIL  if str_value is NULL
 *          STR_OPERATION_OK    if success
 *          STR_ALLOC_FAIL      if allocation failure
 */
int stringSetValue(t_string *str, const char *str_value)
{
    int size;
    char * tmp;
    if (str_value == NULL)
        return STR_OPERATION_FAIL;

    size = strlen(str_value);

    if (str->allocSize < size + 1) {
        tmp = (char *) realloc(str->string, size + 1);
        if (tmp == NULL) {
            stringFree(str);
            return STR_ALLOC_FAIL;
        }
        str->string = tmp;
        str->allocSize = size + 1;
    }

    strncpy(str->string, str_value, size);
    str->string[size] = '\0';
    str->length = size;

    return STR_OPERATION_OK;
}

/**
 * \brief Function frees string from memory
 *        and set his values to zero
 */
void stringFree(t_string *str)
{
    free(str->string);
    str->string = NULL;
    str->length = 0;
    str->allocSize = 0;
}

/**
 * \brief Function deletes the content of string,
 *        size of allocated memory will be unchanged.
 */
void stringClear(t_string *str)
{
    str->string[0] = '\0';
    str->length = 0;
}

/**
 * \brief Function adds char value to the end of string value
 */
int stringAddChar(t_string *str1, char c)
{
    char * tmp;
    if (str1->length + 1 >= str1->allocSize)
    {
        //lack of memory, need realocation (realocation to double size of previous allocation)
        tmp = (char*) realloc(str1->string, str1->allocSize * 2);
        if (tmp == NULL) {
            stringFree(str1);
            return STR_ALLOC_FAIL;
        }
        str1->string = tmp;
        str1->allocSize *= 2;
    }
    str1->string[str1->length] = c;
    str1->length++;
    str1->string[str1->length] = '\0';

    return STR_OPERATION_OK;
}

/**
 * \brief Functiom compares two strings.
 * \param pointer to t_string
 * \param pointer to t_string
 * \return integer, result of comparation.
 *         str1 > str2 return integer < 0
 *         str1 > str2 return integer > 0
 *         str1 == str 2 return integer ==  0
 */
int stringCmpString(t_string *str1, t_string *str2)
{
    return strcmp(str1->string, str2->string);
}

/**
 * \brief Function compares string with constant string.
 * \param pointer to t_string
 * \param constant string value
 * \return integer, result of comparation.
 */
int stringCmpConstString(t_string *str1, char* str2)
{
    return strcmp(str1->string, str2);
}
/**
 * \brief Function makes concatenation of two string values.
 * \param pointer to destination t_string, where the result of concatenation will be stored
 * \param pointer to first t_string
 * \param pointer to second t_string
 * \return STR_ALLOC_FAIL if allocation failed or STR_OPERATION_OK if success
 */
int stringConcatenation(t_string *result, t_string *str1, t_string *str2)
{
    result->length = str1->length + str2->length;
    result->allocSize = result->length + 1;

    if ((result->string = (char*) malloc(sizeof(char) * result->allocSize)) == NULL)
        return STR_ALLOC_FAIL;

    memcpy(result->string, str1->string, str1->length);
    memcpy(&result->string[str1->length], str2->string, str2->length + 1);   //copy second string also with "\0" byte to new string

    return STR_OPERATION_OK;
}

/**
 * \brief Function for copying string from source to destination t_string
 *        source and destination t_string structs must be initialized!!
 * \param pointer to destination t_string
 * \param pointer to source t_string
 * \return STR_ALLOC_FAIL if allocation failed or STR_OPERATION_OK if success
 */
int stringCopy(t_string *destination, t_string *source)
{
    char * tmp;
    if (destination->allocSize < source->allocSize)
    {
        tmp = (char*) realloc(destination->string, source->allocSize);
        if (tmp == NULL) {
            stringFree(destination);
            return STR_ALLOC_FAIL;
        }
        destination->string = tmp;
        destination->allocSize = source->allocSize;
    }

    memcpy(destination->string, source->string, source->length + 1);    //copy source string also with "\0" byte to destination string
    destination->length = source->length;

    return STR_OPERATION_OK;
}

/**
 * \brief Function for duplicating string from source to uninitialized t_string struct
 * \param pointer to destination uninitialized t_string struct
 * \param pointer to source t_string, which will be duplicated
 * \return STR_ALLOC_FAIL if allocation failed or STR_OPERATION_OK if success
 */
int stringDuplicate(t_string *duplicate, t_string *source)
{
    if ((duplicate->string = (char*) malloc(sizeof(char) * source->allocSize)) == NULL)
        return STR_ALLOC_FAIL;

    memcpy(duplicate->string, source->string, source->length + 1);    //copy source string also with "\0" byte to destination string
    duplicate->length = source->length;
    duplicate->allocSize = source->allocSize;

    return STR_OPERATION_OK;
}
