/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   ial.h
 * Datum:   6.10. 2014
 * Autori:  Matej Vido, xvidom00@stud.fit.vutbr.cz
 *          Martin Cernek, xcerne01@stud.fit.vutbr.cz
 */

#ifndef __IAL_H__
#define __IAL_H__

#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "string.h"
#include "instlist.h"

enum heapsort_op {
    HEAPSORT_OP_OK,
    HEAPSORT_OP_FAIL,
};

enum tab_insert {
    TAB_INSERT_OK,
    TAB_INSERT_FOUND,
    TAB_INSERT_ALL_ERR,
};

enum tab_search {
    TAB_SEARCH_OK,
    TAB_SEARCH_FAIL,
};

enum table_data_type {
    DATA_TYPE_VAR,
    DATA_TYPE_FUN,
    DATA_TYPE_FUN_LEN, //builtin function length
    DATA_TYPE_FUN_CPY, //builtin function copy
    DATA_TYPE_FUN_FND, //builtin function find
    DATA_TYPE_FUN_SRT, //builtin function sort
    DATA_TYPE_CONST,
    DATA_TYPE_FUN_ITEM,
};

/**
 * \brief Here is placed value of different types.
 */
typedef union {
    int integer;
    bool boolean;
    double real;
    t_string string;
} t_tab_item_value;

/**
 * \brief Structure for symbol table.
 */
typedef struct table t_table;

/**
 * \brief Structure for data in table. Contains type of data and union with all data.
 */
typedef struct tab_data t_tab_data;

/**
 * \brief Structure for item of table.
 */
typedef struct tab_item t_tab_item;

/**
 * \brief Union for data in table wich is part of structure t_tab_data.
 */
typedef union {
    struct {
        int type;
        t_tab_item_value value;
        bool is_init;
    } variable;
    struct {
        int ret_type;
        int param_count;
        int var_count;  // -1 for builtin functions
        bool has_f_decl;
        bool has_def;
        t_instr_list_item * first_inst;
        t_table * loc_table;
        t_tab_item * first_param;
        t_tab_item * first_var;
        t_tab_item * ret_value;
    } function;
    struct {
        int type;
        t_tab_item_value value;
    } constant;
    struct {
        int type;
        int top_stack_index;
        t_tab_item * next;
    } fun_item;
} t_tab_data_value;

struct tab_data {
    int type;
    t_tab_data_value value;
};

struct tab_item {
    t_string key;
    t_tab_data data;
    t_tab_item * next;
};

struct table {
    uint64_t size;
    t_tab_item * items[];
};

#define ROTL64(num,amount) (((num) << ((amount) & 63)) | ((num) >> (64 - ((amount) & 63))))
#define ROTL32(num,amount) (((num) << ((amount) & 31)) | ((num) >> (32 - ((amount) & 31))))

/**
 * \brief Function initializes table.
 * 
 * @param   table_size      Size of the table, must be power of two.
 * 
 * @return  Pointer to structure with table.
 */
t_table * table_init(uint64_t table_size);

/**
 * \brief Function frees memory and destroys table.
 *
 * @param   table   Pointer to structure of table, which will be freed.
 */
void table_destroy(t_table * table);

/**
 * \brief Function lookups item and if not found, inserts it.
 *
 * Pointer to data is set whether new item is inserted or item already is in table.
 *
 * @param   table   Pointer to table structure.
 * @param   key     Key of item which will be inserted.
 * @param   data    Pointer where the pointer to data of inserted item will be stored.
 *
 * @return  TAB_INSERT_OK       If item was succesfully inserted.
 *          TAB_INSERT_ALL_ERR  If memory for new item could not be allocated.
 *          TAB_INSERT_FOUND    If item with such key already is in table.
 */
int table_insert(t_table * table, t_string * key, t_tab_item **item);

/**
 * \brief Function looks for item.
 *
 * @param   table   Pointer to table structure.
 * @param   key     Key of searched item.
 * @param   data    Pointer where pointer to data of found item will be stored. 
 *                  If item is not found, this pointer has undefined value.
 *
 * @return  TAB_SEARCH_OK   If item is found.
 *          TAB_SEARCH_FAIL If item is not found.
 */
int table_search(t_table * table, t_string * key, t_tab_item **item);

/**
 * \brief Functions provides sorting of strings.
 *
 * @param   source        Pointer to string.
 *          destination   Pointer to string, where sorted string will be stored.
 *
 * @return  HEAPSORT_OP_OK     If success.
 *          HEAPSORT_OP_FAIL   If duplication of new new string failed.
 */
int heap_sort(t_string *source, t_string *destination);

/**
 * \brief Required function for heapsort.
 * @param   string   Pointer to string.
 * @param   left     Index of the leftmost node.
 * @param   right    Index of the rightmost node.
 */
void sift_down(t_string *string, int left, int right);

/**
 * \brief Function finds substring in string and returns its position.
 *        If substring is not found, then function returns -1.
 *
 * @param   string      Pointer to string.
 * @param   substring   Pointer to substring.
 *
 * @return  Index of first occurrence substring in string.
 */
int find(t_string *string, t_string *substring);

/**
 * \brief Hash function for hash table.
 *
 * https://code.google.com/p/smhasher/wiki/MurmurHash3
 */
static inline uint64_t murmur_hash_3(const void *key, uint32_t key_size)
{
    uint32_t c1 = 5351;
    uint32_t c2 = 7193;
    uint32_t r1 = 19;
    uint32_t m1 = 11299;
    uint32_t n1 = 14033;
    uint64_t h  = 42;
    uint64_t * k_ptr = (uint64_t *) key;
    uint8_t * tail_ptr = (uint8_t *) (k_ptr + key_size/8);
    uint64_t k = 0;
    uint32_t rep = key_size / 8;
    uint32_t i;

    for (i = 0; i < rep; i++) {
        k = *(k_ptr + i);
        k *= c1;
        k = ROTL64(k, r1);
        k *= c2;

        h ^= k;
        h = ROTL64(h, r1);
        h = h * m1 + n1;
    }

    switch (key_size & 7) {
    case 7: k ^= (uint64_t) (tail_ptr[6]) << 48;
    case 6: k ^= (uint64_t) (tail_ptr[5]) << 40;
    case 5: k ^= (uint64_t) (tail_ptr[4]) << 32;
    case 4: k ^= (uint64_t) (tail_ptr[3]) << 24;
    case 3: k ^= (uint64_t) (tail_ptr[2]) << 16;
    case 2: k ^= (uint64_t) (tail_ptr[1]) <<  8;
    case 1: k ^= (uint64_t) (tail_ptr[0]) <<  0;
            k *= c1;
            k = ROTL64(k, r1);
            k *= c2;

            h ^= k;
            h = ROTL64(h, r1);
            h = h * m1 + n1;
    }

    h ^= h >> 33;
    h *= 0xff51afd7ed558ccd;
    h ^= h >> 33;
    h *= 0xc4ceb9fe1a85ec53;
    h ^= h >> 33;

    return h;
}

/**
 * \brief Function looks for item in specified row.
 *
 * @param   table   Pointer to table structure.
 * @param   key     Key of searched item.
 * @param   data    Pointer which will be set to point to data of found item.
 * @param   row     Row in which item is looked for.
 *
 * @return  TAB_SEARCH_OK   If item is found.
 *          TAB_SEARCH_FAIL If item is not found.
 */
static inline int table_search_in_row(t_table * table, t_string * key, t_tab_item **item, uint64_t row)
{
    t_tab_item * tmp;
    
    for (tmp = table->items[row]; tmp != NULL; tmp = tmp->next) {
        if (stringCmpString(key, &tmp->key) == 0) {
            if (item != NULL)
                *item = tmp;
            return TAB_SEARCH_OK;
        }
    };

    return TAB_SEARCH_FAIL;
}

#endif
