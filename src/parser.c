/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   parser.c
 * Datum:   24.10. 2014
 * Autori:  Matej Vido, xvidom00@stud.fit.vutbr.cz
 */

#include "common.h"
#include "parser.h"
#include "scanner.h"
#include "expression.h"
#include "ial.h"

uint32_t constant_counter = 1;

/**
 * \brief Function generates key for constant.
 *
 * @param string Pointer to initialized t_string structure.
 */
void generate_key(t_string * string)
{
    char key[11] = {0,};
    
    snprintf(key, 11, "%d", constant_counter);
    
    constant_counter++;

    stringSetValue(string, key);
}

/**
 * \brief Function generates instruction and inserts it in instruction list.
 *
 * @param   type    Type of instruction.
 * @param   ptr1    First pointer, see instruction details.
 * @param   ptr2    Second pointer.
 * @param   ptr3    Third pointer.
 *
 * @return  LIST_OPERATION_OK     if success
 *          LIST_OPERATION_FAIL   if failure
 */
int generate_instruction(int type, void * ptr1, void * ptr2, void * ptr3)
{
    t_instruction new_inst;
    new_inst.instr_type = type;
    new_inst.address1 = ptr1;
    new_inst.address2 = ptr2;
    new_inst.address3 = ptr3;
    return instr_list_insert_last(&instruction_list, new_inst);
}

int parse(void)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    t_string tmp;
    t_tab_item *item_ptr;

    //insert builtin function length in symbol table
    stringInitValue(&tmp, "length");

    ret = table_insert(symbol_table, &tmp, &item_ptr);

    switch (ret) {
    case TAB_INSERT_FOUND:
    case TAB_INSERT_ALL_ERR:
        stringFree(&tmp);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
        return E_INTERN;
    case TAB_INSERT_OK:
        item_ptr->data.type = DATA_TYPE_FUN_LEN;
        item_ptr->data.value.function.ret_type = TYPE_INT;
        item_ptr->data.value.function.param_count = 1;
        item_ptr->data.value.function.var_count = -1; // unknown value
        item_ptr->data.value.function.has_f_decl = true;
        item_ptr->data.value.function.has_def = true;
        item_ptr->data.value.function.first_inst = NULL;
        item_ptr->data.value.function.first_param = NULL;
        item_ptr->data.value.function.first_var = NULL;
        item_ptr->data.value.function.ret_value = NULL;
        item_ptr->data.value.function.loc_table = NULL;
    }

    //insert builtin function copy in symbol table
    stringSetValue(&tmp, "copy");

    ret = table_insert(symbol_table, &tmp, &item_ptr);

    switch (ret) {
    case TAB_INSERT_FOUND:
    case TAB_INSERT_ALL_ERR:
        stringFree(&tmp);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
        return E_INTERN;
    case TAB_INSERT_OK:
        item_ptr->data.type = DATA_TYPE_FUN_CPY;
        item_ptr->data.value.function.ret_type = TYPE_STR;
        item_ptr->data.value.function.param_count = 3;
        item_ptr->data.value.function.var_count = -1; // unknown value
        item_ptr->data.value.function.has_f_decl = true;
        item_ptr->data.value.function.has_def = true;
        item_ptr->data.value.function.first_inst = NULL;
        item_ptr->data.value.function.first_param = NULL;
        item_ptr->data.value.function.first_var = NULL;
        item_ptr->data.value.function.ret_value = NULL;
        item_ptr->data.value.function.loc_table = NULL;
    }

    //insert builtin function find in symbol table
    stringSetValue(&tmp, "find");

    ret = table_insert(symbol_table, &tmp, &item_ptr);

    switch (ret) {
    case TAB_INSERT_FOUND:
    case TAB_INSERT_ALL_ERR:
        stringFree(&tmp);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
        return E_INTERN;
    case TAB_INSERT_OK:
        item_ptr->data.type = DATA_TYPE_FUN_FND;
        item_ptr->data.value.function.ret_type = TYPE_INT;
        item_ptr->data.value.function.param_count = 2;
        item_ptr->data.value.function.var_count = -1; // unknown value
        item_ptr->data.value.function.has_f_decl = true;
        item_ptr->data.value.function.has_def = true;
        item_ptr->data.value.function.first_inst = NULL;
        item_ptr->data.value.function.first_param = NULL;
        item_ptr->data.value.function.first_var = NULL;
        item_ptr->data.value.function.ret_value = NULL;
        item_ptr->data.value.function.loc_table = NULL;
    }

    //insert builtin function sort in symbol table
    stringSetValue(&tmp, "sort");

    ret = table_insert(symbol_table, &tmp, &item_ptr);

    switch (ret) {
    case TAB_INSERT_FOUND:
    case TAB_INSERT_ALL_ERR:
        stringFree(&tmp);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
        return E_INTERN;
    case TAB_INSERT_OK:
        item_ptr->data.type = DATA_TYPE_FUN_SRT;
        item_ptr->data.value.function.ret_type = TYPE_STR;
        item_ptr->data.value.function.param_count = 1;
        item_ptr->data.value.function.var_count = -1; // unknown value
        item_ptr->data.value.function.has_f_decl = true;
        item_ptr->data.value.function.has_def = true;
        item_ptr->data.value.function.first_inst = NULL;
        item_ptr->data.value.function.first_param = NULL;
        item_ptr->data.value.function.first_var = NULL;
        item_ptr->data.value.function.ret_value = NULL;
        item_ptr->data.value.function.loc_table = NULL;
    }

    //start syntax analysis
    ret = getNextToken();
    
    if (ret == E_OK) {
        ret = program();
    }

    stringFree(&tmp);
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
    return ret;
}

int program(void)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_KW_BEGIN || token.type == TOKEN_KW_FUNCTION || token.type == TOKEN_KW_VAR) {
        //rule 1
        ret = glob_var_def();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = fun_def_list();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = main_body();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        //check if program ends correctly
        if (token.type != TOKEN_END_OF_FILE) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        //generate end of program
        ret = generate_instruction(I_STOP, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    }
        
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int glob_var_def(void)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_KW_BEGIN || token.type == TOKEN_KW_FUNCTION) {
        //rule 2
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_KW_VAR) {
        //rule 3
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = glob_var_def_list();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int glob_var_def_list(void)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    t_tab_item *item_ptr;
    int type;

    if (token.type == TOKEN_IDENTIFIER) {
        //rule 4
        //semantic action, insert identifier in table...
        ret = table_insert(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);

        //semantic fault, variable or function with same name already is in table
        if (ret == TAB_INSERT_FOUND) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
            return E_SEM_DEF;
        } else if (ret == TAB_INSERT_ALL_ERR) {
        //allocation error
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        //set type of record in symbol table
        item_ptr->data.type = DATA_TYPE_VAR;

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_COLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = type_term(&type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        //set variable type
        item_ptr->data.value.variable.type = type;

        if (token.type != TOKEN_SEMICOLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = glob_var_def_list_part();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int glob_var_def_list_part(void)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    t_tab_item * item_ptr;
    int type;

    if (token.type == TOKEN_KW_BEGIN || token.type == TOKEN_KW_FUNCTION) {
        //rule 6
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_IDENTIFIER) {
        //rule 5
        //semantic action, insert identifier in table...
        ret = table_insert(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);

        //semantic fault, variable or function with same name already is in table
        if (ret == TAB_INSERT_FOUND) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
            return E_SEM_DEF;
        } else if (ret == TAB_INSERT_ALL_ERR) {
        //allocation error
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        //set type of record in symbol table
        item_ptr->data.type = DATA_TYPE_VAR;

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_COLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = type_term(&type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        //set variable type
        item_ptr->data.value.variable.type = type;

        if (token.type != TOKEN_SEMICOLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = glob_var_def_list_part();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int fun_def_list(void)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    t_tab_item * item_ptr;
    int type;

    if (token.type == TOKEN_KW_BEGIN) {
        //rule 11
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_KW_FUNCTION) {
        //rule 12
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (token.type != TOKEN_IDENTIFIER) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        //semantic action, insert identifier in table...
        ret = table_insert(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);

        //function with same name already is in table
        if (ret == TAB_INSERT_FOUND) {
            if (item_ptr->data.type != DATA_TYPE_FUN) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }

            if (item_ptr->data.value.function.has_f_decl == true &&
                item_ptr->data.value.function.has_def == true) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }
        } else if (ret == TAB_INSERT_ALL_ERR) {
        //allocation error
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        } else {
            //set type of record in symbol table
            item_ptr->data.type = DATA_TYPE_FUN;
    
            //create local symbol table
            if ((item_ptr->data.value.function.loc_table = table_init(DEFAULT_LOCAL_TABLE_SIZE)) == NULL) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                return E_INTERN;
            }

            //insert local variable for return value in local table
            t_tab_item * tmp;
            
            ret = table_insert(item_ptr->data.value.function.loc_table, &(item_ptr->key), &tmp);
            switch (ret) {
            case TAB_INSERT_FOUND:
            case TAB_INSERT_ALL_ERR:
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                return E_INTERN;
            case TAB_INSERT_OK:
                tmp->data.type = DATA_TYPE_FUN_ITEM;
                tmp->data.value.fun_item.next = NULL;
                break;
            }

            item_ptr->data.value.function.ret_value = tmp;
            
            //set bools for function
            item_ptr->data.value.function.has_f_decl = false;
            item_ptr->data.value.function.has_def = false;
    
            //set param and var count to zero
            item_ptr->data.value.function.param_count = 0;
            item_ptr->data.value.function.var_count = 0;
    
            //set pointers to NULL
            item_ptr->data.value.function.first_inst = NULL;
            item_ptr->data.value.function.first_param = NULL;
            item_ptr->data.value.function.first_var = NULL;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_LEFT_PARENTHESIS) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        ret = param_list(item_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (token.type != TOKEN_RIGHT_PARENTHESIS) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_COLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        ret = type_term(&type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (item_ptr->data.value.function.has_f_decl == false &&
            item_ptr->data.value.function.has_def == false) {
            //new record
            item_ptr->data.value.function.ret_type = type;
            (item_ptr->data.value.function.ret_value)->data.value.fun_item.type = type;
            (item_ptr->data.value.function.ret_value)->data.value.fun_item.top_stack_index = 0;
        } else {
            //has definition or declaration
            if (item_ptr->data.value.function.ret_type != type) {
                //redefinition, semantic fault
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }
        }

        if (token.type != TOKEN_SEMICOLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        ret = fun_decl(item_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (token.type != TOKEN_SEMICOLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        ret = fun_def_list();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int main_body(void)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    int i;
    t_tab_item * tmp;

    if (token.type == TOKEN_KW_BEGIN) {
        //rule 24

        //check whether each declared function hash definition
        for (i = 0; i < DEFAULT_TABLE_SIZE; i++) {
            for (tmp = symbol_table->items[i]; tmp != NULL; tmp = tmp->next) {
                if (tmp->data.type == DATA_TYPE_FUN) {
                    if (tmp->data.value.function.has_def != true) {
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                        return E_SEM_DEF;
                    }
                }
            }
        }

        //generate instruction start of program
        ret = generate_instruction(I_START, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        //set program start to I_START
        instr_list_set_program_start(&instruction_list, (t_instr_list_item *) instr_list_get_last(&instruction_list));
        
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = statement_list((t_tab_item *) NULL);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (token.type != TOKEN_KW_END) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_DOT) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        } 
        
        ret = getNextToken();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int param_list(t_tab_item * fun_ptr)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    int type;
    t_tab_item * item_ptr;

    if (token.type == TOKEN_RIGHT_PARENTHESIS) {
        //rule 20
        if (fun_ptr->data.value.function.has_f_decl == true ||
            fun_ptr->data.value.function.has_def == true) {
            //has definition or declaration
            if (fun_ptr->data.value.function.param_count != 0) {
                //redefinition, semantic fault
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }
        }

#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_IDENTIFIER) {
        //rule 21
        //semantic action, insert param in table...
        
        if (fun_ptr->data.value.function.has_f_decl == false &&
            fun_ptr->data.value.function.has_def == false) {
            //check whether function with same name already exists

            ret = table_search(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);
            
            if (ret == TAB_SEARCH_OK && (
                item_ptr->data.type == DATA_TYPE_FUN ||
                item_ptr->data.type == DATA_TYPE_FUN_LEN ||
                item_ptr->data.type == DATA_TYPE_FUN_CPY ||
                item_ptr->data.type == DATA_TYPE_FUN_FND ||
                item_ptr->data.type == DATA_TYPE_FUN_SRT)) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }

            //new record
            ret = table_insert(fun_ptr->data.value.function.loc_table, &(token.attr.lit_string_or_id), &item_ptr);

            if (ret == TAB_INSERT_FOUND) {
                //semantic fault, parameter with same name already is in table
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            } else if (ret == TAB_INSERT_ALL_ERR) {
                //allocation error
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                return E_INTERN;
            }
            
            item_ptr->data.type = DATA_TYPE_FUN_ITEM;

            item_ptr->data.value.fun_item.top_stack_index = fun_ptr->data.value.function.param_count;
            item_ptr->data.value.fun_item.next = NULL;
            
            fun_ptr->data.value.function.first_param = item_ptr;
            fun_ptr->data.value.function.param_count += 1;
        }
        
        t_string tmp_string;
        ret = stringDuplicate(&tmp_string, &(token.attr.lit_string_or_id));
        if (ret != STR_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            stringFree(&tmp_string);
            return ret;
        }
        if (token.type != TOKEN_COLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            stringFree(&tmp_string);
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            stringFree(&tmp_string);
            return ret;
        }
        ret = type_term(&type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            stringFree(&tmp_string);
            return ret;
        }

        if (fun_ptr->data.value.function.has_f_decl == false &&
            fun_ptr->data.value.function.has_def == false) {
            item_ptr->data.value.fun_item.type = type;
        } else {
            if (fun_ptr->data.value.function.param_count == 0) {
                //redefinition, semantic fault
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                stringFree(&tmp_string);
                return E_SEM_DEF;
            } else {
                ret = table_search(fun_ptr->data.value.function.loc_table, &tmp_string, &item_ptr);

                if (ret != TAB_SEARCH_OK ||
                    item_ptr->data.type != DATA_TYPE_FUN_ITEM ||
                    item_ptr->data.value.fun_item.type != type ||
                    item_ptr->data.value.fun_item.top_stack_index != 0) {
                    //redefinition, semantic fault
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                    stringFree(&tmp_string);
                    return E_SEM_DEF;
                }
            }
        }
        
        stringFree(&tmp_string);
        
        /* this is always 0. parameter, therefore next parameter is always 1 */
        ret = param(fun_ptr, 1, &(item_ptr->data.value.fun_item.next)); 
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int type_term(int * type)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif
    
    int ret;

    if (token.type == TOKEN_KW_BOOLEAN || 
        token.type == TOKEN_KW_STRING ||
        token.type == TOKEN_KW_REAL ||
        token.type == TOKEN_KW_INTEGER) {
        //rule 10, 9, 8, 7
        
        switch (token.type) {
        case TOKEN_KW_BOOLEAN:
            *type = TYPE_BOOL;
            break;
        case TOKEN_KW_STRING:
            *type = TYPE_STR;
            break;
        case TOKEN_KW_REAL:
            *type = TYPE_REAL;
            break;
        case TOKEN_KW_INTEGER:
            *type = TYPE_INT;
            break;
        default:
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int fun_decl(t_tab_item * fun_ptr)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_KW_FORWARD) {
        //rule 13
        
        //semantic fault if forward declaration already exists
        if (fun_ptr->data.value.function.has_f_decl == true) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
            return E_SEM_DEF;
        }
        
        fun_ptr->data.value.function.has_f_decl = true;

        ret = getNextToken();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    } else if (token.type == TOKEN_KW_BEGIN || token.type == TOKEN_KW_VAR) {
        //rule 14
        
        //semantic fault if definition already exists
        if (fun_ptr->data.value.function.has_def == true) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
            return E_SEM_DEF;
        }
        
        fun_ptr->data.value.function.has_def = true;
        fun_ptr->data.value.function.has_f_decl = true;

        ret = loc_var_def(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = generate_instruction(I_LABEL, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        //set first instruction of function
        fun_ptr->data.value.function.first_inst = (t_instr_list_item *) instr_list_get_last(&instruction_list);

        if (token.type != TOKEN_KW_BEGIN) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        //set top_stack_indexes for params and vars
        t_tab_item * tmp;
        int param_count = fun_ptr->data.value.function.param_count;
        int var_count = fun_ptr->data.value.function.var_count;

        for (tmp = fun_ptr->data.value.function.first_param; tmp != NULL; tmp = tmp->data.value.fun_item.next) {
            tmp->data.value.fun_item.top_stack_index = param_count + var_count - tmp->data.value.fun_item.top_stack_index;
        }
        
        for (tmp = fun_ptr->data.value.function.first_var; tmp != NULL; tmp = tmp->data.value.fun_item.next) {
            tmp->data.value.fun_item.top_stack_index = param_count + var_count - tmp->data.value.fun_item.top_stack_index;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        ret = statement_list(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (token.type != TOKEN_KW_END) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        
        ret = generate_instruction(I_RETURN, fun_ptr, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        ret = getNextToken();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int param(t_tab_item * fun_ptr, int next_param_num, t_tab_item ** previous_next_param)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    int type;
    t_tab_item * item_ptr;

    if (token.type == TOKEN_RIGHT_PARENTHESIS) {
        //rule 22
        if (fun_ptr->data.value.function.has_f_decl == true ||
            fun_ptr->data.value.function.has_def == true) {
            //has definition or declaration
            if (fun_ptr->data.value.function.param_count != next_param_num) {
                //redefinition, semantic fault
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }
        }

#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_SEMICOLON) {
        //rule 23
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_IDENTIFIER) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        //semantic action with identifier

        if (fun_ptr->data.value.function.has_f_decl == false &&
            fun_ptr->data.value.function.has_def == false) {
            //check whether function with same name already exists

            ret = table_search(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);
            
            if (ret == TAB_SEARCH_OK && (
                item_ptr->data.type == DATA_TYPE_FUN ||
                item_ptr->data.type == DATA_TYPE_FUN_LEN ||
                item_ptr->data.type == DATA_TYPE_FUN_CPY ||
                item_ptr->data.type == DATA_TYPE_FUN_FND ||
                item_ptr->data.type == DATA_TYPE_FUN_SRT)) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }

            //new record
            ret = table_insert(fun_ptr->data.value.function.loc_table, &(token.attr.lit_string_or_id), &item_ptr);

            if (ret == TAB_INSERT_FOUND) {
                //semantic fault, parameter with same name already is in table
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            } else if (ret == TAB_INSERT_ALL_ERR) {
                //allocation error
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                return E_INTERN;
            }
            
            item_ptr->data.type = DATA_TYPE_FUN_ITEM;

            item_ptr->data.value.fun_item.top_stack_index = fun_ptr->data.value.function.param_count;
            item_ptr->data.value.fun_item.next = NULL;

            *previous_next_param = item_ptr;
            
            fun_ptr->data.value.function.param_count += 1;
        }
        
        t_string tmp_string;
        ret = stringDuplicate(&tmp_string, &(token.attr.lit_string_or_id));
        if (ret != STR_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            stringFree(&tmp_string);
            return ret;
        }
        if (token.type != TOKEN_COLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            stringFree(&tmp_string);
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            stringFree(&tmp_string);
            return ret;
        }
        ret = type_term(&type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            stringFree(&tmp_string);
            return ret;
        }
        
        if (fun_ptr->data.value.function.has_f_decl == false &&
            fun_ptr->data.value.function.has_def == false) {
            item_ptr->data.value.fun_item.type = type;
        } else {
            if (fun_ptr->data.value.function.param_count == 0) {
                //redefinition, semantic fault
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                stringFree(&tmp_string);
                return E_SEM_DEF;
            } else {
                ret = table_search(fun_ptr->data.value.function.loc_table, &tmp_string, &item_ptr);

                if (ret != TAB_SEARCH_OK ||
                    item_ptr->data.type != DATA_TYPE_FUN_ITEM ||
                    item_ptr->data.value.fun_item.type != type ||
                    item_ptr->data.value.fun_item.top_stack_index != next_param_num) {
                    //redefinition, semantic fault
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                    stringFree(&tmp_string);
                    return E_SEM_DEF;
                }
            }
        }
        
        stringFree(&tmp_string);
        
        /* this is always 0. parameter, therefore next parameter is always 1 */
        ret = param(fun_ptr, next_param_num + 1, &(item_ptr->data.value.fun_item.next)); 
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int loc_var_def(t_tab_item * fun_ptr)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_KW_BEGIN) {
        //rule 15
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_KW_VAR) {
        //rule 16
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = loc_var_def_list(fun_ptr);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int loc_var_def_list(t_tab_item * fun_ptr)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    int type;
    t_tab_item * item_ptr;

    if (token.type == TOKEN_IDENTIFIER) {
        //rule 17
        //check whether function with same name already exists

        ret = table_search(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);
        
        if (ret == TAB_SEARCH_OK && (
            item_ptr->data.type == DATA_TYPE_FUN ||
            item_ptr->data.type == DATA_TYPE_FUN_LEN ||
            item_ptr->data.type == DATA_TYPE_FUN_CPY ||
            item_ptr->data.type == DATA_TYPE_FUN_FND ||
            item_ptr->data.type == DATA_TYPE_FUN_SRT)) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
            return E_SEM_DEF;
        }

        //semantic action, insert identifier in table...
        ret = table_insert(fun_ptr->data.value.function.loc_table, &(token.attr.lit_string_or_id), &item_ptr);

        //semantic fault, variable or function with same name already is in table
        if (ret == TAB_INSERT_FOUND) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
            return E_SEM_DEF;
        } else if (ret == TAB_INSERT_ALL_ERR) {
        //allocation error
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        //set type of record in symbol table
        item_ptr->data.type = DATA_TYPE_FUN_ITEM;

        //this is 0. local variable, it has same index as number of parameters
        item_ptr->data.value.fun_item.top_stack_index = fun_ptr->data.value.function.param_count;
        item_ptr->data.value.fun_item.next = NULL;

        fun_ptr->data.value.function.first_var = item_ptr;
        fun_ptr->data.value.function.var_count += 1;

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_COLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = type_term(&type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        //set variable type
        item_ptr->data.value.fun_item.type = type;

        if (token.type != TOKEN_SEMICOLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = loc_var_def_list_part(fun_ptr, &(item_ptr->data.value.fun_item.next));
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int loc_var_def_list_part(t_tab_item * fun_ptr, t_tab_item ** previous_next_var)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    int type;
    t_tab_item * item_ptr;

    if (token.type == TOKEN_KW_BEGIN) {
        //rule 19
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_IDENTIFIER) {
        //rule 18
        //check whether function with same name already exists

        ret = table_search(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);
        
        if (ret == TAB_SEARCH_OK && (
            item_ptr->data.type == DATA_TYPE_FUN ||
            item_ptr->data.type == DATA_TYPE_FUN_LEN ||
            item_ptr->data.type == DATA_TYPE_FUN_CPY ||
            item_ptr->data.type == DATA_TYPE_FUN_FND ||
            item_ptr->data.type == DATA_TYPE_FUN_SRT)) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
            return E_SEM_DEF;
        }

        //semantic action, insert identifier in table...
        ret = table_insert(fun_ptr->data.value.function.loc_table, &(token.attr.lit_string_or_id), &item_ptr);

        //semantic fault, variable or function with same name already is in table
        if (ret == TAB_INSERT_FOUND) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
            return E_SEM_DEF;
        } else if (ret == TAB_INSERT_ALL_ERR) {
        //allocation error
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        //set type of record in symbol table
        item_ptr->data.type = DATA_TYPE_FUN_ITEM;

        //this is x. local variable
        item_ptr->data.value.fun_item.top_stack_index = fun_ptr->data.value.function.param_count + fun_ptr->data.value.function.var_count;
        item_ptr->data.value.fun_item.next = NULL;

        *previous_next_var = item_ptr;

        fun_ptr->data.value.function.var_count += 1;

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_COLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = type_term(&type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        //set variable type
        item_ptr->data.value.fun_item.type = type;

        if (token.type != TOKEN_SEMICOLON) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = loc_var_def_list_part(fun_ptr, &(item_ptr->data.value.fun_item.next));
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int statement_list(t_tab_item * fun_ptr)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_KW_BEGIN || 
        token.type == TOKEN_KW_IF || 
        token.type == TOKEN_KW_READLN || 
        token.type == TOKEN_KW_WHILE || 
        token.type == TOKEN_KW_WRITE || 
        token.type == TOKEN_IDENTIFIER) {
        //rule 25
        ret = statement(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = statement_list_part(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_KW_END) {
        //rule 26
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int statement(t_tab_item * fun_ptr)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    int type;
    int id_type;
    t_instr_list_item * il_item_ptr_icj;
    t_instr_list_item * il_item_ptr_ij;
    t_tab_item * item_ptr;

    if (token.type == TOKEN_KW_BEGIN) {
        //rule 30
        ret = complex_statement(fun_ptr);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    } else if (token.type == TOKEN_KW_IF) {
        //rule 32
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = process_expression(fun_ptr, &type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (type != TYPE_BOOL) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
            return E_SEM_TYPE;
        }

        ret = generate_instruction(I_COND_JUMP, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        
        //store address of instruction
        il_item_ptr_icj = (t_instr_list_item *) instr_list_get_last(&instruction_list);

        if (token.type != TOKEN_KW_THEN) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        ret = complex_statement(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = generate_instruction(I_JUMP, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
    
        //store address of instruction
        il_item_ptr_ij = (t_instr_list_item *) instr_list_get_last(&instruction_list);

        ret = generate_instruction(I_LABEL, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        
        //set pointer for I_COND_JUMP
        il_item_ptr_icj->instruction.address1 = instr_list_get_last(&instruction_list);

        ret = else_nonterm(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = generate_instruction(I_LABEL, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        
        //set pointer for I_JUMP
        il_item_ptr_ij->instruction.address1 = instr_list_get_last(&instruction_list);

#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_KW_READLN) {
        //rule 44
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_LEFT_PARENTHESIS) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_IDENTIFIER) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        //semantic action with identifier
        bool found = false;

        if (fun_ptr != NULL) {
            //first look for in local table
            ret = table_search(fun_ptr->data.value.function.loc_table, &(token.attr.lit_string_or_id), &item_ptr);

            if (ret == TAB_SEARCH_OK) {
                found = true;
                if (item_ptr->data.type == DATA_TYPE_FUN_ITEM) {
                    switch (item_ptr->data.value.fun_item.type) {
                    case TYPE_BOOL:
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                        return E_SEM_TYPE;
                    case TYPE_INT:
                    case TYPE_REAL:
                    case TYPE_STR:
                        ret = generate_instruction(I_READLN, item_ptr, NULL, NULL);
                        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                            return E_INTERN;
                        }
                    }
                } else {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                    return E_SEM_DEF;
                }
            }
        }

        if (!found) {
            //look for in global table
            ret = table_search(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);

            if (ret == TAB_SEARCH_FAIL) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }

            if (item_ptr->data.type != DATA_TYPE_VAR) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }

            switch (item_ptr->data.value.variable.type) {
            case TYPE_BOOL:
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                return E_SEM_TYPE;
            case TYPE_INT:
            case TYPE_REAL:
            case TYPE_STR:
                ret = generate_instruction(I_READLN, item_ptr, NULL, NULL);
                if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                    return E_INTERN;
                }
            }
        }
        
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_RIGHT_PARENTHESIS) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        
        ret = getNextToken();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    } else if (token.type == TOKEN_KW_WHILE) {
        //rule 33
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = generate_instruction(I_LABEL, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        
        //store pointer for I_JUMP
        il_item_ptr_ij = (t_instr_list_item *) instr_list_get_last(&instruction_list);
        
        ret = process_expression(fun_ptr, &type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (type != TYPE_BOOL) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
            return E_SEM_TYPE;
        }

        ret = generate_instruction(I_COND_JUMP, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        
        //store address of instruction
        il_item_ptr_icj = (t_instr_list_item *) instr_list_get_last(&instruction_list);

        if (token.type != TOKEN_KW_DO) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = complex_statement(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = generate_instruction(I_JUMP, (void *) il_item_ptr_ij, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        
        ret = generate_instruction(I_LABEL, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        
        //set pointer for I_COND_JUMP
        il_item_ptr_icj->instruction.address1 = instr_list_get_last(&instruction_list);

#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_KW_REPEAT) {
        //rule 53
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = generate_instruction(I_LABEL, NULL, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        
        //store pointer for I_COND_JUMP
        il_item_ptr_icj = (t_instr_list_item *) instr_list_get_last(&instruction_list);
        
        ret = statement(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = statement_list_part(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (token.type != TOKEN_KW_UNTIL) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = process_expression(fun_ptr, &type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        if (type != TYPE_BOOL) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
            return E_SEM_TYPE;
        }

        ret = generate_instruction(I_COND_JUMP, (void *) il_item_ptr_icj, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_KW_WRITE) {
        //rule 45
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_LEFT_PARENTHESIS) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        t_list * list = list_init();
        if (list == NULL) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        ret = term_list(fun_ptr, list);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            list_free(list);
            return ret;
        }

        if (token.type != TOKEN_RIGHT_PARENTHESIS) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            list_free(list);
            return E_SYN;
        }

        ret = generate_instruction(I_WRITE, (void *) list, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            list_free(list);
            return E_INTERN;
        }
        
        ret = getNextToken();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    } else if (token.type == TOKEN_IDENTIFIER) {
        //rule 29
        //semantic action with identifier
        bool found = false;

        if (fun_ptr != NULL) {
            //first look for in local table
            ret = table_search(fun_ptr->data.value.function.loc_table, &(token.attr.lit_string_or_id), &item_ptr);

            if (ret == TAB_SEARCH_OK) {
                found = true;
                if (item_ptr->data.type == DATA_TYPE_FUN_ITEM) {
                    id_type = item_ptr->data.value.fun_item.type;
                } else {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                    return E_SEM_DEF;
                }
            }
        }

        if (!found) {
            //look for in global table
            ret = table_search(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);

            if (ret == TAB_SEARCH_FAIL) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }

            if (item_ptr->data.type != DATA_TYPE_VAR) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }

            id_type = item_ptr->data.value.variable.type;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        if (token.type != TOKEN_ASSIGNMENT) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = process_expression(fun_ptr, &type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (id_type != type) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
            return E_SEM_TYPE;
        }

        //generate
        ret = generate_instruction(I_ASSIGN, item_ptr, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int else_nonterm(t_tab_item * fun_ptr)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_KW_END || token.type == TOKEN_SEMICOLON) {
        //rule 51
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_KW_ELSE) {
        //rule 52
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = complex_statement(fun_ptr);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int statement_list_part(t_tab_item * fun_ptr)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_KW_END || token.type == TOKEN_KW_UNTIL) {
        //rule 27
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else if (token.type == TOKEN_SEMICOLON) {
        //rule 28
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        ret = statement(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = statement_list_part(fun_ptr);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int complex_statement(t_tab_item * fun_ptr)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_KW_BEGIN) {
        //rule 31
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        ret = statement_list(fun_ptr);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        if (token.type != TOKEN_KW_END) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            return E_SYN;
        }
        
        ret = getNextToken();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int term_list(t_tab_item * fun_ptr, t_list * list)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_IDENTIFIER ||
        token.type == TOKEN_LIT_REAL ||
        token.type == TOKEN_LIT_INTEGER ||
        token.type == TOKEN_LIT_STRING ||
        token.type == TOKEN_LIT_BOOLEAN) {
        //rule 46
        ret = term(fun_ptr, list);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = term_list_part(fun_ptr, list);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int term(t_tab_item * fun_ptr, t_list * list)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    t_tab_item * item_ptr;

    if (token.type == TOKEN_IDENTIFIER) {
        //rule 39
        bool found = false;

        if (fun_ptr != NULL) {
            //first look for in local table
            ret = table_search(fun_ptr->data.value.function.loc_table, &(token.attr.lit_string_or_id), &item_ptr);
            if (ret == TAB_SEARCH_OK) {
                found = true;
                if (item_ptr->data.type == DATA_TYPE_FUN_ITEM) {
                    ret = list_insert_last(list, item_ptr);
                    if (ret != LIST_OP_OK) {
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                        return E_INTERN;
                    }
                } else {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                    return E_SEM_DEF;
                }
            }
        }

        if (!found) {
            //look for in global table
            ret = table_search(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);

            if (ret == TAB_SEARCH_FAIL) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }

            if (item_ptr->data.type != DATA_TYPE_VAR) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_DEF, token.type);
#endif
                return E_SEM_DEF;
            }
            
            ret = list_insert_last(list, item_ptr);
            if (ret != LIST_OP_OK) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                return E_INTERN;
            }
        }

        ret = getNextToken();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    } else if (token.type == TOKEN_LIT_REAL || 
               token.type == TOKEN_LIT_INTEGER ||
               token.type == TOKEN_LIT_STRING ||
               token.type == TOKEN_LIT_BOOLEAN) {
        //rule 41
        t_string tmp;

        ret = stringInit(&tmp);
        if (ret != STR_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        generate_key(&tmp);

        ret = table_insert(symbol_table, &tmp, &item_ptr);
        stringFree(&tmp);
        if (ret != TAB_INSERT_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        item_ptr->data.type = DATA_TYPE_CONST;

        switch (token.type) {
        case TOKEN_LIT_REAL:
            item_ptr->data.value.constant.type = TYPE_REAL;
            item_ptr->data.value.constant.value.real = token.attr.lit_double;
            break;
        case TOKEN_LIT_INTEGER:
            item_ptr->data.value.constant.type = TYPE_INT;
            item_ptr->data.value.constant.value.integer = token.attr.lit_int;
            break;
        case TOKEN_LIT_STRING:
            item_ptr->data.value.constant.type = TYPE_STR;
            ret = stringDuplicate(&(item_ptr->data.value.constant.value.string), &(token.attr.lit_string_or_id));
            if (ret != STR_OPERATION_OK) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                return E_INTERN;
            }
            break;
        case TOKEN_LIT_BOOLEAN:
            item_ptr->data.value.constant.type = TYPE_BOOL;
            item_ptr->data.value.constant.value.boolean = token.attr.lit_int ? 1 : 0;
            break;
        }

        ret = list_insert_last(list, item_ptr);
        if (ret != LIST_OP_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        ret = getNextToken();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int term_list_part(t_tab_item * fun_ptr, t_list * list)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_COMMA) {
        //rule 47
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        ret = term(fun_ptr, list);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = term_list_part(fun_ptr, list);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    } else if (token.type == TOKEN_RIGHT_PARENTHESIS) {
        //rule 48
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int function_call(t_tab_item * fun_ptr, t_tab_item * fun_to_call)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;

    if (token.type == TOKEN_LEFT_PARENTHESIS) {
        //rule 34
        //when here it is function for sure

        t_list * list = list_init();
        if (list == NULL) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            list_free(list);
            return ret;
        }
        ret = input_param_list(fun_ptr, list);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            list_free(list);
            return ret;
        }
        
        if (token.type != TOKEN_RIGHT_PARENTHESIS) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
            list_free(list);
            return E_SYN;
        }

        //control param list
        //generate instructions
        
        if (stringCmpConstString(&(fun_to_call->key), "length") == 0) {
            if (list->first_ptr == NULL || list->first_ptr->next_ptr != NULL) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                list_free(list);
                return E_SEM_TYPE;
            }

            switch (list->first_ptr->st_item_ptr->data.type) {
            case DATA_TYPE_VAR:
                if (list->first_ptr->st_item_ptr->data.value.variable.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_FUN_ITEM:
                if (list->first_ptr->st_item_ptr->data.value.fun_item.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_CONST:
                if (list->first_ptr->st_item_ptr->data.value.constant.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            }
            
            ret = generate_instruction(I_FUN_LENGTH, list->first_ptr->st_item_ptr, NULL, NULL);
            if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                list_free(list);
                return E_INTERN;
            }
            list_free(list);
        } else if (stringCmpConstString(&(fun_to_call->key), "copy") == 0) {
            if (list->first_ptr == NULL || list->first_ptr->next_ptr == NULL ||
                list->first_ptr->next_ptr->next_ptr == NULL ||
                list->first_ptr->next_ptr->next_ptr->next_ptr != NULL) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                list_free(list);
                return E_SEM_TYPE;
            }

            switch (list->first_ptr->st_item_ptr->data.type) {
            case DATA_TYPE_VAR:
                if (list->first_ptr->st_item_ptr->data.value.variable.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_FUN_ITEM:
                if (list->first_ptr->st_item_ptr->data.value.fun_item.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_CONST:
                if (list->first_ptr->st_item_ptr->data.value.constant.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            }
            
            switch (list->first_ptr->next_ptr->st_item_ptr->data.type) {
            case DATA_TYPE_VAR:
                if (list->first_ptr->next_ptr->st_item_ptr->data.value.variable.type != TYPE_INT) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_FUN_ITEM:
                if (list->first_ptr->next_ptr->st_item_ptr->data.value.fun_item.type != TYPE_INT) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_CONST:
                if (list->first_ptr->next_ptr->st_item_ptr->data.value.constant.type != TYPE_INT) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            }
            
            switch (list->first_ptr->next_ptr->next_ptr->st_item_ptr->data.type) {
            case DATA_TYPE_VAR:
                if (list->first_ptr->next_ptr->next_ptr->st_item_ptr->data.value.variable.type != TYPE_INT) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_FUN_ITEM:
                if (list->first_ptr->next_ptr->next_ptr->st_item_ptr->data.value.fun_item.type != TYPE_INT) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_CONST:
                if (list->first_ptr->next_ptr->next_ptr->st_item_ptr->data.value.constant.type != TYPE_INT) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            }
            
            ret = generate_instruction(I_FUN_COPY, list->first_ptr->st_item_ptr, list->first_ptr->next_ptr->st_item_ptr, list->first_ptr->next_ptr->next_ptr->st_item_ptr);
            if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                list_free(list);
                return E_INTERN;
            }
            list_free(list);
        } else if (stringCmpConstString(&(fun_to_call->key), "find") == 0) {
            if (list->first_ptr == NULL || list->first_ptr->next_ptr == NULL ||
                list->first_ptr->next_ptr->next_ptr != NULL) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                list_free(list);
                return E_SEM_TYPE;
            }

            switch (list->first_ptr->st_item_ptr->data.type) {
            case DATA_TYPE_VAR:
                if (list->first_ptr->st_item_ptr->data.value.variable.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_FUN_ITEM:
                if (list->first_ptr->st_item_ptr->data.value.fun_item.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_CONST:
                if (list->first_ptr->st_item_ptr->data.value.constant.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            }
            
            switch (list->first_ptr->next_ptr->st_item_ptr->data.type) {
            case DATA_TYPE_VAR:
                if (list->first_ptr->next_ptr->st_item_ptr->data.value.variable.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_FUN_ITEM:
                if (list->first_ptr->next_ptr->st_item_ptr->data.value.fun_item.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_CONST:
                if (list->first_ptr->next_ptr->st_item_ptr->data.value.constant.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            }
            
            ret = generate_instruction(I_FUN_FIND, list->first_ptr->st_item_ptr, list->first_ptr->next_ptr->st_item_ptr, NULL);
            if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                list_free(list);
                return E_INTERN;
            }
            list_free(list);
        } else if (stringCmpConstString(&(fun_to_call->key), "sort") == 0) {
            if (list->first_ptr == NULL || list->first_ptr->next_ptr != NULL) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                list_free(list);
                return E_SEM_TYPE;
            }

            switch (list->first_ptr->st_item_ptr->data.type) {
            case DATA_TYPE_VAR:
                if (list->first_ptr->st_item_ptr->data.value.variable.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_FUN_ITEM:
                if (list->first_ptr->st_item_ptr->data.value.fun_item.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            case DATA_TYPE_CONST:
                if (list->first_ptr->st_item_ptr->data.value.constant.type != TYPE_STR) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }
                break;
            }
            
            ret = generate_instruction(I_FUN_SORT, list->first_ptr->st_item_ptr, NULL, NULL);
            if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                list_free(list);
                return E_INTERN;
            }
            list_free(list);
        } else {
            t_list_item * tmp_list;
            t_tab_item * tmp_tab;

            for (tmp_tab = fun_to_call->data.value.function.first_param,
                 tmp_list = list->first_ptr; tmp_tab != NULL; tmp_tab = tmp_tab->data.value.fun_item.next, tmp_list = tmp_list->next_ptr) {
                if (tmp_list == NULL) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                    list_free(list);
                    return E_SEM_TYPE;
                }

                switch (tmp_list->st_item_ptr->data.type) {
                case DATA_TYPE_VAR:
                    if (tmp_list->st_item_ptr->data.value.variable.type != tmp_tab->data.value.fun_item.type) {
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                        list_free(list);
                        return E_SEM_TYPE;
                    }
                    break;
                case DATA_TYPE_FUN_ITEM:
                    if (tmp_list->st_item_ptr->data.value.fun_item.type != tmp_tab->data.value.fun_item.type) {
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                        list_free(list);
                        return E_SEM_TYPE;
                    }
                    break;
                case DATA_TYPE_CONST:
                    if (tmp_list->st_item_ptr->data.value.constant.type != tmp_tab->data.value.fun_item.type) {
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                        list_free(list);
                        return E_SEM_TYPE;
                    }
                    break;
                }
            }
        
            if (tmp_tab == NULL && tmp_list != NULL) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SEM_TYPE, token.type);
#endif
                list_free(list);
                return E_SEM_TYPE;
            }

            ret = generate_instruction(I_FUN_CALL, fun_to_call, list, NULL);
            if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                list_free(list);
                return E_INTERN;
            }

            ret = generate_instruction(I_LABEL, NULL, NULL, NULL);
            if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
                return E_INTERN;
            }
        }

        ret = getNextToken();
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}

int input_param_list(t_tab_item * fun_ptr, t_list * list)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    int type;
    t_tab_item * item_ptr;

    if (token.type == TOKEN_RIGHT_PARENTHESIS) {
        //rule 35
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    } else {
        //rule 36
        ret = process_expression(fun_ptr, &type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        t_string tmp_key_string;
        ret = stringInit(&tmp_key_string);
        if (ret != STR_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        generate_key(&tmp_key_string);
        ret = table_insert(symbol_table, &tmp_key_string, &item_ptr);
        stringFree(&tmp_key_string);
        if (ret != TAB_INSERT_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return E_INTERN;
        }

        item_ptr->data.type = DATA_TYPE_CONST;
        item_ptr->data.value.constant.type = type;
        if (type == TYPE_STR) {
            item_ptr->data.value.constant.value.string.string = NULL;
        }

        ret = generate_instruction(I_ASSIGN, item_ptr, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return E_INTERN;
        }
        
        ret = list_insert_last(list, item_ptr);
        if (ret != LIST_OP_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        ret = input_param_list_part(fun_ptr, list);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    }
}

int input_param_list_part(t_tab_item * fun_ptr, t_list * list)
{
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | %s\n", __LINE__, __FILE__, __func__);
#endif

    int ret;
    int type;
    t_tab_item * item_ptr;

    if (token.type == TOKEN_COMMA) {
        //rule 37
        ret = getNextToken();
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }
        
        ret = process_expression(fun_ptr, &type);
        if (ret != E_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return ret;
        }

        t_string tmp_key_string;
        ret = stringInit(&tmp_key_string);
        if (ret != STR_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }
        generate_key(&tmp_key_string);
        ret = table_insert(symbol_table, &tmp_key_string, &item_ptr);
        stringFree(&tmp_key_string);
        if (ret != TAB_INSERT_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return E_INTERN;
        }

        item_ptr->data.type = DATA_TYPE_CONST;
        item_ptr->data.value.constant.type = type;
        if (type == TYPE_STR) {
            item_ptr->data.value.constant.value.string.string = NULL;
        }

        ret = generate_instruction(I_ASSIGN, item_ptr, NULL, NULL);
        if (ret != LIST_OPERATION_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
            return E_INTERN;
        }
        
        ret = list_insert_last(list, item_ptr);
        if (ret != LIST_OP_OK) {
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_INTERN, token.type);
#endif
            return E_INTERN;
        }

        ret = input_param_list_part(fun_ptr, list);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, ret, token.type);
#endif
        return ret;
    } else if (token.type == TOKEN_RIGHT_PARENTHESIS) {
        //rule 38
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_OK, token.type);
#endif
        return E_OK;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | end %s; return: %d token_type: %d\n", __LINE__, __FILE__, __func__, E_SYN, token.type);
#endif
    return E_SYN;
}
