/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   scanner.c
 * Datum:   11.10. 2014
 * Autori:  Frederik Muller, xmulle20@stud.fit.vutbr.cz
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <limits.h>
#include "common.h"
#include "string.h"
#include "scanner.h"

#define OFFSET_KEYWORD TOKEN_KW_BEGIN
#define NUMBER_OF_KEYWORDS 20
#define APOSTROPHE 39
#define MIN_NON_ESCAPE 31
#define MIN_ESCAPE 1
#define MAX_ESCAPE 255



char *keyword[] = {"begin","boolean","do","else","end","false","forward","function","if","integer",
"readln","real","string","then","true","var","while","write","repeat","until"}; //ARRAY OF KEYWORDS

int open_File(int argc, char *argv[]) //FUNCTION FOR OPENING FILE
{
    if (argc==2){
        source_file=fopen(argv[1], "rw");
        if (source_file==NULL)  
            return 1; //could not open file 
    }
    else return 1;
    return 0;
}

void close_File(void)   // FUNCTION FOR CLOSING FILE
{
    fclose(source_file);
}

int getNextToken (void)
{
    int end; //FOR CHECKING END_OF_FILE
    int character; //READING SOURCE_FILE VARIABLE 
    int state;
    state = STATE_START; // STATE_START POSITION
    stringClear(&token.attr.lit_string_or_id);
    t_string auxiliary_array;   

    while ((character=fgetc(source_file)) != EOF) { //READING CHARACTERS FROM INPUT FILE
    switch (state){
    case STATE_START: {
        if (isspace(character)) //white character
            ;
        else if (character == ';') {
            token.type=TOKEN_SEMICOLON;
            return E_OK;
        } else if (character == '(') {
            token.type=TOKEN_LEFT_PARENTHESIS;
            return E_OK;
        } else if (character == ')') {
            token.type=TOKEN_RIGHT_PARENTHESIS;
            return E_OK;
        } else if (character == '/') {
            token.type=TOKEN_DIVISION;
            return E_OK;
        } else if (character == '*') {
            token.type=TOKEN_MULTIPLICATION;
            return E_OK;
        } else if (character == ',') {
            token.type=TOKEN_COMMA;
            return E_OK;
        } else if (character == '.') {
            token.type=TOKEN_DOT;
            return E_OK;
        } else if (character == '-') {
            token.type=TOKEN_MINUS;
            return E_OK;
        } else if (character == '+') {
            token.type=TOKEN_PLUS;
            return E_OK;
        } else if (character == '=') {
            token.type=TOKEN_EQUAL;
            return E_OK;
        } else if (character == '<') {
            state=STATE_LESS_THAN;
            end = fgetc(source_file);   //checking EOF
            if (end == EOF) {
                token.type = TOKEN_LESS_THAN;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else if (character == '>') {
            state=STATE_GREATER_THAN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                token.type = TOKEN_GREATER_THAN;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else if (character == ':') {
            state=STATE_COLON;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                token.type = TOKEN_COLON;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else if (character == '{') {
            state=STATE_COMMENT;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if ((character>='a' && character<='z') ||  (character>='A' && character<='Z') ||  (character=='_')) {
            state=STATE_IDENTIFIER;
            if (character >= 'A' && character <= 'Z') //uppercase to lowercase
                character=tolower(character);
            if (stringAddChar(&token.attr.lit_string_or_id, character)==STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                token.type = TOKEN_IDENTIFIER;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else if (character == APOSTROPHE) { 
            state=STATE_LIT_STRING1;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if (character == '$') { 
            state=STATE_LIT_INTEGER_HEXADECIMAL1;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if (character == '&') { 
            state=STATE_LIT_INTEGER_OCTAL1;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if (character == '%') { 
            state=STATE_LIT_INTEGER_BINARY1;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if (character >= '0' && character <= '9') {
            state=STATE_LIT_INTEGER;
            if (stringInit(&auxiliary_array) == STR_ALLOC_FAIL)
                return E_INTERN;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                int number;
                number=strtol(auxiliary_array.string, NULL, 10); //DECIMAL.. checking overflow no needed (only one number)
                token.attr.lit_int=number;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_INTEGER;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else
            return E_LEX; //UNKNOWN INPUT
        } 
        break;
    case STATE_LESS_THAN: {
        if (character == '=') {
            token.type=TOKEN_LESS_THAN_OR_EQUAL;
            return E_OK;
        } else if (character == '>') {
            token.type=TOKEN_NOT_EQUAL;
            return E_OK;
        } else {
            token.type=TOKEN_LESS_THAN;
            ungetc (character,source_file);
            return E_OK;
        }
        }
        break;
    case STATE_GREATER_THAN: {
        if (character == '=') {
            token.type=TOKEN_GREATER_THAN_OR_EQUAL;
            return E_OK;
        } else {
            token.type=TOKEN_GREATER_THAN;
            ungetc (character,source_file);
            return E_OK;
        }
        }
        break;
    case STATE_COLON: {
        if (character == '=') {
            token.type=TOKEN_ASSIGNMENT;
            return E_OK;
        } else {
            token.type=TOKEN_COLON;
            ungetc (character,source_file);
            return E_OK;
        }
        }
        break;
    case STATE_COMMENT: {
        if (character == '}') 
            state=STATE_START; //no token, no return
        else {
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        }
        }
        break;
    case STATE_IDENTIFIER: {
        if ((character>='a' && character<='z') || (character>='A' && character<='Z') || (character=='_') || (character>='0' && character<='9')) {
            if (character >= 'A' && character <= 'Z') //uppercase to lowercase
                character=tolower(character);
            if (stringAddChar(&token.attr.lit_string_or_id, character)==STR_ALLOC_FAIL)
                return E_INTERN;
        }
        else {
            int i;
            for (i=0; i<NUMBER_OF_KEYWORDS; i++) { //IS KEYWORD ?
                if((strcmp(keyword[i],token.attr.lit_string_or_id.string)) == 0){
                    if (strcmp(token.attr.lit_string_or_id.string, "true") == 0){
                        ungetc (character,source_file);
                        token.attr.lit_int = 1; 
                        token.type = TOKEN_LIT_BOOLEAN;
                        return E_OK;
                    } else if (strcmp(token.attr.lit_string_or_id.string, "false") == 0){
                        ungetc (character,source_file);
                        token.attr.lit_int = 0; 
                        token.type = TOKEN_LIT_BOOLEAN;
                        return E_OK;
                    } 
                    token.type = i + OFFSET_KEYWORD;
                    ungetc (character,source_file); 
                    return E_OK;
                }
            }
            token.type=TOKEN_IDENTIFIER;
            ungetc (character,source_file); 
            return E_OK;
        }
        end = fgetc(source_file);   //checking EOF
        if (end == EOF) {
            int i;
            for (i=0; i<NUMBER_OF_KEYWORDS; i++) { //IS KEYWORD ?
                if((strcmp(keyword[i],token.attr.lit_string_or_id.string)) == 0){
                    if (strcmp(token.attr.lit_string_or_id.string, "true") == 0){
                        token.attr.lit_int = 1; 
                        token.type = TOKEN_LIT_BOOLEAN;
                        ungetc(end,source_file);
                        return E_OK;
                    } else if (strcmp(token.attr.lit_string_or_id.string, "false") == 0){
                        token.attr.lit_int = 0; 
                        token.type = TOKEN_LIT_BOOLEAN;
                        ungetc(end,source_file);
                        return E_OK;
                    }
                    token.type = i + OFFSET_KEYWORD;
                    ungetc(end,source_file);
                    return E_OK;
                    }
                }
                token.type=TOKEN_IDENTIFIER;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        }
        break;
    case STATE_LIT_STRING1: {
        if (character <= MIN_NON_ESCAPE) { 
            return E_LEX;
        } else if (character == APOSTROPHE) { 
            state=STATE_LIT_STRING3; 
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                token.type = TOKEN_LIT_STRING;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else {
            if (stringAddChar(&token.attr.lit_string_or_id, character)==STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        }
        }
        break;
    case STATE_LIT_STRING3: {
        if (character == APOSTROPHE) { 
            state=STATE_LIT_STRING1; //STORE '
            if (stringAddChar(&token.attr.lit_string_or_id, character)==STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if (character == '#') {
            state=STATE_LIT_STRING4; 
            if (stringInit(&auxiliary_array)== STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                stringFree(&auxiliary_array);
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else {
            ungetc (character,source_file);
            token.type=TOKEN_LIT_STRING;
            return E_OK;
        }
        }
        break;
    case STATE_LIT_STRING4: {
        if (character == '0') {
            state=STATE_LIT_STRING5; //ignoring zeros
        } else if (character >= '1' && character <= '9') {
            state=STATE_LIT_STRING2; 
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;
            }
        } else if (character == '%') {
            state=STATE_LIT_STRING_BINARY1; 
        } else if (character == '&') {
            state=STATE_LIT_STRING_OCTAL1; 
        } else if (character == '$') {
            state=STATE_LIT_STRING_HEXADECIMAL1; 
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        end = fgetc(source_file);    //checking EOF
        if (end == EOF) {
            stringFree(&auxiliary_array);
            ungetc(end,source_file);
            return E_LEX;
        }
        ungetc(end,source_file);
        }
        break;
    case STATE_LIT_STRING_BINARY1: {
        if (character == '0') {
            ; //ignoring zeros
        }
        else if (character == '1'){
            state=STATE_LIT_STRING_BINARY2;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL){
                stringFree(&auxiliary_array);
                return E_INTERN;
            }
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        end = fgetc(source_file);    //checking EOF
        if (end == EOF) {
            stringFree(&auxiliary_array);
            ungetc(end,source_file);
            return E_LEX;
        }
        ungetc(end,source_file);
        }
        break;
    case STATE_LIT_STRING_BINARY2: {
        if (character == '0' || character == '1') {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                stringFree(&auxiliary_array);
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if (character == APOSTROPHE) { 
            state=STATE_LIT_STRING1; 
            long int number1; 
            int number2;
            number1=strtol(auxiliary_array.string, NULL, 2);    // BINARY
            if (errno==ERANGE) {
                stringFree(&auxiliary_array);
                return E_LEX;
            } 
            if (number1>INT_MAX) {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
            number2=(int)number1;
            if (number2>=MIN_ESCAPE && number2<=MAX_ESCAPE) {
                if (stringAddChar(&token.attr.lit_string_or_id, number2)==STR_ALLOC_FAIL) {
                    stringFree(&auxiliary_array);
                    return E_INTERN;
                }
                else stringFree(&auxiliary_array); 
            } else {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        end = fgetc(source_file);    //checking EOF
        if (end == EOF) {
            ungetc(end,source_file);
            return E_LEX;
        }
        ungetc(end,source_file);
        }
        break;   
    case STATE_LIT_STRING_OCTAL1: {
        if (character == '0') {
            ; //ignoring zeros
        }
        else if (character >= '1' && character <= '7'){
            state=STATE_LIT_STRING_OCTAL2;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL){
                stringFree(&auxiliary_array);
                return E_INTERN;
            }
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        end = fgetc(source_file);    //checking EOF
        if (end == EOF) {
            stringFree(&auxiliary_array);
            ungetc(end,source_file);
            return E_LEX;
        }
        ungetc(end,source_file);
        }
        break;
    case STATE_LIT_STRING_OCTAL2: {
        if (character >= '0' && character <= '7'){
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                stringFree(&auxiliary_array);
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if (character == APOSTROPHE) { 
            state=STATE_LIT_STRING1; 
            long int number1; 
            int number2;
            number1=strtol(auxiliary_array.string, NULL, 8);    // OCTAL
            if (errno==ERANGE) {
                stringFree(&auxiliary_array);
                return E_LEX;
            } 
            if (number1>INT_MAX) {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
            number2=(int)number1;
            if (number2>=MIN_ESCAPE && number2<=MAX_ESCAPE) {
                if (stringAddChar(&token.attr.lit_string_or_id, number2)==STR_ALLOC_FAIL) {
                    stringFree(&auxiliary_array);
                    return E_INTERN;
                }
                else stringFree(&auxiliary_array); 
            } else {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        end = fgetc(source_file);    //checking EOF
        if (end == EOF) {
            ungetc(end,source_file);
            return E_LEX;
        }
        ungetc(end,source_file);
        }
        break;   
    case STATE_LIT_STRING_HEXADECIMAL1: {
        if (character == '0') {
            ; //ignoring zeros
        }
        else if ((character >= '1' && character <= '9') || (character >= 'a' && character <= 'f') || (character >= 'A' && character <= 'F')) {
            state=STATE_LIT_STRING_HEXADECIMAL2;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL){
                stringFree(&auxiliary_array);
                return E_INTERN;
            }
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        end = fgetc(source_file);    //checking EOF
        if (end == EOF) {
            stringFree(&auxiliary_array);
            ungetc(end,source_file);
            return E_LEX;
        }
        ungetc(end,source_file);
        }
        break;
    case STATE_LIT_STRING_HEXADECIMAL2: {
        if ((character >= '0' && character <= '9') || (character >= 'a' && character <= 'f') || (character >= 'A' && character <= 'F')) {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                stringFree(&auxiliary_array);
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if (character == APOSTROPHE) { 
            state=STATE_LIT_STRING1; 
            long int number1; 
            int number2;
            number1=strtol(auxiliary_array.string, NULL, 16);   // HEXADECIMAL
            if (errno==ERANGE) {
                stringFree(&auxiliary_array);
                return E_LEX;
            } 
            if (number1>INT_MAX) {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
            number2=(int)number1;
            if (number2>=MIN_ESCAPE && number2<=MAX_ESCAPE) {
                if (stringAddChar(&token.attr.lit_string_or_id, number2)==STR_ALLOC_FAIL) {
                    stringFree(&auxiliary_array);
                    return E_INTERN;
                }
                else stringFree(&auxiliary_array); 
            } else {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        end = fgetc(source_file);    //checking EOF
        if (end == EOF) {
            ungetc(end,source_file);
            return E_LEX;
        }
        ungetc(end,source_file);
        }
        break;   
    case STATE_LIT_STRING5: {
        if (character == '0') {
            state=STATE_LIT_STRING5; //ignoring zeros
        } else if (character >= '1' && character <= '9') {
            state=STATE_LIT_STRING2; 
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL){
                stringFree(&auxiliary_array);
                return E_INTERN;
            }
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        end = fgetc(source_file);    //checking EOF
        if (end == EOF) {
            stringFree(&auxiliary_array);
            ungetc(end,source_file);
            return E_LEX;
        }
        ungetc(end,source_file);
        }
        break;
    case STATE_LIT_STRING2: {
        if (character >= '0' && character <= '9') {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                stringFree(&auxiliary_array);
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if (character == APOSTROPHE) { 
            state=STATE_LIT_STRING1; 
            long int number1; 
            int number2;
            number1=strtol(auxiliary_array.string, NULL, 10);   // DECIMAL
            if (errno==ERANGE) {
                stringFree(&auxiliary_array);
                return E_LEX;
            } 
            if (number1>INT_MAX) {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
            number2=(int)number1;
            if (number2>=MIN_ESCAPE && number2<=MAX_ESCAPE) {
                if (stringAddChar(&token.attr.lit_string_or_id, number2)==STR_ALLOC_FAIL) {
                    stringFree(&auxiliary_array);
                    return E_INTERN;
                }
                else stringFree(&auxiliary_array); 
            } else {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        end = fgetc(source_file);    //checking EOF
        if (end == EOF) {
            ungetc(end,source_file);
            return E_LEX;
        }
        ungetc(end,source_file);
        }
        break;
    case STATE_LIT_INTEGER: {
        if (character >= '0' && character <= '9') {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;  
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                long int number1;
                int number2;
                number1=strtol(auxiliary_array.string, NULL, 10);   // DECIMAL
                if (errno==ERANGE) {
                    stringFree(&auxiliary_array); 
                    return E_LEX;
                } 
                if (number1>INT_MAX) {
                    stringFree(&auxiliary_array);
                    return E_LEX;
                }
                number2=(int)number1;
                token.attr.lit_int=number2;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_INTEGER;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else if (character == 'e' || character == 'E') {
            state=STATE_LIT_REAL2;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;      
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                stringFree(&auxiliary_array);
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else if (character == '.') {
            state=STATE_LIT_REAL6;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;      
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                stringFree(&auxiliary_array);
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else {
            ungetc (character,source_file);
            long int number1;
            int number2;
            number1=strtol(auxiliary_array.string, NULL, 10);   // DECIMAL
            if (errno==ERANGE) {
                stringFree(&auxiliary_array); 
                return E_LEX;
            } 
            if (number1>INT_MAX) {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
            number2=(int)number1;
            token.type=TOKEN_LIT_INTEGER;
            token.attr.lit_int=number2;
            stringFree(&auxiliary_array);
            return E_OK;
        }
        }
        break;
    case STATE_LIT_REAL6: {
        if (character >= '0' && character <= '9') {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;      
            }
            state=STATE_LIT_REAL1;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                double number;
                number=strtod(auxiliary_array.string, NULL);
                if (errno==ERANGE) {
                    stringFree(&auxiliary_array); 
                    return E_LEX;
                } 
                token.attr.lit_double=number;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_REAL;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }

        }
        break;
    case STATE_LIT_REAL1: {
        if (character >= '0' && character <= '9') {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;      
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                double number;
                number=strtod(auxiliary_array.string, NULL);
                if (errno==ERANGE) {
                    stringFree(&auxiliary_array); 
                    return E_LEX;
                } 
                token.attr.lit_double=number;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_REAL;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else if (character == 'e' || character == 'E') {
            state=STATE_LIT_REAL2;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;      
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                stringFree(&auxiliary_array);
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else {
            ungetc (character,source_file);
            double number;
            number=strtod(auxiliary_array.string,NULL);
            if (errno==ERANGE) {
                stringFree(&auxiliary_array); 
                return E_LEX;
            } 
            token.type=TOKEN_LIT_REAL;
            token.attr.lit_double=number;
            stringFree(&auxiliary_array);
            return E_OK;
        }
        }
        break;
    case STATE_LIT_REAL2: {
        if (character >= '0' && character <= '9') {
            state=STATE_LIT_REAL3;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;      
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                double number;
                number=strtod(auxiliary_array.string, NULL);
                if (errno==ERANGE) {
                    stringFree(&auxiliary_array); 
                    return E_LEX;
                } 
                token.attr.lit_double=number;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_REAL;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else if (character == '+' || character == '-') {
            state=STATE_LIT_REAL4;    
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;      
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                stringFree(&auxiliary_array);
                ungetc(end,source_file);
                return E_LEX;
            }
            ungetc(end,source_file);
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        }
        break;
    case STATE_LIT_REAL3: {
        if (character >= '0' && character <= '9') {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;      
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                double number;
                number=strtod(auxiliary_array.string, NULL);
                if (errno==ERANGE) {
                    stringFree(&auxiliary_array); 
                    return E_LEX;
                } 
                token.attr.lit_double=number;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_REAL;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else {
            ungetc (character,source_file);
            double number;
            number=strtod(auxiliary_array.string,NULL);
            if (errno==ERANGE) {
                stringFree(&auxiliary_array); 
                return E_LEX;
            } 
            token.type=TOKEN_LIT_REAL;
            token.attr.lit_double=number;
            stringFree(&auxiliary_array);
            return E_OK;
        }
        }
        break;
    case STATE_LIT_REAL4: {
        if (character >= '0' && character <= '9') {
            state=STATE_LIT_REAL5;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;      
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                double number;
                number=strtod(auxiliary_array.string, NULL);
                if (errno==ERANGE) {
                    stringFree(&auxiliary_array); 
                    return E_LEX;
                } 
                token.attr.lit_double=number;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_REAL;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else {
            stringFree(&auxiliary_array);
            return E_LEX;
        }
        }
        break;
    case STATE_LIT_REAL5: {
        if (character >= '0' && character <= '9') {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL) {
                stringFree(&auxiliary_array);
                return E_INTERN;      
            }
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                double number;
                number=strtod(auxiliary_array.string, NULL);
                if (errno==ERANGE) {
                    stringFree(&auxiliary_array); 
                    return E_LEX;
                } 
                token.attr.lit_double=number;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_REAL;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else {
            ungetc (character,source_file);
            double number;
            number=strtod(auxiliary_array.string, NULL);
            if (errno==ERANGE) {
                stringFree(&auxiliary_array); 
                return E_LEX;
            } 
            token.type=TOKEN_LIT_REAL;
            token.attr.lit_double=number;
            stringFree(&auxiliary_array);
            return E_OK;
        }
        }
        break;
        case STATE_LIT_INTEGER_BINARY1: {
        if (character == '0' || character == '1') {
            state=STATE_LIT_INTEGER_BINARY2;
            if (stringInit(&auxiliary_array) == STR_ALLOC_FAIL)
                return E_INTERN;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                int number;
                number=strtol(auxiliary_array.string, NULL, 2); //BINARY.. overflow no needed - only 1 number
                token.attr.lit_int=number;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_INTEGER;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else 
            return E_LEX; 
        } 
        break;
        case STATE_LIT_INTEGER_BINARY2: {
        if (character == '0' || character == '1') {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                long int number1; 
                int number2;
                number1=strtol(auxiliary_array.string, NULL, 2);    // BINARY
                if (errno==ERANGE) {
                    stringFree(&auxiliary_array);
                    return E_LEX;
                } 
                if (number1>INT_MAX) {
                    stringFree(&auxiliary_array);
                    return E_LEX;
                }
                number2=(int)number1;
                token.attr.lit_int=number2;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_INTEGER;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else {
            long int number1; 
            int number2;
            number1=strtol(auxiliary_array.string, NULL, 2);    // BINARY
            if (errno==ERANGE) {
                stringFree(&auxiliary_array);
                return E_LEX;
            } 
            if (number1>INT_MAX) {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
            number2=(int)number1;
            token.attr.lit_int=number2;
            stringFree(&auxiliary_array);
            token.type = TOKEN_LIT_INTEGER;
            ungetc (character,source_file);
            return E_OK;
        }
        } 
        break;
        case STATE_LIT_INTEGER_OCTAL1: {
        if (character >= '0' && character <= '7') {
            state=STATE_LIT_INTEGER_OCTAL2;
            if (stringInit(&auxiliary_array) == STR_ALLOC_FAIL)
                return E_INTERN;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                int number;
                number=strtol(auxiliary_array.string, NULL, 8); //OCTAL.. overflow no needed - only 1 number
                token.attr.lit_int=number;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_INTEGER;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else 
            return E_LEX; 
        } 
        break;
        case STATE_LIT_INTEGER_OCTAL2: {
        if (character >= '0' && character <= '7') {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                long int number1; 
                int number2;
                number1=strtol(auxiliary_array.string, NULL, 8);    // OCTAL
                if (errno==ERANGE) {
                    stringFree(&auxiliary_array);
                    return E_LEX;
                } 
                if (number1>INT_MAX) {
                    stringFree(&auxiliary_array);
                    return E_LEX;
                }
                number2=(int)number1;
                token.attr.lit_int=number2;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_INTEGER;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else {
            long int number1; 
            int number2;
            number1=strtol(auxiliary_array.string, NULL, 8);    // OCTAL
            if (errno==ERANGE) {
                stringFree(&auxiliary_array);
                return E_LEX;
            } 
            if (number1>INT_MAX) {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
            number2=(int)number1;
            token.attr.lit_int=number2;
            stringFree(&auxiliary_array);
            token.type = TOKEN_LIT_INTEGER;
            ungetc (character,source_file);
            return E_OK;
        }
        } 
        break;
        case STATE_LIT_INTEGER_HEXADECIMAL1: {
        if ((character >= '0' && character <= '9') || (character >= 'a' && character <= 'f') || (character >= 'A' && character <= 'F')) {
            state=STATE_LIT_INTEGER_HEXADECIMAL2;
            if (stringInit(&auxiliary_array) == STR_ALLOC_FAIL)
                return E_INTERN;
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                int number;
                number=strtol(auxiliary_array.string, NULL, 16); //HEXADECIMAL.. overflow no needed - only 1 number
                token.attr.lit_int=number;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_INTEGER;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else 
            return E_LEX; 
        } 
        break;
        case STATE_LIT_INTEGER_HEXADECIMAL2: {
        if ((character >= '0' && character <= '9') || (character >= 'a' && character <= 'f') || (character >= 'A' && character <= 'F')) {
            if (stringAddChar(&auxiliary_array, character)==STR_ALLOC_FAIL)
                return E_INTERN;
            end = fgetc(source_file);    //checking EOF
            if (end == EOF) {
                long int number1; 
                int number2;
                number1=strtol(auxiliary_array.string, NULL, 16);   // HEXADECIMAL
                if (errno==ERANGE) {
                    stringFree(&auxiliary_array);
                    return E_LEX;
                } 
                if (number1>INT_MAX) {
                    stringFree(&auxiliary_array);
                    return E_LEX;
                }
                number2=(int)number1;
                token.attr.lit_int=number2;
                stringFree(&auxiliary_array);
                token.type = TOKEN_LIT_INTEGER;
                ungetc(end,source_file);
                return E_OK;
            }
            ungetc(end,source_file);
        } else {
            long int number1; 
            int number2;
            number1=strtol(auxiliary_array.string, NULL, 16);   // HEXADECIMAL
            if (errno==ERANGE) {
                stringFree(&auxiliary_array);
                return E_LEX;
            } 
            if (number1>INT_MAX) {
                stringFree(&auxiliary_array);
                return E_LEX;
            }
            number2=(int)number1;
            token.attr.lit_int=number2;
            stringFree(&auxiliary_array);
            token.type = TOKEN_LIT_INTEGER;
            ungetc (character,source_file);
            return E_OK;
        }
        } 
        break;
    }
    }
    token.type=TOKEN_END_OF_FILE; 
    return E_OK;
}

