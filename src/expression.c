/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   expression.c
 * Datum:   12.11. 2014
 * Autori:  Jergus Jacko, xjacko03@stud.fit.vutbr.cz
 */

#include "expression.h"

// precedent table
const char precedent_table[END_EXPRESSION + 1][END_EXPRESSION + 1] = {

//TOKEN_TYPE                                             id  int rl str bool  (   )   +   -   *   /   <   >   <= >=   =  <>   $ 
    [TOKEN_IDENTIFIER]              ={[TOKEN_IDENTIFIER]= 0 , 0 , 0 , 0 , 0 , 0 ,'>','>','>','>','>','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_LIT_INTEGER]             ={[TOKEN_IDENTIFIER]= 0 , 0 , 0 , 0 , 0 , 0 ,'>','>','>','>','>','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_LIT_REAL]                ={[TOKEN_IDENTIFIER]= 0 , 0 , 0 , 0 , 0 , 0 ,'>','>','>','>','>','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_LIT_STRING]              ={[TOKEN_IDENTIFIER]= 0 , 0 , 0 , 0 , 0 , 0 ,'>','>','>','>','>','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_LIT_BOOLEAN]             ={[TOKEN_IDENTIFIER]= 0 , 0 , 0 , 0 , 0 , 0 ,'>','>','>','>','>','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_LEFT_PARENTHESIS]        ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','=','<','<','<','<','<','<','<','<','<','<',[END_EXPRESSION]= 0 },
    [TOKEN_RIGHT_PARENTHESIS]       ={[TOKEN_IDENTIFIER]= 0 , 0 , 0 , 0 , 0 , 0 ,'>','>','>','>','>','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_PLUS]                    ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','>','>','<','<','<','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_MINUS]                   ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','>','>','<','<','<','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_MULTIPLICATION]          ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','>','>','<','>','>','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_DIVISION]                ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','>','>','<','>','>','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_LESS_THAN]               ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','>','<','<','<','<','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_GREATER_THAN]            ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','>','<','<','<','<','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_LESS_THAN_OR_EQUAL]      ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','>','<','<','<','<','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_GREATER_THAN_OR_EQUAL]   ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','>','<','<','<','<','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_EQUAL]                   ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','>','<','<','<','<','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [TOKEN_NOT_EQUAL]               ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<','>','<','<','<','<','>','>','>','>','>','>',[END_EXPRESSION]='>'},
    [END_EXPRESSION]                ={[TOKEN_IDENTIFIER]='<','<','<','<','<','<', 0 ,'<','<','<','<','<','<','<','<','<','<',[END_EXPRESSION]= 0 },
};

//semanic table
const char semantic_table[TYPE_BOOL+1][END_EXPRESSION] = {

//data_type                     +   -   *   /   <   >   <=  >=  =   <>
    [TYPE_INT]  ={[TOKEN_PLUS]= 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 },
    [TYPE_REAL] ={[TOKEN_PLUS]= 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 },
    [TYPE_STR]  ={[TOKEN_PLUS]= 1 , 0 , 0 , 0 , 1 , 1 , 1 , 1 , 1 , 1 },
    [TYPE_BOOL] ={[TOKEN_PLUS]= 0 , 0 , 0 , 0 , 1 , 1 , 1 , 1 , 1 , 1 },
};

int top_and_pop(t_stack *S, t_tab_item **item_ptr);
int shift(t_stack *S, t_tab_item *item_ptr, int *type);
int top_terminal(t_stack *S);
int apply_rule(t_stack *S);
int top_type(t_stack *S);
int one_under_top_terminal(t_stack *S);
bool fun_call = false; //for ex_funexp, to correct end of expression, in last parameter in function call

/**
 * \brief Function processing expression with precedent analyze down-up.
 *
 * @param   fun_ptr     pointer to function, in which is located processed expression
 *                      if NULL, processed expression is located in main
 * @param   result_type result data type of processed expression
 * @return  err code    E_OK, E_INTERN, E_SEM_DEF, E_SEM_OTHER, E_SYN, E_SEM_TYPE
 */
int process_expression(t_tab_item * fun_ptr, int *result_type)
{
    int err;
    t_stack * S;
    int end = END_EXPRESSION;   //base of the stack
    int top = END_EXPRESSION;    //nearest terminal to peak of stack, initial value is "$"
    int expression = EXPRESSION;  //expression type for stack to use as pointer
    t_tab_item *item_ptr;
    t_string tmp;
    bool found = false; //for identifier, if found in function local table
    int token_type; //all others tokens (not in precedent table) are considered as END_EXPRESSION "$" types
    int topType; //for ex_minus, top type on stack
    int oneUnderTopTerminal = END_EXPRESSION; //for ex_minus, one item under terminal on stack
    bool next_token = false; //bool for ex_funexp
    int right_parenthesis = TOKEN_RIGHT_PARENTHESIS; //ex_funexp
    bool end_precedent = false; //ex_funexp
    bool tmp_fun_call; //ex_funexp, because od function_call in function_call

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | expression START\n", __LINE__, __FILE__);
#endif

    if ((S = malloc(sizeof(t_stack))) == NULL) {
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | stack malloc; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
        return E_INTERN;
    }

    err = init_stack(S);
    if (err != STACK_INIT_OK) {
        free(S);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | stack init; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
        return E_INTERN;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | shift $\n", __LINE__, __FILE__);
#endif
    err = shift(S, NULL, &end);       //insert first terminal ($) on stack
    if (err != E_OK) {
        free_stack(S);
        free(S);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | shift $ on stack; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
        return err;
    }

    err = stringInit(&tmp);
    if (err != STR_OPERATION_OK) {
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | string init; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
        return E_INTERN;
    }


    do {
        if (token.type < TOKEN_IDENTIFIER) {
            free_stack(S);
            free(S);
            stringFree(&tmp);
#ifdef DEBUG
            fprintf(stderr, "%4d--| %15s | invalid token; return: %d token_type: %d\n", __LINE__, __FILE__, E_SYN, token.type);
#endif
            return E_SYN;
        }

        if (token.type > END_EXPRESSION) {  // others token (not in precedent table) are considered as END_EXPRESSION ($) types
            token_type = END_EXPRESSION;
        } else {
            token_type = token.type;
        }

        topType = top_type(S);
//ex_minus-------------------------------------------------------------------------------------------------------
        if (top != END_EXPRESSION) {
            oneUnderTopTerminal = one_under_top_terminal(S);
        }
//end------------------------------------------------------------------------------------------------------------

        switch (precedent_table[top][token_type]) {
            case '<':
            case '=':
                if (token.type == TOKEN_IDENTIFIER) {

                    if (fun_ptr != NULL) {
                        //first look for in local table
                        err = table_search(fun_ptr->data.value.function.loc_table, &(token.attr.lit_string_or_id), &item_ptr);
                        if (err == TAB_SEARCH_OK) {
                            found = true;

                            if (item_ptr->data.type != DATA_TYPE_FUN_ITEM) {
                                found = false;
                            }
//ex_funexp---------------------------------------------------------------------------------------------------------------------------------------
                            //podmienka: ak sa kluc toknu zhoduje s klucom funptr, tak vtedy
                                //nacitat dalsi token, ak je to zatvorka, tak je to volanie tejto funkcie
                                //ak to neni zatvorka, tak je to navratova hodnota
                            //ak sa kluce nezhoduju, tak je to obyc lok. premenna al. parameter

                            if (stringCmpString(&(token.attr.lit_string_or_id), &(fun_ptr->key)) == 0) {
                            
                                err = getNextToken();
                                if (err != E_OK) {
                                    free_stack(S);
                                    free(S);
                                    stringFree(&tmp);
#ifdef DEBUG
                                    fprintf(stderr, "%4d--| %15s | get next token error; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                                    return err;
                                }
                                next_token = true;

                                if (token.type == TOKEN_LEFT_PARENTHESIS) {

                                    tmp_fun_call = fun_call;
                                    fun_call = true;

                                    err = function_call(fun_ptr, fun_ptr);
                                    if (err != E_OK) {
                                        free_stack(S);
                                        free(S);
                                        stringFree(&tmp);
#ifdef DEBUG
                                        fprintf(stderr, "%4d--| %15s | function call err; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                                        return err;
                                    }

                                    fun_call = tmp_fun_call;

                                    t_string tmp_key_string;
                                    err = stringInit(&tmp_key_string);
                                    if (err != STR_OPERATION_OK) {
                                        free_stack(S);
                                        free(S);
                                        stringFree(&tmp);
#ifdef DEBUG
                                        fprintf(stderr, "%4d--| %15s | function call err; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                                        return err;
                                    }
                                    generate_key(&tmp_key_string);
                                    err = table_insert(symbol_table, &tmp_key_string, &item_ptr);
                                    stringFree(&tmp_key_string);
                                    if (err != TAB_INSERT_OK) {
                                        free_stack(S);
                                        free(S);
                                        stringFree(&tmp);
#ifdef DEBUG
                                        fprintf(stderr, "%4d--| %15s | INTEGER table insert fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                        return E_INTERN;
                                    }



                                    item_ptr->data.type = DATA_TYPE_CONST;
                                    item_ptr->data.value.constant.type = fun_ptr->data.value.function.ret_type;
                                    if (item_ptr->data.value.constant.type == TYPE_STR) {
                                        item_ptr->data.value.constant.value.string.string = NULL;
                                    }

                                    err = generate_instruction(I_ASSIGN, item_ptr, NULL, NULL);
                                    if (err != LIST_OPERATION_OK) {
                                        free_stack(S);
                                        free(S);
                                        stringFree(&tmp);
#ifdef DEBUG
                                        fprintf(stderr, "%4d--| %15s | generate instruction I_ASIGN fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                        return E_INTERN;
                                    }

                                }
                            }
//end-----------------------------------------------------------------------------------------------------------------------------------
                        }
                    }
                            
                    if (!found) {
                        //if not in loc. table, look for in global table
                        err = table_search(symbol_table, &(token.attr.lit_string_or_id), &item_ptr);
                        if (err == TAB_SEARCH_FAIL) {
                            free_stack(S);
                            free(S);
                            stringFree(&tmp);
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | undefinied variable; return: %d token_type: %d\n", __LINE__, __FILE__, E_SEM_DEF, token.type);
#endif
                            return E_SEM_DEF;
                        }

                        if (item_ptr->data.type != DATA_TYPE_VAR) {
//ex_funexp--------------------------------------------------------------------------------------------------------------------
                            //ak je to data type FUN, alebo FUN_ (4x) && dalsi token == (
                                //tak vtedy zavolam funkciu- ret=functionCAll(...), I_ASSIGN,
                                //inak err

                            err = getNextToken();
                            if (err != E_OK) {
                                free_stack(S);
                                free(S);
                                stringFree(&tmp);
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | get next token error; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                                return err;
                            }
                            next_token = true;

                            if (item_ptr->data.type == DATA_TYPE_FUN ||
                                item_ptr->data.type == DATA_TYPE_FUN_LEN ||
                                item_ptr->data.type == DATA_TYPE_FUN_CPY ||
                                item_ptr->data.type == DATA_TYPE_FUN_FND ||
                                item_ptr->data.type == DATA_TYPE_FUN_SRT) {

                                tmp_fun_call = fun_call;
                                fun_call = true;

                                err = function_call(fun_ptr, item_ptr);
                                if (err != E_OK) {
                                    free_stack(S);
                                    free(S);
                                    stringFree(&tmp);
#ifdef DEBUG
                                    fprintf(stderr, "%4d--| %15s | function call err; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                                    return err;
                                }

                                fun_call = tmp_fun_call;

                                int tmp_var_type = item_ptr->data.value.function.ret_type;

                                t_string tmp_key_string;
                                err = stringInit(&tmp_key_string);
                                if (err != STR_OPERATION_OK) {
                                    free_stack(S);
                                    free(S);
                                    stringFree(&tmp);
#ifdef DEBUG
                                    fprintf(stderr, "%4d--| %15s | function call err; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                                    return err;
                                }
                                generate_key(&tmp_key_string);
                                err = table_insert(symbol_table, &tmp_key_string, &item_ptr);
                                stringFree(&tmp_key_string);
                                if (err != TAB_INSERT_OK) {
                                    free_stack(S);
                                    free(S);
                                    stringFree(&tmp);
#ifdef DEBUG
                                    fprintf(stderr, "%4d--| %15s | INTEGER table insert fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                    return E_INTERN;
                                }

                                item_ptr->data.type = DATA_TYPE_CONST;
                                item_ptr->data.value.constant.type = tmp_var_type;
                                if (item_ptr->data.value.constant.type == TYPE_STR) {
                                    item_ptr->data.value.constant.value.string.string = NULL;
                                }
                                
                                err = generate_instruction(I_ASSIGN, item_ptr, NULL, NULL);
                                if (err != LIST_OPERATION_OK) {
                                    free_stack(S);
                                    free(S);
                                    stringFree(&tmp);
#ifdef DEBUG
                                    fprintf(stderr, "%4d--| %15s | generate instruction I_ASSIGN fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                    return E_INTERN;
                                }

                            } else {
//end-------------------------------------------------------------------------------------------
                                free_stack(S);
                                free(S);
                                stringFree(&tmp);
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | data type not DATA_TYPE_VAR || FUN; return: %d token_type: %d\n", __LINE__, __FILE__, E_SEM_DEF, token.type);
#endif
                                return E_SEM_DEF;
                            }
                        }
                    }
                    found = false;

                    err = shift(S, item_ptr, &expression);


                } else if (token.type == TOKEN_LIT_INTEGER) {
                    generate_key(&tmp);
                    
                    err = table_insert(symbol_table, &tmp, &item_ptr);
                    if (err != TAB_INSERT_OK) {
                        free_stack(S);
                        free(S);
                        stringFree(&tmp);
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | INTEGER table insert fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                        return E_INTERN;
                    }

                    item_ptr->data.type = DATA_TYPE_CONST;
                    item_ptr->data.value.constant.type = TYPE_INT;
                    item_ptr->data.value.constant.value.integer = token.attr.lit_int;
                    err = shift(S, item_ptr, &expression);


                } else if (token.type == TOKEN_LIT_REAL) {
                    generate_key(&tmp);

                    err = table_insert(symbol_table, &tmp, &item_ptr);
                    if (err != TAB_INSERT_OK) {
                        free_stack(S);
                        free(S);
                        stringFree(&tmp);
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | REAL table insert fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                        return E_INTERN;
                    }

                    item_ptr->data.type = DATA_TYPE_CONST;
                    item_ptr->data.value.constant.type = TYPE_REAL;
                    item_ptr->data.value.constant.value.real = token.attr.lit_double;
                    err = shift(S, item_ptr, &expression);


                } else if (token.type == TOKEN_LIT_STRING) {
                    generate_key(&tmp);

                    err = table_insert(symbol_table, &tmp, &item_ptr);
                    if (err != TAB_INSERT_OK) {
                        free_stack(S);
                        free(S);
                        stringFree(&tmp);
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | STRING table insert fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                        return E_INTERN;
                    }

                    item_ptr->data.type = DATA_TYPE_CONST;
                    item_ptr->data.value.constant.type = TYPE_STR;
                    err = stringDuplicate(&(item_ptr->data.value.constant.value.string), &(token.attr.lit_string_or_id));
                    if (err != STR_OPERATION_OK) {
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | STRING, string duplicate fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                        return E_INTERN;
                    }
                    err = shift(S, item_ptr, &expression);


                } else if (token.type == TOKEN_LIT_BOOLEAN) {
                    generate_key(&tmp);

                    err = table_insert(symbol_table, &tmp, &item_ptr);
                     if (err != TAB_INSERT_OK) {
                        free_stack(S);
                        free(S);
                        stringFree(&tmp);
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | BOOL table insert fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                        return E_INTERN;
                     }

                     item_ptr->data.type = DATA_TYPE_CONST;
                     item_ptr->data.value.constant.type = TYPE_BOOL;
                     item_ptr->data.value.constant.value.boolean = token.attr.lit_int ? 1 : 0;
                     err = shift(S, item_ptr, &expression);
//expansion for unary minus -------------------------------------------------------------------------------------------------------------------------
                } else if ( ( (token.type == TOKEN_MINUS) && //1.
                            (topType == EXPRESSION) && (top != END_EXPRESSION) && (top != TOKEN_LEFT_PARENTHESIS) &&
                            (top != TOKEN_GREATER_THAN) && (top != TOKEN_LESS_THAN) && (top != TOKEN_LESS_THAN_OR_EQUAL) &&
                            (top != TOKEN_GREATER_THAN_OR_EQUAL) && (top != TOKEN_EQUAL) && (top != TOKEN_NOT_EQUAL) ) //end 1.
                         || ( (token.type == TOKEN_MULTIPLICATION || token.type == TOKEN_DIVISION) && (top != TOKEN_PLUS) &&//2.
                            (((top == TOKEN_MINUS) && (oneUnderTopTerminal != EXPRESSION)) || (top != TOKEN_MINUS)) &&
                            (topType == EXPRESSION) && (top != END_EXPRESSION) && (top != TOKEN_LEFT_PARENTHESIS) &&
                            (top != TOKEN_GREATER_THAN) && (top != TOKEN_LESS_THAN) && (top != TOKEN_LESS_THAN_OR_EQUAL) &&
                            (top != TOKEN_GREATER_THAN_OR_EQUAL) && (top != TOKEN_EQUAL) && (top != TOKEN_NOT_EQUAL) ) ) { //end 2.

                    err = apply_rule(S);
                    if (err != E_OK) {
                        free_stack(S);
                        free(S);
                        stringFree(&tmp);
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | function apply_rule fail; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                        return err;
                    }

                    top = top_terminal(S);

                    if ((token.type == TOKEN_MINUS) && (top == TOKEN_MINUS)) {
                        err = apply_rule(S);
                        if (err != E_OK) {
                            free_stack(S);
                            free(S);
                            stringFree(&tmp);
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | function apply_rule fail; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                            return err;
                        }
                        err = shift (S, NULL, &token.type);
                    } else {
                        err = shift (S, NULL, &token.type);
                    }
//end of expansion -----------------------------------------------------------------------------------------------------------------------------------------
                } else {
                    err = shift (S, NULL, &token.type);
                }

                if(err != E_OK) {
                    free_stack(S);
                    free(S);
                    stringFree(&tmp);
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | shift fail; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                    return err;
                }
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | shift token_type: %d\n", __LINE__, __FILE__, token.type);
#endif
//ex_funexp-----------------------------------------------------------------------
                if (!next_token) {
//end-----------------------------------------------------------------------
                    err = getNextToken();
                    if (err != E_OK) {
                        free_stack(S);
                        free(S);
                        stringFree(&tmp);
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | get next token error; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                        return err;
                    }
                }
//ex_funexp----------------------------------------
                next_token = false;
//end---------------------------------------------

                break;

            case '>':
                err = apply_rule(S);
                if (err != E_OK) {
                    free_stack(S);
                    free(S);
                    stringFree(&tmp);
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | function apply_rule fail; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                    return err;
                }

                break;
            
            //zero in precedent_table
            default:
//ex_funexp---------------------------------------------------------------------------------------------------------------
                if ((top == END_EXPRESSION) && (token_type == TOKEN_RIGHT_PARENTHESIS) && (fun_call)) {
                
                    err = shift(S, NULL, &right_parenthesis);
                    if (err != E_OK) {
                        return err;
                    }
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | shift token_type: %d\n", __LINE__, __FILE__, TOKEN_RIGHT_PARENTHESIS);
#endif
                    err = apply_rule(S);
                    if (err != E_OK) {
                        free_stack(S);
                        free(S);
                        stringFree(&tmp);
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | function apply_rule fail; return: %d token_type: %d\n", __LINE__, __FILE__, err, token.type);
#endif
                        return err;
                    }
                    end_precedent = true;
//end---------------------------------------------------------------------------------------------------------------------
                } else {

                    free_stack(S);
                    free(S);
                    stringFree(&tmp);
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | zero in precedent table; return: %d token_type: %d\n", __LINE__, __FILE__, E_SYN, token.type);
#endif
                    return E_SYN;
                }
                break;
        }

        top = top_terminal(S);

    } while (((top != END_EXPRESSION) || (token.type < END_EXPRESSION)) && (!end_precedent));
    
    top = top_and_pop(S, &item_ptr);     //result expression
    if (top != EXPRESSION) {
        free_stack(S);
        free(S);
        stringFree(&tmp);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | Result EXPRESSION is not at the top of stack; return: %d token_type: %d\n", __LINE__, __FILE__, E_SYN, token.type);
#endif
        return E_SYN;
    }

#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | result type is %d\n", __LINE__, __FILE__, item_ptr->data.value.constant.type);
#endif
    *result_type = item_ptr->data.value.constant.type;
    
    err = generate_instruction(I_LOAD, item_ptr, NULL, NULL);
    if (err != LIST_OPERATION_OK) {
        free_stack(S);
        free(S);
        stringFree(&tmp);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | generate instruction I_LOAD fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
        return E_INTERN;
    }

    top = top_and_pop(S, &item_ptr);     //end expression on stack
    if (top != END_EXPRESSION) {
        free_stack(S);
        free(S);
        stringFree(&tmp);
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | $ is not as last at stack; return: %d token_type: %d\n", __LINE__, __FILE__, E_SYN, token.type);
#endif
        return E_SYN;
    }

    free_stack(S);
    free(S);
    stringFree(&tmp);
#ifdef DEBUG
    fprintf(stderr, "%4d--| %15s | expression OK\n", __LINE__, __FILE__);
#endif
    return E_OK;
}

/**
 * \brief Function to apply rule with actual non-terminal
 *        (to reduce non-terminals on stack, if it is possible according to rules)
 * @param   S         pointer to t_stack structute, where are terminals and non-terminals stored
 * @return  E_INTERN
 *          E_SEM_TYPE
 *          E_SYN
 *          E_OK      if success
 */
int apply_rule(t_stack *S)
{
    int actual_type, operator;
    int err = E_SYN;
    int type_1, type_2;
    t_tab_item *item_ptr_1, *item_ptr_2, *item_ptr_Rslt;
    t_string tmp;
    int expression = EXPRESSION;  //expression type for stack to use as pointer
    int top;
    int left_parenthesis = TOKEN_LEFT_PARENTHESIS; //ex_funexp

    actual_type = top_and_pop(S, &item_ptr_2);

    //rule E --> (E)
    if (actual_type == TOKEN_RIGHT_PARENTHESIS) {
        actual_type = top_and_pop(S, &item_ptr_2);
         
        if (actual_type == EXPRESSION) {
//ex_funexp-----------------------------------------------------------------------------------
            top = top_type(S);
            if ((top == END_EXPRESSION) && (fun_call)) {
                
                err = shift(S, NULL, &left_parenthesis);
                if (err != E_OK) {
                    return err;
                }

                fun_call = false;

            }
//end-----------------------------------------------------------------------------------------
            actual_type = top_and_pop(S, &item_ptr_1);
            
            if (actual_type == TOKEN_LEFT_PARENTHESIS) {
                err = shift(S, item_ptr_2, &expression);
#ifdef DEBUG
                fprintf(stderr, "%4d--| %15s | rule E --> (E); return: %d shifted EXPRESSION: %d\n", __LINE__, __FILE__, err, item_ptr_2->data.value.variable.type);
#endif
            }
         }
    //E
    } else if (actual_type == EXPRESSION) {
        operator = top_and_pop(S, &item_ptr_1);
        //operator
        if ((operator >= TOKEN_PLUS) && (operator <= TOKEN_NOT_EQUAL)) {

// expansion for unary minus --------------------------------------------------------------------------
            if (operator == TOKEN_MINUS) {
                
                int topType = top_type(S);
                int minus_count = 1;

                while (topType == TOKEN_MINUS) {
                    actual_type = top_and_pop(S, &item_ptr_1);
                    minus_count += 1;
                    topType = top_type(S);
                }

                int type;
                switch (item_ptr_2->data.type) {
                    case DATA_TYPE_FUN_ITEM:
                        type = item_ptr_2->data.value.fun_item.type;
                        break;
                    case DATA_TYPE_VAR:
                        type = item_ptr_2->data.value.variable.type;
                        break;
                    case DATA_TYPE_CONST:
                        type = item_ptr_2->data.value.constant.type;
                        break;
                    default:
                        return E_SEM_DEF;
                }

                if (topType == END_EXPRESSION) {
                    if ((minus_count % 2) == 1) {

                        if ((type == TYPE_INT) || (type == TYPE_REAL)) {

                            err = stringInit(&tmp);
                            if (err != STR_OPERATION_OK) {
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | String init fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                return E_INTERN;
                            }

                            generate_key(&tmp);

                            err = table_insert(symbol_table, &tmp, &item_ptr_Rslt);
                            if (err != TAB_INSERT_OK) {
                                stringFree(&tmp);
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | item_ptr_Rslt table insert fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                return E_INTERN;
                            }
                            stringFree(&tmp);

                            item_ptr_Rslt->data.type = DATA_TYPE_CONST;
                            item_ptr_Rslt->data.value.constant.type = type; //TYPE_INT || TYPE_RAL

                            err = generate_instruction(I_NEG, item_ptr_Rslt, item_ptr_2, NULL);
                            if (err != LIST_OPERATION_OK) {
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | generate intruction fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                return E_INTERN;
                            }

                            err = shift(S, item_ptr_Rslt, &expression);
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | rule ---E --> - E; return: %d shifted EXPRESSION type: %d\n", __LINE__, __FILE__, err, type);
#endif
                            return err;
                        } else {
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | not allowed rule ---E --> -E with type: %d, err: %d\n", __LINE__, __FILE__, type, E_SEM_OTHER);
#endif
                            return E_SEM_OTHER;
                        }
                    
                    } else {
                        if ((type == TYPE_INT) || (type == TYPE_REAL)) {

                            err = shift(S, item_ptr_2, &expression);
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | rule --E -->  E; return: %d shifted EXPRESSION type: %d\n", __LINE__, __FILE__, err, type);
#endif
                            return err;
                        } else {
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | not allowed rule --E --> E with type: %d, err: %d\n", __LINE__, __FILE__, type, E_SEM_OTHER);
#endif
                            return E_SEM_OTHER;
                        }
                    }

                } else if (topType == EXPRESSION) {

                    if ((minus_count % 2) == 0) {
                        if ((type == TYPE_INT) || (type == TYPE_REAL)) {

                            err = stringInit(&tmp);
                            if (err != STR_OPERATION_OK) {
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | String init fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                return E_INTERN;
                            }

                            generate_key(&tmp);

                            err = table_insert(symbol_table, &tmp, &item_ptr_Rslt);
                            if (err != TAB_INSERT_OK) {
                                stringFree(&tmp);
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | item_ptr_Rslt table insert fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                return E_INTERN;
                            }
                            stringFree(&tmp);

                            item_ptr_Rslt->data.type = DATA_TYPE_CONST;
                            item_ptr_Rslt->data.value.constant.type = type; //TYPE_INT || TYPE_RAL

                            err = generate_instruction(I_NEG, item_ptr_Rslt, item_ptr_2, NULL);
                            if (err != LIST_OPERATION_OK) {
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | generate intruction fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                return E_INTERN;
                            }

                            item_ptr_2 = item_ptr_Rslt;
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | rule ---E --> -E; EXPRESSION type: %d\n", __LINE__, __FILE__, type);
#endif
                        } else {
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | not allowed rule ---E --> -E with type: %d, err: %d\n", __LINE__, __FILE__, type, E_SEM_OTHER);
#endif
                            return E_SEM_OTHER;
                        }

                    } else {
                        if ((type == TYPE_INT) || (type == TYPE_REAL) || (type == TYPE_STR) || (type == TYPE_BOOL)) {
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | rule --E --> E or nothing; EXPRESSION type: %d\n", __LINE__, __FILE__, type);
#endif
                        }
                    }

                } else if ((topType == TOKEN_PLUS) || (topType == TOKEN_MULTIPLICATION) || (topType == TOKEN_DIVISION) || (topType == TOKEN_LEFT_PARENTHESIS) ||
                           (topType == TOKEN_GREATER_THAN) || (topType == TOKEN_GREATER_THAN_OR_EQUAL) || (topType == TOKEN_LESS_THAN) || (topType == TOKEN_LESS_THAN_OR_EQUAL) || 
                           (topType == TOKEN_EQUAL) || (topType == TOKEN_NOT_EQUAL)) {

                    if ((minus_count % 2) == 1) {
                        if ((type == TYPE_INT) || (type == TYPE_REAL)) {

                            err = stringInit(&tmp);
                            if (err != STR_OPERATION_OK) {
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | String init fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                return E_INTERN;
                            }

                            generate_key(&tmp);

                            err = table_insert(symbol_table, &tmp, &item_ptr_Rslt);
                            if (err != TAB_INSERT_OK) {
                                stringFree(&tmp);
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | item_ptr_Rslt table insert fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                return E_INTERN;
                            }
                            stringFree(&tmp);

                            item_ptr_Rslt->data.type = DATA_TYPE_CONST;
                            item_ptr_Rslt->data.value.constant.type = type; //TYPE_INT || TYPE_RAL

                            err = generate_instruction(I_NEG, item_ptr_Rslt, item_ptr_2, NULL);
                            if (err != LIST_OPERATION_OK) {
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | generate intruction fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                                return E_INTERN;
                            }

                            if ((topType == TOKEN_LEFT_PARENTHESIS) || (topType == TOKEN_GREATER_THAN) || (topType == TOKEN_GREATER_THAN_OR_EQUAL) || (topType == TOKEN_LESS_THAN) || (topType == TOKEN_LESS_THAN_OR_EQUAL) ||
                                (topType == TOKEN_EQUAL) || (topType == TOKEN_NOT_EQUAL)) {

                                err = shift(S, item_ptr_Rslt, &expression);
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | rule ---E --> - E; return: %d shifted EXPRESSION type: %d\n", __LINE__, __FILE__, err, type);
#endif
                                return err;
                            } else {
                                item_ptr_2 = item_ptr_Rslt;
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | rule ---E --> -E; EXPRESSION type: %d\n", __LINE__, __FILE__, type);
#endif
                            }

                        } else {
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | not allowed rule ---E --> -E with type: %d, err: %d\n", __LINE__, __FILE__, type, E_SEM_OTHER);
#endif
                            return E_SEM_OTHER;
                        }

                    } else {
                        if ((type == TYPE_INT) || (type == TYPE_REAL) || (type == TYPE_STR) || (type == TYPE_BOOL)) {
                            
                            if (topType == TOKEN_LEFT_PARENTHESIS) {

                                err = shift(S, item_ptr_2, &expression);
#ifdef DEBUG
                                fprintf(stderr, "%4d--| %15s | rule --E --> E; return: %d shifted EXPRESSION type: %d\n", __LINE__, __FILE__, err, type);
#endif
                                return err;
                            } else {
#ifdef DEBUG
                            fprintf(stderr, "%4d--| %15s | rule --E --> E or nothing; EXPRESSION type: %d\n", __LINE__, __FILE__, type);
#endif
                            }
                        }
                    }

                    operator = top_and_pop(S, &item_ptr_1);
                }
            }
//end of expansion------------------------------------------------------------------------------------------------------------------

            // rule E --> E op E
            actual_type = top_and_pop(S, &item_ptr_1);
            //E
            if (actual_type == EXPRESSION) {

                switch (item_ptr_1->data.type) {
                    case DATA_TYPE_FUN_ITEM:
                        type_1 = item_ptr_1->data.value.fun_item.type;
                        break;
                    case DATA_TYPE_VAR:
                        type_1 = item_ptr_1->data.value.variable.type;
                        break;
                    case DATA_TYPE_CONST:
                        type_1 = item_ptr_1->data.value.constant.type;
                        break;
                    default:
                        return E_SEM_DEF;
                }

                switch (item_ptr_2->data.type) {
                    case DATA_TYPE_FUN_ITEM:
                        type_2 = item_ptr_2->data.value.fun_item.type;
                        break;
                    case DATA_TYPE_VAR:
                        type_2 = item_ptr_2->data.value.variable.type;
                        break;
                    case DATA_TYPE_CONST:
                        type_2 = item_ptr_2->data.value.constant.type;
                        break;
                    default:
                        return E_SEM_DEF;
                }
                
                //chceck of type compatibility of operands
                if ( (((type_1 == TYPE_INT)||(type_1 == TYPE_REAL)) && ((type_2 == TYPE_INT)||(type_2 == TYPE_REAL))
                    && ((operator >= TOKEN_PLUS)&&(operator <= TOKEN_DIVISION))) || (type_1 == type_2) ) {
                    //check of allowed arithmetic operations with operands
                    if ((semantic_table[type_1][operator] == 0) || (semantic_table[type_2][operator] == 0)){
#ifdef DEBUG
                        fprintf(stderr, "%4d--| %15s | Not allowed operation with operands; return: %d token_type: %d\n", __LINE__, __FILE__, E_SEM_TYPE, token.type);
#endif
                        return E_SEM_TYPE;      //forbidden operation with current data type
                    }
                } else {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | Bad type compatibility of operands; return: %d token_type: %d\n", __LINE__, __FILE__, E_SEM_TYPE, token.type);
#endif
                    return E_SEM_TYPE;
                }

                err = stringInit(&tmp);
                if (err != STR_OPERATION_OK) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | String init fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                    return E_INTERN;
                }

                generate_key(&tmp);

                err = table_insert(symbol_table, &tmp, &item_ptr_Rslt);
                if (err != TAB_INSERT_OK) {
                    stringFree(&tmp);
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | item_ptr_Rslt table insert fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                    return E_INTERN;
                }
                stringFree(&tmp);

                if (operator >= TOKEN_LESS_THAN) {
                    item_ptr_Rslt->data.type = DATA_TYPE_CONST;
                    item_ptr_Rslt->data.value.constant.type = TYPE_BOOL;
                
                } else if (operator == TOKEN_PLUS) {
                    
                    if ((type_1 == TYPE_STR) && (type_2 == TYPE_STR)) {
                        item_ptr_Rslt->data.type = DATA_TYPE_CONST;
                        item_ptr_Rslt->data.value.constant.type = TYPE_STR;
                        item_ptr_Rslt->data.value.constant.value.string.string = NULL;

                    } else if ((type_1 == TYPE_INT) && (type_2 == TYPE_INT)) {
                        item_ptr_Rslt->data.type = DATA_TYPE_CONST;
                        item_ptr_Rslt->data.value.constant.type = TYPE_INT;
                   
                    } else {
                        item_ptr_Rslt->data.type = DATA_TYPE_CONST;
                        item_ptr_Rslt->data.value.constant.type = TYPE_REAL;
                    }

                } else if ((operator == TOKEN_MINUS) || (operator == TOKEN_MULTIPLICATION)) {
                    
                    if ((type_1 == TYPE_INT) && (type_2 == TYPE_INT)) {
                        item_ptr_Rslt->data.type = DATA_TYPE_CONST;
                        item_ptr_Rslt->data.value.constant.type = TYPE_INT;
                    
                    } else {
                        item_ptr_Rslt->data.type = DATA_TYPE_CONST;
                        item_ptr_Rslt->data.value.constant.type = TYPE_REAL;
                    }
                
                } else if (operator == TOKEN_DIVISION) {
                    item_ptr_Rslt->data.type = DATA_TYPE_CONST;
                    item_ptr_Rslt->data.value.constant.type = TYPE_REAL;
                }

                err = generate_instruction((operator - TOKEN_PLUS + I_ADD), item_ptr_Rslt, item_ptr_1, item_ptr_2);
                if (err != LIST_OPERATION_OK) {
#ifdef DEBUG
                    fprintf(stderr, "%4d--| %15s | generate intruction fail; return: %d token_type: %d\n", __LINE__, __FILE__, E_INTERN, token.type);
#endif
                    return E_INTERN;
                }

                err = shift(S, item_ptr_Rslt, &expression);

#ifdef DEBUG 
                fprintf(stderr, "%4d--| %15s | rule E --> E %d E; return: %d shifted EXPRESSION type: %d\n", __LINE__, __FILE__, operator, err, item_ptr_Rslt->data.value.constant.type);
#endif
            }
        }
    }
    return err;
}



/**
 * \brief Function to discover type, of top terminal on stack.
 * @param   S            pointer to t_stack structute
 * @return  top_terminal type of first terminal, from top of the stack
 */
int top_terminal(t_stack *S)
{
    int top_terminal;
    int i = 1;

    memcpy(&top_terminal, S->array + S->stack_top - sizeof(int), sizeof(int));

    while (top_terminal == EXPRESSION) {
        memcpy(&top_terminal, S->array + S->stack_top - (i*(sizeof(int) + sizeof(t_tab_item *)) + sizeof(int)), sizeof(int));    //preskocim EXPRESSION data na zasobniku
        i += 1;
    }

    return top_terminal;
}


/**
 * \brief Function to discover second terminal type from top of the stack.
 * @param   S            pointer to t_stack structute
 * @return  top_terminal type of second terminal from top of the stack
 */
int one_under_top_terminal(t_stack *S)
{
    int top_terminal;
    int i = 1;

    memcpy(&top_terminal, S->array + S->stack_top - sizeof(int), sizeof(int));

    while (top_terminal == EXPRESSION) {
        memcpy(&top_terminal, S->array + S->stack_top - (i*(sizeof(int) + sizeof(t_tab_item *)) + sizeof(int)), sizeof(int)); //top terminal
        i += 1;
    }

    i--;
    memcpy(&top_terminal, S->array + S->stack_top - (i*(sizeof(int) + sizeof(t_tab_item *)) + 2*sizeof(int)), sizeof(int)); //one under top terminal

    return top_terminal;
}

/**
 * \brief Function discovers the top type of terminal/non-terminal on stack
 *        and simuntaneously pop the top item from stack.
 *        If terminal, pop item once.
 *        If non-terminal (type EXPRESSION), pop twice because of stored item pointer to data of non-terminal.
 * @param   S         pointer to t_stack structute
 * @param   item_ptr  returned pointer to data, of stored EXPRESSION item
 *                    NULL if terminal on top
 * @return  top_type  type of poped item
 */
int top_and_pop(t_stack *S, t_tab_item **item_ptr)
{
    int top_type;

    memcpy(&top_type, S->array + S->stack_top - sizeof(int), sizeof(int));
    pop(S, sizeof(int));

    if (top_type == EXPRESSION) {
        memcpy(item_ptr, S->array + S->stack_top - sizeof(t_tab_item *), sizeof(t_tab_item *));
        pop(S, sizeof(t_tab_item *));
    } else {
        *item_ptr = NULL;
    }

    return top_type;
}

/**
 * \brief Function to discover type of item on the top of stack.
 * @param   S         pointer to t_stack structute
 * @return  top_type  type of top item on stack
 */
int top_type(t_stack *S)
{
    int top_type;

    memcpy(&top_type, S->array + S->stack_top - sizeof(int), sizeof(int));

    return top_type;
}

/**
 * \brief Function to push terminal/non-terminal on stack.
 *        Terminals are pushed as one item (void item - type with sizeof int)
 *        Non-terminals are pushed as two items one after another: first push - void* item(item pinter, with sizeof t_tab_item* )
 *                                                                 second push (because we want type upper on stack)- void* item(type, with sizeof int)
 * @param   S         pointer to t_stack structute
 * @param   item_ptr  pointer to item, which will be pushed on stack
 *                    if NULL, we're goint to push terminal
 * @param   type      type of pushed terminal/non-terminal
 * @return  E_INTERN  if stack_push failed
 *          E_OK      if success
 */
int shift(t_stack *S, t_tab_item *item_ptr, int *type)
{
    int err;

    if (item_ptr != NULL) {
        err = push(S, (void *)&item_ptr, sizeof(t_tab_item *));
        if (err != STACK_PUSH_OK) {
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s | fun: shift: adress-push-part: STACK_PUSH_FAIL\n", __LINE__, __FILE__);
#endif
            return E_INTERN;
        }
    }

    err = push(S, (void *)type, sizeof(int));
    if (err != STACK_PUSH_OK) {
#ifdef DEBUG
        fprintf(stderr, "%4d--| %15s |  fun: shift: type-push-part: STACK_PUSH_FAIL\n", __LINE__, __FILE__);
#endif
        return E_INTERN;
    }

    return E_OK;
}
