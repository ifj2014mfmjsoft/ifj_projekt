/**
 * Projekt: Projekt do predmetu IFJ
 * Popis:   Interpret jazyka IFJ14
 * Subor:   common.h
 * Datum:   6.10. 2014
 * Autori:  Matej Vido, xvidom00@stud.fit.vutbr.cz
 */

#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include "string.h"
#include "ial.h"
#include "instlist.h"

/**
 * Error codes.
 */
enum ecodes {
    E_OK         = 0,
    E_LEX        = 1,
    E_SYN        = 2,
    E_SEM_DEF    = 3,
    E_SEM_TYPE   = 4,
    E_SEM_OTHER  = 5,
    E_RUN_INPUT  = 6,
    E_RUN_N_INIT = 7,
    E_RUN_Z_DIV  = 8,
    E_RUN_OTHER  = 9,
    E_INTERN     = 99,
};

/**
 * Constants for types of variables and functions in ifj14.
 */
enum types {
    TYPE_INT  = 0,
    TYPE_REAL  = 1,
    TYPE_STR = 2,
    TYPE_BOOL = 3,
    TYPE_UNINIT = 4,
};

/**
 * Token.
 */
typedef struct {
    int type;
    struct {
        int lit_int;
        double lit_double;
        t_string lit_string_or_id;
    } attr;
} t_token;

#define DEFAULT_TABLE_SIZE 1024
#define DEFAULT_LOCAL_TABLE_SIZE 512

/**
 * Global variables.
 */
extern FILE * source_file;
extern t_token token;
extern t_table * symbol_table;
extern t_instr_list instruction_list;

#endif
